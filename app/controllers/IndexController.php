<?php
use Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email;
class IndexController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        $this->view->somePartners = $this->_getSomePartners();
        $this->validateLoginVolunteer();
    }

    public function indexAction()
    {
       $about=Tblother::findfirst("title='Main Tagline'");
       $this->view->about=$about;
       $contact= Tblcontact::find();
       $this->view->contacts=$contact;


    	$progs = Tblprograms::find();
        $slides = Tblslides::find();
        $this->view->slides = null;
    	foreach ($progs as $p => $v) {
    		$this->view->setVar("prog".$v->programID, 
    			array(
    				'id' => $v->programID,
    				'title' => $v->programName,
    				'tagline' => $v->programTagline,
    				'url' => $v->programPage,
    				'banner' => $v->programBanner
    				));
    		$progsimg = Tblprogramsimg::find('programID='.$v->programID);
    		$imgs = array();
    		foreach ($progsimg as $pik => $piv) {
    			$imgs[] = $piv->imgname;
    		}
    		$this->view->setVar("progimg".$v->programID, $imgs);
    	}
        // Construct slides html
        foreach($slides as $slide) {
            $video = "";
            if(!is_null($slide->slideVideoUrl)){
                $video = "<div class='auto-resizable-iframe'><div><iframe class='youtube-float-".$slide->slideVideoAlignment."' src='".$slide->slideVideoUrl."' frameborder='0' allowfullscreen></iframe></div></div>";         
            }
            $this->view->slides .= "<li><div class='header-images' 
            style='
                background-image: url(".$slide->slideImgUrl.");
                background-position: center 80%;'>".$video."</div>
            </li>";

              $this->view->youtubeoutbox .= "<div class='youtube-outbox'><center><iframe src='".$slide->slideVideoUrl."' frameborder='0' allowfullscreen></iframe></center></div>";
        }

        
    }

    public function submitEmailNewsLetterAction(){
        $this->view->disable();
        if ($this->request->isAjax()){
            $response = array();
            $name = $this->request->getPost('name', 'striptags');
            $email = $this->request->getPost('email', 'striptags');

            $validation = new Phalcon\Validation();

            $validation->add('name', new PresenceOf(array(
                'message' => 'The name is required'
            )));

            $validation->add('email', new PresenceOf(array(
                'message' => 'The e-mail is required',
                'cancelOnFail' => true
            )));
           /* new StringLength(array(
              'min' => 2,
              'messageMinimum' => 'Username should have at least 2 minimum characters'            
              ))
*/
            $validation->add('email', new Email(array(
                'message' => 'The e-mail is not valid'
            )));

            $messages = $validation->validate($_POST);
            if (count($messages)) {
                $errorMessage = null;
                foreach ($messages as $message) {
                    $errorMessage .= $message. '<br>';
                }
                $response['success'] = false;
                $response['message'] = '<div class="alert alert-warning">'.$errorMessage.'</div>';
            }else{
                $enews = Tblnewsletteremails::findFirst('email="'.$email.'"');
                if($enews){
                    $response['success'] = false;
                    $response['message'] = '<div class="alert alert-warning">Your email is already registered to our e-newsletter.</div>';
                }elseif(strlen($name) < 2){
                    $response['success'] = false;
                    $response['message'] = '<div class="alert alert-warning">Fullname should have at least 2 minimum characters.</div>';
                }else{
                    $enews = new Tblnewsletteremails();
                    $enews->assign(array('name'=>$name, 'email'=>$email, 'dateAdded'=>time()));
                    $enews->save();
                    $response['success'] = true;
                    $response['message'] = '<div class="alert alert-success">Thank you for subscribing to our e-newsletter.</div>';
                }
            }

            echo json_encode($response);
        }
    }

    private function _getSomePartners(){
        $phql = 'SELECT 
                Tblpartners.partnerID,
                Tblpartners.partnerName,
                Tblpartners.partnerInfo
                FROM Tblpartners
                LEFT JOIN Tblusers
                ON Tblpartners.userID = Tblusers.userID
                WHERE userLevel = 2 ORDER BY RAND() LIMIT 5';
        $result = $this->modelsManager->executeQuery($phql);
        $dataArray = array();
        foreach ($result as $key => $value) {            
            $dataArray[] = array(
                'partnerID'=>$value->partnerID,
                'partnerName'=>$value->partnerName,
                'partnerInfo'=>$this->_truncateHtml($value->partnerInfo),
                );
        }

        return $dataArray;
    }

    public function logoutAction(){
        $this->session->remove('vol_auth');
        header("Location: ".$this->url->get());
    }
}