<?php
use Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email,
    Phalcon\Validation\Validator\StringLength as StringLength,
    Phalcon\Validation\Validator\Confirmation as Confirmation;
class MyaccountController extends ControllerBase
{    

    protected $breadCrumbs = "<a href='/'>Home</a> > ";

    public function initialize()
    {
        parent::initialize();
        $this->view->bread_crumbs = $this->breadCrumbs;
        $this->view->current = $this->router->getActionName();
        $this->validateLoginVolunteer();
    }

    public function indexAction(){
        if(!$this->session->has("vol_auth")){
            $this->response->redirect('/');
        } else {
            $this->view->bread_crumbs = $this->breadCrumbs .= 'My Account';
            $this->view->program_title = 'My Account';
            $this->view->abkVolUser = Tblvolunteers::findFirst("volunteerID=".$this->volUserId);
            $phql = 'SELECT  Tblprograms.programName, Tblporgramactivities.activity
                FROM Tblvolunteeractivities
                LEFT JOIN Tblporgramactivities ON Tblporgramactivities.activityID = Tblvolunteeractivities.activityID
                LEFT JOIN Tblprograms ON Tblprograms.programID = Tblporgramactivities.programID
                WHERE Tblvolunteeractivities.volunteerID = '.$this->volUserId.' AND Tblvolunteeractivities.activityID != 0
                ORDER BY Tblprograms.programName
                ';
            $volunteerActivities =  $this->modelsManager->executeQuery($phql);
            $acitvityHtml = '';
            if(count($volunteerActivities) > 0){
                $proName = $volunteerActivities[0]->programName;
                $acitvityHtml = '<strong>'.$proName.'</strong><ul>';
                foreach ($volunteerActivities as $key => $value) {
                    if($proName != $value->programName){
                        $proName = $value->programName;
                        $acitvityHtml .= '</ul><strong>'.$proName.'</strong><ul>';
                    }
                    $acitvityHtml .= '<li>'.$value->activity.'</li>';
                }
                $acitvityHtml .= '</ul>';
            }
            $volunteerSuggActivities = null;
            $sugg = Tblvolunteeractivities::find('volunteerID = '.$this->volUserId.' AND activityID = 0');
            foreach ($sugg as $key => $value) {
                $volunteerSuggActivities .= '<li>'.$value->custom.'</li>';
            }

            $this->view->volunteerSuggActivities = $volunteerSuggActivities ? $volunteerSuggActivities: null;
            $this->view->volunteerActivities = $acitvityHtml ? $acitvityHtml:null;
        }
    }

    public function forgotpasswordAction(){
        $this->view->bread_crumbs = $this->breadCrumbs .= "Forgot Password";
        $this->view->program_title = 'Forgot Password';
        $this->view->emailError = null;
        /*if($this->request->isPost() && $this->request->isAjax() == false)*/

        if($this->request->isPost() && !$this->request->getPost('loginFormActive'))
        {
            $email = $this->request->getPost("email");
            if($volunteer = Tblvolunteers::findFirst("email = '$email' AND status = '1'")) {
                $a = '';
                for ($i = 0; $i < 6; $i++) {
                    $a .= mt_rand(0, 9);
                }
                $token = sha1($a);
                $volunteer->forgotToken = $token;
                $volunteer->save();

                $body = "
                <h4>Having trouble signing in?</h4>
                <p>No worries, resetting your account password is easy.</p>
                <p>Please <a href='http://abk.gotitgenius.com/myaccount/resetpassword/$email/$token'>click here</a> to change your account password.</p>
                <p>Sincerely,</p>
                <p>ABK Foundation</p>
                ";

                $mailObjects = array(
                'From'=> 'angbayanko.org@no-reply.com',
                'FromName' => 'angbayanko.org',
                'AddAddress'=> $volunteer->email,
                'Subject' => "ABK Foundation: Password Reset",
                'Body' => $body
                );
                
                if($this->_sendmail($mailObjects)) {
                    $response['success'] = true;
                    $response['message'] = "<div class='alert alert-success alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                            A password reset link has been sent to your mailbox.
                            </div>";
                } else {
                    $response['success'] = false;
                    $response['message'] = "<div class='alert alert-danger alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                            There seems to be a problem while sending an email. Please try again later.
                            </div>";
                }
            } else {
                $response['success'] = false;
                $response['message'] = "<div class='alert alert-danger alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                            Email entered is not associated with an account.
                            </div>";
            }
            echo json_encode($response);
        }
    }

    public function resetpasswordAction($email, $token){
        $this->view->bread_crumbs = $this->breadCrumbs .= "Reset Password";
        $this->view->program_title = 'Reset Password';
        $this->view->email = $email;
        $this->view->token = $token;
        $passError = "";

        if(!$volunteer = Tblvolunteers::findFirst("email = '$email' AND forgotToken = '$token'")) {
            $this->response->redirect('/');
        }

        if($this->request->isPost() && !$this->request->getPost('loginFormActive'))
        {
            $validation = new Phalcon\Validation();
            $validation->add('password', new PresenceOf(array(
                'message' => 'The password is required',
                'cancelOnFail' => true
                )));
            $validation->add('password', new StringLength(array(
                'max' => 20,
                'min' => 8,
                'messageMaximum' => 'Thats an exagerated password.',
                'messageMinimum' => 'Password should be Minimum of 8 characters.'
                )));

            $validation->add('repassword', new PresenceOf(array(
                'message' => 'Retyping your password is required'
                )));
            $validation->add('repassword', new Confirmation(array(
                'message' => 'Password doesn\'t match confirmation',
                'with' => 'password'
                )));

            if(count($validation->validate($_POST))){
                foreach ($validation->getMessages()->filter('password') as $message) {
                    $passError .= "<li>".$message."</li> ";
                    $response['success'] = false;
                    $response['message'] = "<div class='alert alert-danger alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                            $passError
                            </div>";
                }
                foreach ($validation->getMessages()->filter('repassword') as $message) {
                    $passError .= "<li>".$message."</li> ";
                    $response['success'] = false;
                    $response['message'] = "<div class='alert alert-danger alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                            $passError
                            </div>";
                }
            } else {
                // http://abk.gotitgenius.com/admin/resetpassword/jl@mailinator.com/e4f82fbf3aa759990f43aac3dca36ac75fccc52d
                $volunteer->password = sha1($this->request->getPost("password"));
                $volunteer->forgotToken = null;
                $save = $volunteer->save();

                if($save) {
                    $response['success'] = true;
                    $response['message'] = "<div class='alert alert-success alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                            New password has been set. <a href='/' >Go to Homepage.</a>
                            </div>";
                } else {
                    $response['success'] = false;
                    $response['message'] = "<div class='alert alert-danger alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                            There seems to be a problem while processing your request. Please try again later.
                            </div>";
                }
            }
            echo json_encode($response);
        }
    }

    public function changepassAction(){
        $this->view->bread_crumbs = $this->breadCrumbs .= "<a href='/myaccount/'>My Account</a> > Change Password";
        $this->view->program_title = 'Change Password';
        if(!$this->session->has("vol_auth")){
            $this->response->redirect('/');
        } else {
            $this->view->abkVolUser = $volunteer = Tblvolunteers::findFirst("volunteerID=".$this->volUserId);
            if($this->request->isPost() && $this->request->isAjax()){
                $validation = new Phalcon\Validation();

                $validation
                ->add('oldpassword', new PresenceOf(array(
                    'message' => 'The current password is required',
                )))
                ->add('password', new PresenceOf(array(
                    'message' => 'The new password is required',
                )))
                ->add('repassword', new PresenceOf(array(
                    'message' => 'Retyping your password is required',
                )))
                ->add('password', new StringLength(array(
                    'max' => 20,
                    'min' => 8,
                    'messageMaximum' => 'Thats an exagerated password.',
                    'messageMinimum' => 'Password should be Minimum of 8 characters.'
                )))
                ->add('repassword', new Confirmation(array(
                    'message' => 'Password doesn\'t match confirmation',
                    'with' => 'password'
                )));

                $validation->setFilters('oldpassword', 'trim');
                $validation->setFilters('password', 'trim');
                $validation->setFilters('repassword', 'trim');

                $messages = $validation->validate($_POST);
                $errMessage = null;

                if (count($messages)) {
                    foreach ($messages as $message) {
                        $errMessage .= '<li>'.$message. '</li>';
                    }
                    $response['success'] = false;
                    $response['message'] = '
                        <div class="alert alert-danger alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>Please fix the following:</strong>'.$errMessage.'
                        </div>
                    ';
                } else {
                    if(sha1($this->request->getPost('oldpassword')) == $volunteer->password){
                        $volunteer->password = sha1($this->request->getPost('password'));
                        if($volunteer->save()){
                            $response['success'] = true;
                            $response['message'] = "
                                <div class='alert alert-success alert-dismissible' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                  Password successfully changed. <a href='/myaccount'>Back to My Account</a>
                                </div>
                            ";
                        } else {
                            $response['success'] = false;
                            $response['message'] = '
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  Something went wrong while processing your request. Please try again later.
                                </div>
                            ';
                        }
                    } else {
                        $response['success'] = false;
                        $response['message'] = '
                            <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <strong>Please fix the following:</strong><li>Wrong password entered</li>
                            </div>
                        ';
                    }
                }
                
                echo json_encode($response);
            }
        }
    }

    public function editProfileAction(){
        $this->view->bread_crumbs = $this->breadCrumbs .= "<a href='/myaccount/'>My Account</a> > Edit Profile";
        $this->view->program_title = 'Edit Profile';
        if(!$this->session->has("vol_auth")){
            $this->response->redirect('/');
        } else {
            $this->view->abkVolUser = $volunteer = Tblvolunteers::findFirst("volunteerID=".$this->volUserId);
            if($this->request->isPost() && $this->request->isAjax()){
                $validation = new Phalcon\Validation();

                $validation
                ->add('title', new PresenceOf(array(
                    'message' => 'The title is required',
                )))
                ->add('fname', new PresenceOf(array(
                    'message' => 'The first name is required',
                )))
                ->add('mname', new PresenceOf(array(
                    'message' => 'The middle name is required',
                )))
                ->add('lname', new PresenceOf(array(
                    'message' => 'The last name is required',
                )))
                ->add('address', new PresenceOf(array(
                    'message' => 'The address is required',
                )))
                ->add('fname', new StringLength(array(
                  'max' => 255,
                  'min' => 1,
                  'messageMaximum' => 'Your name is too long',
                  'messageMinimum' => 'Your first name must be atleast 1 character long'
                )))
                ->add('mname', new StringLength(array(
                      'max' => 255,
                      'min' => 1,
                      'messageMaximum' => 'Your middle name is too long',
                      'messageMinimum' => 'Your middle name must be atleast 1 character long'
                )))
                ->add('lname', new StringLength(array(
                      'max' => 255,
                      'min' => 1,
                      'messageMaximum' => 'Your last name is too long',
                      'messageMinimum' => 'Your last name must be atleast 1 character long'
                )))
                ->add('extname', new StringLength(array(
                      'max' => 255,
                      'min' => 0,
                      'messageMaximum' => 'Your extension name is too long',
                      'messageMinimum' => 'Your last extension name must be atleast 0 character long'
                )))
                ->add('address', new StringLength(array(
                      'max' => 255,
                      'min' => 1,
                      'messageMaximum' => 'Your address is too long',
                      'messageMinimum' => 'Your last address must be atleast 1 character long'
                )));

                $validation->setFilters('fname', 'trim');
                $validation->setFilters('mname', 'trim');
                $validation->setFilters('lname', 'trim');
                $validation->setFilters('extname', 'trim');
                $validation->setFilters('address', 'trim');
                $validation->setFilters('phone', 'trim');

                $messages = $validation->validate($_POST);
                $errMessage = null;

                if (count($messages)) {
                    foreach ($messages as $message) {
                        $errMessage .= '<li>'.$message. '</li>';
                    }
                    $response['success'] = false;
                    $response['message'] = '
                        <div class="alert alert-danger alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>Please fix the following:</strong>'.$errMessage.'
                        </div>
                    ';
                }else{
                    $volunteer->title = $this->request->getPost('title', 'striptags');
                    $volunteer->fname = $this->request->getPost('fname', 'striptags');
                    $volunteer->mname = $this->request->getPost('mname', 'striptags');
                    $volunteer->lname = $this->request->getPost('lname', 'striptags');
                    $volunteer->extname = $this->request->getPost('extname', 'striptags');
                    $volunteer->address = $this->request->getPost('address', 'striptags');
                    $volunteer->phone = $this->request->getPost('phone', 'striptags');
                    if($volunteer->save()){
                        $response['success'] = true;
                        $response['message'] = "
                            <div class='alert alert-success alert-dismissible' role='alert'>
                              <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                              Profile successfully updated. <a href='/myaccount'>Back to My Account</a>
                            </div>
                        ";
                    } else {
                        $response['success'] = false;
                        $response['message'] = '
                            <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              There seems to be a problem while processing your request. Please try again later.
                            </div>
                        ';
                    }
                }

                echo json_encode($response);
            }
        }
    }
}