
            <?php if(empty($page->items)){ ?>
              <span>No partners found</span>
            <?php } ?>
            <?php foreach ($page->items as $ann) { ?>
              <h4 class="fontNormal"><a href="/partners/view/<?php echo $ann['partnerID'] ?>"><?php echo $ann['partnerName'] ?></a></h4>
              <?php echo $ann['partnerInfo'] ?>
              <hr />
            <?php } ?>

            <div class="">
              <!-- Footer goes here -->
              {% if page.total_pages > 1 %}   
              <ul class="pagination pull-right">
                {% if page.current != 1 %}
                <li>{{ link_to("partners?page=" ~ page.before, 'Prev') }}</li>
                {% endif %}

                {% for index in 1..page.total_pages %}
                {% if page.current == index %}
                <li>{{ link_to("partners?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                {% else %}
                <li>{{ link_to("partners?page=" ~ index, index) }}</li>
                {% endif %}
                {% endfor %}         

                {% if page.current != page.total_pages %}                 
                <li>{{ link_to("partners?page=" ~ page.next, 'Next') }}</li>
                {% endif %}
              </ul>
              <div class="clearfix"></div>
              {% endif %}
            </div>