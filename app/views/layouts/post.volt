<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>Ang Bayan Ko</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>

  <!-- Nomarlize -->
  {{ stylesheet_link('css/normalize.css') }}
  {{ stylesheet_link('css/bootstrap.css') }}
  {{ stylesheet_link('css/prettyPhoto.css') }}
  {{ stylesheet_link('css/flexslider.css') }}
  {{ stylesheet_link('css/font-awesome.css') }}
  {{ stylesheet_link('css/font.css') }}
  {{ stylesheet_link('css/slider.css') }}
  {{ stylesheet_link('css/refineslide.css') }}
  {{ stylesheet_link('css/style.css') }}
  <!-- Stylesheet for Color 
  <link href="style/blue.css" rel="stylesheet">
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon/favicon.ico">
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<!-- Header starts -->
  <header class="inner-header">
    <div class="container">
      <div class="row">

          <div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
              <div class="modal-dialog">
                <div class="modal-content border-flat">
                  <form method="post" id="loginModalForm">
                  <input type="hidden" name="loginFormActive" value="1">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h4 class="modal-title" id="townAlbumModalTitle">Login</h4>
                    </div>
                    <div id="loginModalBody" class="modal-body">
                      <div id="loginErrorMessage"></div>
                      <label>Username</label>
                      <input type="text" name="username" class="form-control border-flat" placeholder="Username">
                      <br />
                      <label>Password</label>
                      <input type="password" name="password" class="form-control border-flat" placeholder="Password">
                      <br />
                      <a class="" href="/myaccount/forgotpassword">I forgot my password</a>
                    </div>
                    <div class="modal-footer" id="modal-footer">
                      <!-- <label class="pull-left"><input type="checkbox" name="rememberMe"> Remember Me</label> -->
                      <button type="button" class="btn btn-default border-flat" data-dismiss="modal" aria-hidden="true">Cancel</button>
                      <input id="loginModalBtn" type="submit" name="login" class="border-flat btn btn-primary" value="Login">
                    </div>
                  </form>
                </div>
              </div>
            </div>

          <div id="tellafriendModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tellafriendModal" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
              <div class="modal-content border-flat">
                <form method="post" id="tellafriendModalForm">
                  <input type="hidden" name="tellfriendFormActive" value="1">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="townAlbumModalTitle">Tell A Friend</h4>
                  </div>
                  <div id="tellafriendModalBody" class="modal-body">
                    <div id="tellFriendErrorMessage"></div>
                    <label>Email</label>
                    <input type="text" name="email" class="form-control border-flat" placeholder="Your friend's email">
                    <br />
                    <label>Your Message</label>
                    <textarea name="message" class="form-control border-flat limitChar" maxlength="200" placeholder="Your message"></textarea>
                    <div class="maxlength"></div>
                    <br />
                    <strong>http://angbayanko.org/</strong><br />
                    <small>ABK URL will be attached along with your message</small>
                  </div>
                  <div class="modal-footer" id="modal-footer">
                    <button type="button" class="btn btn-default border-flat" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <button type="submit" id="tellFriendModalBtn" class="border-flat btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div class="col-md-12 upper-mini-nav">
             {{ form('search') }}
            <ul>
              <li><a href="/about">About ABK</a></li>
              <li><a href="/forum">Forum</a></li>
              <li><a href="/contactus">Contact ABK</a></li>
              <li><a href="#tellafriendModal" data-toggle="modal">Tell a Friend</a></li>
              <li>
                <?php
                if(!empty($abk_vol_username)){
                  echo '<a href="/myaccount" style="padding:0"><i class="icon-user"></i> <strong>'.$abk_vol_username.'</strong></a> | <a style="padding-left:0" href="'.$this->url->get().'index/logout">Logout</a>';
                }else{
                  echo '<a href="#loginModal" data-toggle="modal">Login</a>';
                }
                ?>
                </li>
              <li>
                <input type="text" name="keyword" class="searchinput" placeholder="Search" />
                <input type="submit" name="search" class="btnSearch" value="Search">
              </li>
            </ul>
          </form>  
          </div>       
      </div>
    </div>
    <div class="container">
      <div class="row inner-logo">
        <div class="inner-mainlogo pull-left"><a href="{{ url() }}"><img src="{{ url('img/inner-logo.png')}}" /></a></div>
        <div class="inner-txtlogo pull-left"><div class="inner-textlogo"></div></div>
               <div class="navbar-header">
                 <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                 </button>
               </div>        
        <div class="inner-menu pull-left collapse navbar-collapse bs-navbar-collapse">
                    <ul>
                      <li class="dm"><a href="{{ url('programs/page/' ~ prog_menu1['url']) }}">{{ prog_menu1['title'] }}</a></li>
                      <li class="cp"><a href="{{ url('programs/page/' ~ prog_menu2['url']) }}">{{ prog_menu2['title'] }}</a></li>
                      <li class="he"><a href="{{ url('programs/page/' ~ prog_menu3['url']) }}">{{ prog_menu3['title'] }}</a></li>
                      <li class="el"><a href="{{ url('programs/page/' ~ prog_menu4['url']) }}">{{ prog_menu4['title'] }}</a></li>
                      <li class="ee"><a href="{{ url('programs/page/' ~ prog_menu5['url']) }}">{{ prog_menu5['title'] }}</a></li>
                      <li class="ep"><a href="{{ url('programs/page/' ~ prog_menu6['url']) }}">{{ prog_menu6['title'] }}</a></li>
                    </ul>         
        </div>
      </div>
    </div>
  </header>
<!-- Programs and Announcements-->
<div class="container">
    <div class="row">
      <div class="inner-bread-crumbs">{{ bread_crumbs }}</div>  
	  {{ content() }}
      <div class="col-md-3 col-sm-3 col-xs-5 sidebar pull-left">
        <div class="connectwithus">
          <a href="{{ specialPage1['pageUrl'] }}">
            <div class="connectTiles left-tile sscp">
              <div class="connectTitle-Title" style="background-color: rgb(8, 74, 178);">{{ specialPage1['pageTitle'] }}</div>
            </div>
          </a>
          <a href="{{ url('towns') }}">
            <div class="connectTiles right-tile swhh">
              <div class="connectTitle-Title" style="background-color: rgb(99, 47, 0);">See What's Happening in your Hometown</div>
            </div>
          </a>
          <a href="{{ specialPage3['pageUrl'] }}">
            <div class="connectTiles left-tile bottom-tile rmd">
              <div class="connectTitle-Title" style="background-color: rgb(210, 71, 38);">{{ specialPage3['pageTitle'] }}</div>
            </div>
          </a>
          <a href="{{ specialPage4['pageUrl'] }}">
            <div class="connectTiles right-tile bottom-tile bsc">
              <div class="connectTitle-Title" style="background-color: rgb(62, 85, 109);">{{ specialPage4['pageTitle'] }}</div>
            </div>
          </a>
        </div>
        <div class="social-links">
          <ul class="social-list">
            <li class="social-icons"><a href="https://www.facebook.com/Ang-Bayan-Ko-Foundation-1664040513867744/" target="_blank" class="fb"></a></li>
            <li class="social-icons"><a href="https://plus.google.com/u/0/101853314610354158387" target="_blank" class="gplus"></a></li>
            <li class="social-icons"><a href="https://twitter.com/abk_foundation" target="_blank" class="twitter"></a></li>
            <li class="social-icons"><a href="https://www.linkedin.com/in/ang-bayan-ko-foundation-7a33a6113" target="_blank" class="in"></a></li>
            <li class="social-icons"><a href="#" class="pi"></a></li>
            <li class="social-icons sc-last"><a href="https://www.youtube.com/channel/UCA9g-GNgrI0Eq28lMt7g0Iw" target="_blank" class="youtube"></a></li>
          </ul>        
        </div>

        <a href="{{ url('forum') }}">
          <div class="joinforum">
            <div class="forum-icon"></div>
            <h2>Join the discussions at the Forums</h2>
          </div>
        </a>

        <div style="clear:both"></div>
        <div id="side-donation">
          <a href="/donate"><img height="74" width="307" src="{{ url('img/paypal.jpg')}}"></a>
        </div>

        <?php if(count($announcements) > 0){ ?>
        <div class="announcements">
          <h5>Announcements</h5>
          <?php
          foreach ($announcements as $ann) { ?>
          <div class="annList">
          <a href="/announcements/view/<?php echo $ann['annID'] ?>" class="pull-left ann-title"><?php echo $ann['annTitle'] ?></a>
            <div class="pull-right"><?php echo date("m-d-Y", $ann['annDate']) ?></div> 
            <div class="pull-left">
              <p>
                <?php echo $ann['annDesc'] ?>
              </p>
            </div>
          </div>
          <?php } ?>
          <div class="annViewAll pull-right">
            <a href="/announcements">View all announcements</a>
          </div>                                 
        </div>
        <?php } ?>

      </div>            
    </div>
</div>

<!-- Programs and Announcements Ends-->

<div class="container section-feature ">
  <div class="row">
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
  </div>
</div>

<!-- Footer starts -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
            <div class="row">
              <div class="col-md-3 contact-us-footer pull-left">
                  <h3>Contact Us</h3>
                  <strong>Philippines:</strong> <br/>
                  <i class="icon-home"></i> 123, Some Area. Philippines. <br/>
                  <i class="icon-phone"></i> +239-3823-3434 <br/>
                  <i class="icon-envelope-alt"></i> <a href="mailto:#">someone@company.com</a> <br/> <br/>

                  <strong>United States of America:</strong> <br/>
                  <i class="icon-home"></i> 123, Some Area. Los Angeles, CA. <br/>
                  <i class="icon-phone"></i> +239-3823-3434 <br/>
                  <i class="icon-envelope-alt"></i> <a href="mailto:#">someone@company.com</a> <br/>
              </div>

              <div class="col-md-3 more-info-footer pull-left">
                  <h3>More Info</h3>
                  <ul class="more-info">
                    <li> <a href="#">Endorsements</a> </li>
                    {{ moreInfoLinks }}
                  </ul>
              </div>

              <div class="col-md-3 programs-footer pull-left">
                  <h3>Programs</h3>
                  <ul class="more-info">                
                    {{ programLinks }}
                  </ul>
              </div>
              <div class="col-md-3 others-footer pull-left">
                  <h3>Others</h3>
                  <ul class="more-info">               
                    {{ specialPagesLinks }}
                    <li> <a href="#">e-Newletter</a> </li>
                    <li> <a href="#">Joing the Forums</a> </li>
                  </ul>
                </div>      
              </div>
            </div>
            <!-- Copyright info -->
            <p class="copy">Copyright &copy; 2014 | <a href="#">AngBayanKo Site</a> - <a href="#">Home</a> </p>
      </div>
    </div>
  <div class="clearfix"></div>
  </div>
</footer> 	

<!-- Footer ends -->

<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 

<!-- JS -->
{{ javascript_include('js/jquery.js') }}
{{ javascript_include('js/bootstrap.js') }}
{{ javascript_include('js/jquery.isotope.js') }}
{{ javascript_include('js/jquery.prettyPhoto.js') }}
{{ javascript_include('js/filter.js') }}

{{ javascript_include('js/jquery.flexslider-min.js') }}
{{ javascript_include('js/sparkjquery.csliderlines.js') }}
{{ javascript_include('js/modernizr.custom.28468.js') }}

{{ javascript_include('js/jquery.carouFredSel-6.1.0-packed.js') }}
{{ javascript_include('js/jquery.refineslide.min.js') }}
{{ javascript_include('js/jquery.backgroundSize.js') }}
{{ javascript_include('js/respond.src.js') }}
{{ javascript_include('js/jquery.maxlength.min.js') }}
{{ javascript_include('js/custom.js') }}

</body>
</html>