
<div class="inner-post pull-left">
   <div class="inner-news-content">
      {{ content() }}
      {% if postNone %}
      <div class="entry">
         <h2><a href="{{ url('post/news/' ~ post.postSlug) }}">{{ post.postTitle }}</a></h2>
         <!-- Meta details -->

         <div class="meta">
            <div><i class="icon-calendar"></i> {{ date('d-m-Y', post.postPublishDate) }}</div>
            <!-- Load Facebook SDK for JavaScript -->

             <div id="fb-root"></div>
             <script>(function(d, s, id) {
               var js, fjs = d.getElementsByTagName(s)[0];
               if (d.getElementById(id)) return;
               js = d.createElement(s); js.id = id;
               js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1";
               fjs.parentNode.insertBefore(js, fjs);
             }(document, 'script', 'facebook-jssdk'));</script>

             <!-- Your like button code -->
             <div>
                <div style="float:left">
                      <div class="fb-like"  data-href="<?=$_SERVER['REQUEST_URI']?>" data-layout="button_count" data-action="like"  data-show-faces="false"></div>
                      &nbsp;&nbsp;&nbsp;
               </div>

                <div style="float:left">
                  <a style="margin-bottom:-10px !important;" href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
               </div>
             </div>
             
            <!-- <span class="pull-right"><i class="icon-comment"></i> <a href="#">2 Comments</a></span> -->
         </div>

         <!-- Thumbnail -->
         {% if post.postFeatureImage %}
         <div class="bthumb3">
            <a href="#"><img src="{{ post.postFeatureImage }}" alt="" class="img-responsive"></a>
         </div>
         {% endif %}
         <!-- Para -->
         {{ post.postContent }}

         <!-- Read more -->
         <div class="button"><a href="#">Read More...</a></div>
      </div> 
      {% endif %}

      <div class="col-sm-12">
         <div id="disqus_thread"></div>
         <script>
         /**
         * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
         * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
         */
         /*
         var disqus_config = function () {
         this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
         this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
         };
         */
         (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');

            s.src = '//angbayankofoundation.disqus.com/embed.js';

            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
         })();
      </script>
      <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
   </div>
</div>
</div> 