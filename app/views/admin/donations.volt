
            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> Donations</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="{{ url('admin') }}"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <span>Donations</span>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->

            <div class="matter">


{{ form('admin/donations', 'class': 'form-horizontal', 'id':'main-table-form') }}
  <div class="container">

<!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>

      <div class="form-group">
        <label class="col-lg-1 control-label">Search</label>
        <div class="col-lg-4">
              {{ text_field('search_text' , 'class':'form-control') }}
          </div>
        <div class="col-lg-6">
          {{ submit_button('Search', 'class':'btn btn-default') }}
           <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
        </div>

      </div>  
      
      <div class="col-md-12">
        {{ content() }}
        
        
        <div class="widget">

          <div class="widget-head">
            <div class="pull-left">Donations List</div>
            <div class="clearfix"></div>
          </div>


          <div class="widget-content">
            {{ hidden_field('csrf', 'value': security.getToken())}}
            <input type="hidden" class="tbl-action" name="action" value=""/>
            <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
            <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>

            

            <table class="table table-striped table-bordered table-hover tblusers">
              <thead>
                <tr>
                  <th width="10">{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th>
                  <th width="100">Transaction ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th width="100">Amount</th>
                  <th width="200">Date</th>
                  <th width="10"></th>
                </tr>
              </thead>
              <tbody>

                <?php
                if($page->items > 0){
                  foreach ($page->items as $key => $value) {
                    ?>

                    <tr>
                      <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $value['txn_id'] ?>"></td>
                        <td class="name">
                          <?php echo $value['txn_id'] ?>
                        </td>
                        <td><?php echo $value['fname'].' '.$value['lname'] ?></td>
                        <td><?php echo $value['email'] ?></td>
                        <td><?php echo $value['phone'] ?></td>
                        <td><?php echo $value['amount'] ?></td>
                        <td><?php echo date("F j, Y", $value['date']) ?></td>
                        <td>
                            <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $value['txn_id'] ?>"><i class="icon-remove"></i> </a>
                        </td>
                    </tr>

                <?php
                  }
                }else{
                  echo '
                    <tr>
                        <td colspan="8">No suggested programs found</td>
                    </tr>
                  ';
                }
                ?>
              </tbody>
              </table>

              <div class="tblbottomcontrol" style="display:none">
                <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
                <a href="#" class="tbl_unselect_all"> Unselect </a>
              </div>

                <div class="widget-foot">

                  {% if page.total_pages > 1 %}
                 
                  <ul class="pagination pull-right">

                    {% if page.current != 1 %}
                    <li>{{ link_to("admin/donations/pages?page=" ~ page.before, 'Prev') }}</li>
                    {% endif %}

                    {% for index in 1..page.total_pages %}
                    {% if page.current == index %}
                    <li>{{ link_to("admin/donations/pages?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                    {% else %}
                    <li>{{ link_to("admin/donations/pages?page=" ~ index, index) }}</li>
                    {% endif %}
                    {% endfor %}         

                    {% if page.current != page.total_pages %}                 
                    <li>{{ link_to("admin/donations/pages?page=" ~ page.next, 'Next') }}</li>
                    {% endif %}
                  </ul>
                  {% endif %}
                  
                  <div class="clearfix"></div> 
                </div>


              </div>

            </div>

          </div>

        </div>



              </div>
    </form>

</div>

<!-- Matter ends -->