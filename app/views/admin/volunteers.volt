
            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    {% if viewlist==false %} <strong>{{ volunteer.inqSubject }}</strong>{% endif %}
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> Volunteer List</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="{{ url('admin') }}"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <a href="/admin/volunteers" class="bread-current">Volunteers</a>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->

            <div class="matter">

              {% if viewlist == true %}

                {{ form('admin/volunteers', 'id':'main-table-form') }}
                <div class="container">

                  <div class="row">
                    <div class="col-md-2">
                      <label>Search by Text</label>
                    </div>
                    <div class="col-md-6 form-group">
                      <div class="col-md-12">
                        {{ text_field('search_text' , 'class':'form-control') }}
                      </div>
                    </div>
                    <!-- <div class="col-md-1">
                      <label>Filter</label>
                    </div>
                    <div class="col-md-3 form-group">
                      <div class="col-md-12">
                        <select name="searchstat" class="form-control">
                          <option value="status">-Status-</option>
                          <option value='0'>New</option>
                          <option value='1'>Reviewed</option>
                        </select>
                      </div>
                    </div> -->
                    <div class="col-md-2 form-group">
                      {{ submit_button('Search', 'class':'btn btn-default') }}
                      <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                    </div>
                  </div>

                  <!-- Table -->
                  <div class="row">
                    <div class="col-md-2">
                      <label>Filter by Date</label>
                    </div>
                    <div class="col-md-3 form-group" id="fromDatepicker" class="input-append">
                      <label class="col-md-2 control-label">From</label>
                      <div class="col-md-10">
                        {{ text_field('fromDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
                        <span class="add-on">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                        </span>
                      </div>
                    </div>
                    <div class="col-md-3 form-group" id="toDatepicker" class="input-append">
                      <label class="col-md-2 control-label">To</label>
                      <div class="col-md-10">
                        {{ text_field('toDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
                        <span class="add-on">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                        </span>
                      </div>
                    </div>
                    <div class="col-md-2">
                      {{ submit_button('Filter', 'class':'btn btn-default') }}
                    </div>
                  </div>

                  <!-- Table -->
                  <div class="row">
                    <div class="padd">
                      <div class="col-lg-12">
                        {{ content() }} 
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    
                    <!-- <div class="form-group">
                      <div class="col-lg-8">
                        <div class="form-group">
                          <label class="control-label col-lg-3">Search</label>
                          <div class="col-lg-4"> 
                            {#{ text_field('search_text' , 'class':'form-control') }#}
                          </div>
                          <label class="control-label col-lg-3">Filter</label>
                          <div class="col-lg-4"> 
                            <select name="volFilter" class="form-control">{#{ activityOptions }#}</select>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-4">
                        {#{ submit_button('Search', 'class':'btn btn-default') }#}
                        <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                      </div>
                    </div>  -->

                    <div class="col-md-12">
                      
                      
                      <div class="widget">

                        <div class="widget-head">
                          <div class="pull-left">Volunteers</div>
                          <div class="widget-icons pull-right">
                            <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                            <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                          </div>  
                          <div class="clearfix"></div>
                        </div>


                        <div class="widget-content">
                          {{ hidden_field('csrf', 'value': security.getToken())}}
                          <input type="hidden" class="tbl-action" name="action" value=""/>
                          <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                          <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>

                          

                          <table class="table table-striped table-bordered table-hover tblusers">
                            <thead>
                              <tr>
                                <th>{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th>
                                <th><a href="?sort={{ nameHref }}">Name <i class="{{ nameIndicator ? nameIndicator : "" }}"></i></a></th>
                                <th><a href="?sort={{ usernameHref }}">Username <i class="{{ usernameIndicator ? usernameIndicator : "" }}"></i></a></th>
                                <th><a href="?sort={{ emailHref }}">Email <i class="{{ emailIndicator ? emailIndicator : "" }}"></i></a></th>
                                <th><a href="?sort={{ phoneHref }}">Phone <i class="{{ phoneIndicator ? phoneIndicator : "" }}"></i></a></th>
                                <th><a href="?sort={{ dateaddedHref }}">Date Registered <i class="{{ dateaddedIndicator ? dateaddedIndicator : "" }}"></i></a></th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
   
                              <?php
                              if($page->items){
                                foreach ($page->items as $key => $value) {
                                  ?>

                                  <tr>
                                    <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $value['volunteerID'] ?>"></td>
                                      <td class="name"><?php echo $value['name'] ?>
                                      </td>
                                      <td><?php echo $value['username'] ?></td>
                                      <td><?php echo $value['email'] ?></td>
                                      <td><?php echo $value['phone'] ?></td>
                                      <td><?php echo date("F j, Y", $value['dateAdded']) ?></td>

                                      <td>
                                          <a href="/admin/volunteers/<?php echo $value['volunteerID'] ?>" class="btn btn-xs btn-success"><i class="icon-ok"></i> </a>

                                          <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $value['volunteerID'] ?>"><i class="icon-remove"></i> </a>
                                      </td>
                                  </tr>

                              <?php
                                }
                              }else{
                                echo '
                                  <tr>
                                      <td colspan="6">No volunteers found</td>
                                  </tr>
                                ';
                              }
                              ?>
                            </tbody>
                            </table>

                            <div class="tblbottomcontrol" style="display:none">
                              <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
                              <a href="#" class="tbl_unselect_all"> Unselect </a>
                            </div>

                              <div class="widget-foot">

                                {% if page.total_pages > 1 %}
                               
                                <ul class="pagination pull-right">

                                  {% if page.current != 1 %}
                                  <li>{{ link_to("admin/volunteers?page=" ~ page.before, 'Prev') }}</li>
                                  {% endif %}

                                  {% for index in 1..page.total_pages %}
                                  {% if page.current == index %}
                                  <li>{{ link_to("admin/volunteers?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                  {% else %}
                                  <li>{{ link_to("admin/volunteers?page=" ~ index, index) }}</li>
                                  {% endif %}
                                  {% endfor %}         

                                  {% if page.current != page.total_pages %}                 
                                  <li>{{ link_to("admin/volunteers?page=" ~ page.next, 'Next') }}</li>
                                  {% endif %}
                                </ul>
                                {% endif %}
                                
                                <div class="clearfix"></div> 

                              </div>

                            </div>

                          </div>


                        </div>

                      </div>


                    </div>
                  </form>
                {% else %}
                  <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="widget">

                        <div class="widget-head">
                          <div class="pull-left">Volunteer Profile</div>
                          <div class="widget-icons pull-right">
                            <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                            <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                          </div>  
                          <div class="clearfix"></div>
                        </div>

                        <div class="widget-content">
                          <div class="padd">
                          <!-- <?php // echo $volunteer->fname ."sadfsad" ?> -->

                            {% if(volunteer == false) %}
                              <div class="alert alert-danger">Volunteer not found</div>
                            {% else %}
                              <div>
                                Name: <strong>{{ volunteer.title }} {{ volunteer.fname }} {{ volunteer.mname }} {{ volunteer.lname }} {{ volunteer.extname }}</strong>
                              </div>
                              <div>
                                Username: <strong>{{ volunteer.username }}</strong>
                              </div>
                              <div>
                                Address: <strong>{{ volunteer.address }}</strong>
                              </div>
                              <div>
                                Email: <strong>{{ volunteer.email }}</strong>
                              </div>
                              <div>
                                Phone: <strong>{{ volunteer.phone }}</strong>
                              </div>
                              <div>
                                Date registered: <strong><?php echo date("F j, Y", $volunteer->dateAdded) ?></strong>
                              </div>

                              
                              {{ volunteerActivities }}
                              {{ volunteerSuggActivities }}
                              

                            {% endif %}
                          </div>
                        </div>

                        <div class="widget-foot">
                          {{ form('admin/volunteers', 'class': 'form-horizontal', 'id':'main-table-form') }}
                            {{ hidden_field('csrf', 'value': security.getToken())}}
                            <input type="hidden" class="tbl-action" name="action" value=""/>
                            <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                            <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
                            {{ link_to('admin/volunteers', 'Back to volunteer list', 'class':'btn btn-default') }}
                            <!-- <a href="#modalPrompt" class="btn btn-warning tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="{# volunteer.volunteerID #}">Delete </a> -->
                          </form>
                        </div>  
                      </div> 
                    </div>
                  </div>  
                  </div>
                {% endif %}

              </div>

<!-- Matter ends -->