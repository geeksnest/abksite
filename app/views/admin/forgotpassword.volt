<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>ABK CMS - LOGIN</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  {{ stylesheet_link('css/bootstrap.css') }}
  {{ stylesheet_link('css/font-awesome.css') }}
  {{ stylesheet_link('css/abkadmin/style.css') }}
  {{ stylesheet_link('css/bootstrap-responsive.css') }}
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="{{url.getBaseUri()}}public/img/favicon/favicon.png">
</head>

<body>


<!-- Form area -->
<div class="admin-form">
  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <!-- Widget starts -->
        <div class="logoHead worange">
          <img src="{{ url.getBaseUri() }}img/inner-logo.png">
        </div>
        <div class="textHead worange">
          <img src="{{ url.getBaseUri() }}img/abktxtlogolonglogin.png">
        </div>        
            <div class="widget worange">
              <!-- Widget head -->
              <div class="widget-head">
                <i class="icon-lock"></i> Forgot Password 
              </div>

              <div class="widget-content">
                <div class="padd">
                  {{ content() }}
                  <!-- Forgot Password form -->
                  {{ form('admin/forgotpassword', 'class': 'form-horizontal') }}
                    <!-- Email -->
                    <div class="form-group">
                      <label class="control-label col-lg-3" for="inputEmail">Email</label>
                      <div class="col-lg-9">
                        {{ text_field('email', 'size': "30", 'class': "form-control", 'id': "inputEmail", 'placeholder': 'Email') }}
                        {{ emailError }}
                      </div>
                    </div>
                    <!-- Remember me checkbox and sign in button -->

                        <div class="col-lg-9 col-lg-offset-2">
                        {{ submit_button('Send Password Reset', 'class': 'btn btn-danger') }}
            </div>
                    <br />
                  </form>
          
        </div>
                </div>
            </div>  
      </div>
    </div>
  </div> 
</div>    
<!-- JS -->
{{ javascript_include('js/jquery.js') }}
{{ javascript_include('js/bootstrap.js') }}
</body>
</html>