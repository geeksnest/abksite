<!-- Modal View-->
            <div id="modalView" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
              <div class="modal-dialog" style="width: 800px;">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">View Content</h4>
                  </div>
                  <div class="modal-body" style=" height: 400px; overflow:scroll;">
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
                </div>
              </div>
            </div>

<?php $token = $this->security->getToken(); ?>
<div class="page-head">

  <h2 class="pull-left">
    <span class="page-meta">Pages</span>
  </h2>

  <div class="bread-crumb pull-right">
    <a href="/admin"><i class="icon-home"></i> Home</a> 
    <span class="divider">/</span> 
     <a href="/admin/pages"> Pages</a> 
    <span class="divider">/</span> 
    <a href="" class="bread-current">Edit</a>          
  </div>
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
  {{ content() }}
  {{ form('admin/editpage/'~pageInfo.pageID, 'class': 'form-horizontal') }}
    <!--start row-->
    <div class="row">
      <div class="col-md-8">
        <div class="widget">
          <div class="widget-head">
            Edit page
          </div>

          <div class="widget-content">
            <div class="padd">

              

              <label>Page Title</label>{{ titleError }}
              <div class="form-group">
                <div class="col-lg-12">
                  {{ hidden_field('hpage_title', 'name':'hpage_title', 'value': pageInfo.pageTitle) }}
                  {{ text_field('page_title', 'name':'page_title' , 'placeholder':'Enter title', 'class':'form-control', 'value': pageInfo.pageTitle) }}
                  {{ hidden_field('hpage_slug', 'id':'hpage_slug', 'value':pageInfo.pageSlug) }}
                  <label>Slug:</label> <span id="page-slug">{{ pageInfo.pageSlug }}</span>
                </div>
              </div>

              <label>Page Keyword</label>
              <div class="form-group">

                <div class="col-lg-12">
                  {{ text_field('page_keyword', 'class':'form-control', 'placeholder':'Enter Keywords separated by comma. Ex. disaster, philippine climate change, philippine emergency', 'value':pageInfo.pageKeywords) }}
                </div>
              </div>


              <label>Page Content</label>  
              {{ text_area('programPageText'~pageInfo.pageID, 'name':'page_content' , 'placeholder':'Enter content', 'class':'form-control programPageText', 'value':pageInfo.pageContent) }}
              
              <em>Last Updated: {{ date("F j, Y h:i:s a", pageInfo.pageLastUpdated) }}</em>

              <br /><br />
              <input type="checkbox" name="page_active" value="1" <?php echo $pageInfo->pageActive==1?'checked':'' ?>> <label>Page Active</label>
              <!-- {{ check_field('page_active', 'value': '1', 'checked':'checked') }} <label>Page Active</label> -->

              <br/> <br/>
              {{ submit_button("save_page", 'class':'btn btn-primary', 'value':'Save Changes', 'name':'save_page' ) }}
              <button type="button" class="btn btn-default" onclick="window.location='/admin/pages'">Cancel</button>

              <a href="#modalView" data-toggle="modal" type="button" class="btn btn-info view-page-sample" data-pageid="{{ pageInfo.pageID }}">Info</a>


              <input type="hidden" name="csrf"value="<?php echo $token ?>"/>


          </div>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="widget">
        <div class="widget-head">
          Options      
        </div>

        <div class="widget-content">
          <div class="padd">

            <div class="clearfix"></div>

            <div class="form-horizontal">
              <div class="form-group">

                <div class="col-lg-7">
                  <label> Page Order</label>
                </div>
                <div class="col-lg-5">
                  {{ numeric_field('page_order', 'placeholder':'0', 'class':'form-control', 'value':pageInfo.pageOrder,'min':'0') }}
                </div>
              </div>
            </div>


            <button type="button" class="btn btn-default pull-right" id="removeSideBarImg" style="<?php echo empty($pageInfo->sidebarImg)?'display:none':'' ?>">Remove Image</button>
            <div class="clearfix"></div>

          </div>
        </div>

      </div>
    </div>


    <div class="col-md-4">
      <div class="widget">
        <div class="widget-head">
          <div class="pull-left">Page Banner</div>
          <div class="clearfix"></div>
        </div>
        <div class="widget-content">
          <div class="padd">
              <div class="form-group">
                <div class="col-lg-12">
                  <div id="pageBannerImgWrapper">
                    <?php
                    echo !empty($pageInfo->pageBanner)?'<img src="'.$pageInfo->pageBanner.'" style="width: 100%">':'';
                    ?>
                  </div>
                  <!-- <input type="text" id="pageBannerUrl" name="page_banner" class="form-control" placeholder="Place banner link here."> -->

                  {{ text_field('pageBannerUrl', 'class':'form-control', 'placeholder':'Place banner link here.', 'value':pageInfo.pageBanner) }}
                </div>
              </div>

                <button type="button" class="btn btn-default pull-right" id="removeBanner" style="<?php echo empty($pageInfo->pageBanner)?'display:none':'' ?>">Remove Banner</button>
                <!--<input type="submit" name="updatebanner" value="Update Banner" class="btn btn-primary pull-right"> -->

                <div class="clearfix"></div>
            </div>
            <div class="widget-foot"></div>
          </div>
        </div>
      </div>

    </div><!--end row-->






    <div class="row"><!--start row-->
      <div class="col-lg-12">
        <div class="widget">
          <div class="widget-head">
          <div class="pull-left">Digital Assets (<span id="fileCount"><?php echo count($pictures)?></span> files)</div>
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
            <div class="padd">
              <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Add files...</span>
                <!-- The file input field used as target for the file upload widget -->
                <input id="pagespictures" type="file" name="files[]" multiple accept="image/*">
              </span>
              <!-- The global progress bar -->
              <div id="progress" class="progress">
                <div class="progress-bar progress-bar-success"></div>
              </div>
              <div class="gallery" id="pages-gallery">
                {% for img in pictures %}
                <div class="program-digital-assets-library pull-left" style="position: relative">
                  <a href="{{ url(img.imgpath~img.fileName) }}" class='prettyPhoto[pp_gal]'>
                    <img src="{{ url(img.imgpath~img.fileName) }}" alt=""></a>
                    <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="{{ url(img.imgpath~img.fileName) }}">
                    <button type="button" class="btn btn-xs btn-danger delete-recent-upload-page-pic" data-picture-id="{{ img.imgID }}"  style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
                  </div>
                  {% endfor %}
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="widget-foot">
              </div>
            </div>
          </div>  
        </div>       
      </div><!--end row-->



      </form>
    </div><!--end container-->
</div><!--end matter-->