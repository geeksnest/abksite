          <input type="hidden" id="townID" value="{{ townID }}">
          <div id="createAlbumModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title" id="townAlbumModalTitle">Create Album</h4>
                </div>
                <div class="modal-body">
                  <div id="albumResult"></div>
                  <label>Album Name</label>
                  <input type="hidden" id="editAlbumID">
                  <input type="hidden" id="origAlbumName">
                  <input type="text" name="albumName" id="albumName" placeholder="Album Name" class="form-control">
                </div>
                <div class="modal-footer" id="modal-footer">
                  <span id="createAlbumClose">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  </span>
                  <button type="button" id="createTownAlbumBtn" class="btn btn-primary">Save Changes</button>
                </div>
              </div>
            </div>
          </div>

          
          <div id="uploadTownPicture" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" style="width: 900px;">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title" id="townAlbumModalTitle">Upload picture to Album: <strong> {{ albumName }}</strong></h4>
                </div>
                <div class="modal-body" style=" height: 400px; overflow:scroll;">
                    <div id="imageError"></div>
                     <input type="text" class="tbl-recordID" name="albumID" value=""/>
                    <span class="btn btn-success fileinput-button">
                      <i class="glyphicon glyphicon-plus"></i>
                      <span>Add files...</span>
                      <!-- The file input field used as target for the file upload widget -->
                      <input id="townPictures" type="file" name="files[]" multiple accept="image/*">
                    </span>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                      <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <div class="gallery digital-assets-gallery">
                      <span id="selectImg">Please select files to upload</span>
                    </div>
                    <div class="clearfix"></div>
                    
                  </div>
                <div class="modal-footer" id="modal-footer">
                  <span id="createAlbumClose">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <!-- Modal Prompt-->
          <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Alert!</h4>
                </div>
                <div class="modal-body">
                  <p class="modal-message"></p>
                  <span class="modal-list-names"></span>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                  <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                </div>
              </div>
            </div>
          </div>
          <!-- Page heading -->
          <div class="page-head">
            <h2 class="pull-left"><i class="icon-table"></i> Towns</h2>

            <!-- Breadcrumb -->
            <div class="bread-crumb pull-right">
              <a href="index.html"><i class="icon-home"></i> Home</a> 
              <!-- Divider -->
              <span class="divider">/</span> 
              {{ link_to('admin/towns', 'Towns') }}
              <span class="divider">/</span> 
              <span>Gallery</span>
            </div>

            <div class="clearfix"></div>

          </div>
          <!-- Page heading ends -->

          <!-- Matter -->

          <div class="matter">
            {{ form('class': 'form-horizontal', 'id':'main-table-form') }}
            <div class="container">

              <!-- Table -->

              <div class="row">

                <div class="col-md-12">
                  {{ content() }}

                  <h2>{{ town.townName }}</h2>

                  <div class="widget">

                    <div class="widget-head">
                      <div class="pull-left">Towns</div>
                      <div class="widget-icons pull-right">
                      </div>  
                      <div class="clearfix"></div>
                    </div>


                    <div class="widget-content">
                      <div class="padd">
                        {{ townTab }}
                      </div>

                      {{ hidden_field('csrf', 'value': security.getToken())}}
                      <input type="hidden" class="tbl-action" name="action" value=""/>
                      <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                      <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
                      <table class="table table-striped table-bordered table-hover tblusers">
                        <thead>
                          <tr>
                            <th colspan="5">
                              <div class="col-lg-6">
                                <div class="form-group">
                                  <label class="control-label col-lg-3">Search</label>
                                  <div class="col-lg-9"> 
                                    {{ text_field('search_album_text' , 'class':'form-control') }}
                                  </div>
                                </div>
                              </div>

                              <div class="col-lg-6">
                                 <button type="submit" name="searchBtn" class="btn btn-default" value="Clear Search">Seach</button>
                                  <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                                <a id="createTownAlbumLink" href="#createAlbumModal" style="margin-right:25px" class="btn btn-primary pull-right" data-toggle="modal">+ Create New Album</a>
                              </div>
                            </th>
                          </tr>
                          <tr>
                            <th width="10">{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th>
                            <th><a href="?sort={{ nameHref }}">Album Name <i class="{{ nameIndicator ? nameIndicator : "" }}"></i></a></th>
                            <th><a href="?sort={{ picturesHref }}">Pictures <i class="{{ picturesIndicator ? picturesIndicator : "" }}"></i></a></th>
                            <th><a href="?sort={{ dateHref }}">Date Created <i class="{{ dateIndicator ? dateIndicator : "" }}"></i></a></th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>

                          {% if page.total_pages == 0 %}  
                          <tr>
                            <td colspan="5">No albums found</td>
                          </tr>
                          {% else %}
                          {% for post in page.items %}
                          <tr>
                            <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="{{ post['albumID']}}"> </td>
                            <td class="name">{{ link_to('admin/townAlbum/'~townID~'/'~post['albumID'], post['albumName']) }}</td>
                            <td>{{ post['picCount'] }}</td>
                            <td>{{ date("F j, Y", post['dateCreated']) }}</td>
                            <td>
                              <a href="#createAlbumModal" class="btn btn-xs btn-warning editTownAlbumLink" data-toggle="modal" data-album-name="{{ post['albumName'] }}" data-album-id="{{ post['albumID'] }}"><i class="icon-pencil"></i> </a>

                              <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="{{ post['albumID'] }}"><i class="icon-remove"></i> </a>

                           <!--    <a href="#uploadTownPicture" class="btn btn-xs btn-primary modal-control-button" data-toggle="modal" data-recorID="{{ post['albumID'] }}" data-action="picture" name="picturealbum"><i class="icon icon-picture"></i></a> -->

                            </td>
                          </tr>
                          {% endfor %}
                          {% endif %}
                        </tbody>
                      </table>

                      <div class="tblbottomcontrol" style="display:none">
                        <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
                        <a href="#" class="tbl_unselect_all"> Unselect </a>
                      </div>

                      <div class="widget-foot">

                        {% if page.total_pages > 1 %}

                        <ul class="pagination pull-right">

                          {% if page.current != 1 %}
                          <li>{{ link_to("admin/townsgallery/pages?page=" ~ page.before, 'Prev') }}</li>
                          {% endif %}

                          {% for index in 1..page.total_pages %}
                          {% if page.current == index %}
                          <li>{{ link_to("admin/townsgallery/pages?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                          {% else %}
                          <li>{{ link_to("admin/townsgallery/pages?page=" ~ index, index) }}</li>
                          {% endif %}
                          {% endfor %}         

                          {% if page.current != page.total_pages %}                 
                          <li>{{ link_to("admin/townsgallery/pages?page=" ~ page.next, 'Next') }}</li>
                          {% endif %}
                        </ul>
                        {% endif %}
                        <a href="/admin/towns" class="btn btn-default">Back to Town List</a>
                        <div class="clearfix"></div> 

                      </div>

                    </div>

                  </div>


                </div>

              </div>


            </div>
          </form>
        </div>

<!-- Matter ends -->