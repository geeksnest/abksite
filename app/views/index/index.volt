<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>Ang Bayan Ko</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>

  <!-- Nomarlize -->
  {{ stylesheet_link('css/normalize.css') }}
  <!-- Stylesheets -->
  {{ stylesheet_link('css/bootstrap.css') }} 
  <!-- Pretty Photo -->
  {{ stylesheet_link('css/prettyPhoto.css') }}
  <!-- Flex slider -->
  {{ stylesheet_link('css/flexslider.css') }}
  <!-- Font awesome icon -->
  {{ stylesheet_link('css/font-awesome.css') }}
  <!-- Load Fonts -->
  {{ stylesheet_link('css/font.css') }}
  <!-- Parallax slider -->
  {{ stylesheet_link('css/slider.css') }}
  <!-- Refind slider -->
  {{ stylesheet_link('css/refineslide.css') }}
  <!-- Main stylesheet -->
  {{ stylesheet_link('css/style.css') }}
  <!-- Stylesheet for Color 
  {{ stylesheet_link('css/blue.css') }}
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon/favicon.ico">
</head>

<body>

  <!-- Header starts -->
  <header>
    <div class="container">
      <div class="row">

      <div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
              <div class="modal-content border-flat">
                <form method="post" id="loginModalForm">
                  <input type="hidden" name="loginFormActive" value="1">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="townAlbumModalTitle">Login</h4>
                  </div>
                  <div id="loginModalBody" class="modal-body">
                    <div id="loginErrorMessage"></div>
                    <label>Username</label>
                    <input type="text" name="username" class="form-control border-flat" placeholder="Username">
                    <br />
                    <label>Password</label>
                    <input type="password" name="password" class="form-control border-flat" placeholder="Password">
                    <br />
                    <a class="" href="/myaccount/forgotpassword">I forgot my password</a>
                  </div>
                  <div class="modal-footer" id="modal-footer">
                    <!-- <label class="pull-left"><input type="checkbox" name="rememberMe"> Remember Me</label> -->
                    <button type="button" class="btn btn-default border-flat" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <input id="loginModalBtn" type="submit" name="login" class="border-flat btn btn-primary" value="Login">
                  </div>
                </form>
              </div>
            </div>
          </div>

        <div id="tellafriendModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tellafriendModal" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
              <div class="modal-content border-flat">
                <form method="post" id="tellafriendModalForm">
                  <input type="hidden" name="tellfriendFormActive" value="1">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="townAlbumModalTitle">Tell A Friend</h4>
                  </div>
                  <div id="tellafriendModalBody" class="modal-body">
                    <div id="tellFriendErrorMessage"></div>
                    <label>Email</label>
                    <input type="text" name="email" class="form-control border-flat" placeholder="Your friend's email">
                    <br />
                    <label>Your Message</label>
                    <textarea name="message" class="form-control border-flat limitChar" maxlength="200" placeholder="Your message"></textarea>
                    <div class="maxlength"></div>
                    <br />
                    <strong>http://angbayanko.org/</strong><br />
                    <small>ABK URL will be attached along with your message</small>
                  </div>
                  <div class="modal-footer" id="modal-footer">
                    <button type="button" class="btn btn-default border-flat" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <button type="submit" id="tellFriendModalBtn" class="border-flat btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>  


        <div class="col-md-12 upper-mini-nav">
          {{ form('search') }}
            <ul>
              <li><a href="/about" class="aboutabktop">About ABK</a></li>
              <li><a href="/forum">Forum</a></li>
              <li><a href="/contactus">Contact ABK</a></li>
              <li><a href="#tellafriendModal" data-toggle="modal">Tell a Friend</a></li>
              <li>
                <?php
                if(!empty($abk_vol_username)){
                  echo '<a href="/myaccount" style="padding:0"><i class="icon-user"></i> <strong>'.$abk_vol_username.'</strong></a> | <a style="padding-left:0" href="'.$this->url->get().'index/logout">Logout</a>';
                }else{
                  echo '<a href="#loginModal" data-toggle="modal">Login</a>';
                }
                ?>
                </li>
              <li>
                <!-- <div class="search-box"> -->
                <input type="text" name="keyword" id="inputsearch" class="searchinput" placeholder="Search" />
                <input type="submit" id="btnsearch" name="search" class="btnSearch" value="Search">
              <!-- </div> -->
              </li>
            </ul>
          </form>  
        </div>  
             
      </div>
    </div>
    <div class="container">
      <div class="row logo">
        <div class="mainlogo pull-left"><img src="{{ url('img/toppage/abklogo.png')}}" /></div>
        <div class="txtlogo pull-left"><div class="textlogo"></div></div>
        <div class=" texttag pull-left">
          {{ about.content }}         
        </div>
      </div>
    </div>
  </header>


  <!-- Header ends -->
  <!-- Flexslider Header -->
  <div class="header-flex flexslider">
    <ul class="slides">
      {{ slides }}
      <!-- <li>
        <div class="header-images img1">
          <iframe class="youtube-float-center" src="https://www.youtube.com/watch?v=LUdBAgIHJmE" frameborder="0" allowfullscreen></iframe>
        </div>      
      </li>
      <li>
        <div class="header-images img2">

        </div>     
      </li>
      <li>
        <div class="header-images img3">

        </div>     
      </li>
      <li>
        <div class="header-images img4">

        </div>     
      </li> -->
    </ul>    
  </div>
  
  <!-- End Flexslider Header -->

<!-- youtube for smart phone -->
 <div class="youtube-container">
  <center>
    {{ youtubeoutbox }}
  </center>
  </div>     
<!-- end youtube for smart phone -->
  <!-- Programs and Announcements-->
  <div class="container">

    <div class="row">
<div class="programs pull-left">
        <h5 class="section-title">AngBayanKo Programs</h5>
        <div class="programTiles">
          <a href="{{ url('programs/page/' ~ prog1['url']) }}">
            <div class="tile-flex flexslider dm">
              <div class="tile-title tile1">{{ prog1['title'] }}</div>
              <ul class="slides" style="height: 160px">
                <!-- Each slider should be enclosed inside li tag -->
                {% for pi in progimg1 %}
                <li class="slide"><div style="background: url('{{ url('img/programs/' ~ pi) }}') no-repeat center;"></div></li>
                {% endfor %}
              </ul>
              <div class="tile-tagline transparent">{{ prog1['tagline'] }}</div>
            </div>
          </a>
        </div>    
        <div class="programTiles">
          <a href="{{ url('programs/page/' ~ prog2['url']) }}">
            <div class="tile-flex flexslider pc">
              <div class="tile-title tile3">{{ prog2['title'] }}</div>
              <ul class="slides" style="height: 160px">
                <!-- Each slider should be enclosed inside li tag -->
                {% for pi in progimg2 %}
                <li class="slide"><div style="background: url('{{ url('img/programs/' ~ pi) }}') no-repeat center;"></div></li>
                {% endfor %}
              </ul>
              <div class="tile-tagline transparent">{{ prog2['tagline'] }}</div>
            </div> 
          </a>         
        </div>
        <div class="programTiles">
          <a href="{{ url('programs/page/' ~ prog3['url']) }}">
            <div class="tile-flex flexslider hc">
              <div class="tile-title tile2">{{ prog3['title'] }}</div>
              <ul class="slides" style="height: 160px">
                <!-- Each slider should be enclosed inside li tag -->
                {% for pi in progimg3 %}
                <li class="slide"><div style="background: url('{{ url('img/programs/' ~ pi) }}') no-repeat center;"></div></li>
                {% endfor %}
              </ul>
              <div class="tile-tagline transparent">{{ prog3['tagline'] }}</div>
            </div>
          </a>
        </div>  
        <div class="programTiles">
          <a href="{{ url('programs/page/' ~ prog4['url']) }}">
            <div class="tile-flex flexslider el">
              <div class="tile-title tile4">{{ prog4['title'] }}</div>
              <ul class="slides" style="height: 160px">
                <!-- Each slider should be enclosed inside li tag -->
                {% for pi in progimg4 %}
                <li class="slide"><div style="background: url('{{ url('img/programs/' ~ pi) }}') no-repeat center;"></div></li>
                {% endfor %}
              </ul>
              <div class="tile-tagline transparent">{{ prog4['tagline'] }}</div>
            </div>   
          </a>        
        </div>        
        <div class="programTiles">
          <a href="{{ url('programs/page/' ~ prog5['url']) }}">
            <div class="tile-flex flexslider fc">
              <div class="tile-title tile5">{{ prog5['title'] }} </div>
              <ul class="slides" style="height: 160px">
                <!-- Each slider should be enclosed inside li tag -->
                {% for pi in progimg5 %}
                <li class="slide"><div style="background: url('{{ url('img/programs/' ~ pi) }}') no-repeat center;"></div></li>
                {% endfor %}
              </ul>
              <div class="tile-tagline transparent">{{ prog5['tagline'] }}</div>
            </div> 
          </a>          
        </div>  
        <div class="programTiles">
          <a href="{{ url('programs/page/' ~ prog6['url']) }}">
            <div class="tile-flex flexslider ep">
              <div class="tile-title tile6">{{ prog6['title'] }}</div>
              <ul class="slides" style="height: 160px">
                <!-- Each slider should be enclosed inside li tag -->
                {% for pi in progimg6 %}
                <li class="slide"><div style="background: url('{{ url('img/programs/' ~ pi) }}') no-repeat center;"></div></li>
                {% endfor %}
              </ul>
              <div class="tile-tagline transparent">{{ prog6['tagline'] }}</div>
            </div>  
          </a>         
        </div>
        
        <?php if(!empty($somePartners)){ ?>
          <h5 class="section-title abkpartners-title">ABK Partners</h5>



          <div class="other-left-contents endorsements pull-left">


          <?php foreach ($somePartners as $key) { ?>
          <div class="testimonial">
            <div class="test-info">
              <span class="test-name"><a href="<?php echo $this->url->get().'partners/view/'.$key['partnerID'] ?>"><?php echo $key['partnerName']?></a></span><br/>
              <p class=""><?php echo $key['partnerInfo']; ?></p>
            </div>
          </div>
          <?php } ?>
        </div>

          <div class="clearfix"></div>

          <br />
          <div class="">
            <a href="<?php echo $this->url->get() ?>partners">View all ABK partners</a>
          </div>  
        <?php } ?>
      </div> 

      <div class="col-md-3 col-sm-3 col-xs-5 sidebar pull-left">
        <h5 class="section-title">Connect with us</h5>
        <div class="connectwithus">
          <a href="{{ specialPage1['pageUrl'] }}">
            <div class="connectTiles left-tile sscp">
              <div class="connectTitle-Title" style="background-color: rgb(8, 74, 178);">{{ specialPage1['pageTitle'] }}</div>
            </div>
          </a>
          <a href="{{ url('towns') }}">
            <div class="connectTiles right-tile swhh">
              <div class="connectTitle-Title" style="background-color: rgb(99, 47, 0);">See What's Happening in your Hometown</div>
            </div>
          </a>
          <a href="{{ specialPage3['pageUrl'] }}">
            <div class="connectTiles left-tile bottom-tile rmd">
              <div class="connectTitle-Title" style="background-color: rgb(210, 71, 38);">{{ specialPage3['pageTitle'] }}</div>
            </div>
          </a>
          <a href="{{ specialPage4['pageUrl'] }}">
            <div class="connectTiles right-tile bottom-tile bsc">
              <div class="connectTitle-Title" id="box-program" style="background-color: rgb(62, 85, 109);">{{ specialPage4['pageTitle'] }}</div>
            </div>
          </a>
        </div>
        <div class="social-links">
          <ul class="social-list">
            <li class="social-icons"><a href="https://www.facebook.com/Ang-Bayan-Ko-Foundation-1664040513867744/" target="_blank" class="fb"></a></li>
            <li class="social-icons"><a href="https://plus.google.com/u/0/101853314610354158387" target="_blank" class="gplus"></a></li>
            <li class="social-icons"><a href="https://twitter.com/abk_foundation" target="_blank" class="twitter"></a></li>
            <li class="social-icons"><a href="https://www.linkedin.com/in/ang-bayan-ko-foundation-7a33a6113" target="_blank" class="in"></a></li>
            <li class="social-icons"><a href="#" class="pi"></a></li>
            <li class="social-icons sc-last"><a href="https://www.youtube.com/channel/UCA9g-GNgrI0Eq28lMt7g0Iw" target="_blank" class="youtube"></a></li>
          </ul>        
        </div>
        <a href="{{ url('forum') }}">
          <div class="joinforum">
            <div class="forum-icon"></div>
            <h2>Join the discussions at the Forums</h2>
          </div>
        </a>

        <div style="clear:both"></div>
        <div id="side-donation">
          <a href="/donate"><img height="74" width="307" src="{{ url('img/paypal.jpg')}}"></a>
        </div>

        <div class="e-newsletter">
          <h5>e-Newsletter</h5>
          {{ form('index/submitEmailNewsLetter', 'id':'e-newsletterForm') }}
            <div id="enewsMessage"></div>
            <input type="hidden" name="emailnewsletterAjax" value="1">
            {{ text_field('name', 'class':'input-newsletter-name', 'name':'name', 'placeholder':'Your full name') }}
            {{ text_field('email', 'class':'input-newsletter-email', 'name':'email', 'placeholder':'Your Email') }}
            {{ submit_button("subscribe", 'class':'input-newsletter-button', 'value':'Subscribe', 'name':'subscribe' ) }}
          </form>
        </div>

        <?php if(count($announcements) > 0){ ?>
        <div class="announcements-container">
          <br>
          <table width="100%">
          <tr>
            <td class="annhead"><h5>Announcements</h5></td>
          </tr>
          <tr><td>&nbsp;</td></tr>
          <tr>
            <?php
          foreach ($announcements as $ann) { ?>
            <td><a href="/announcements/view/<?php echo $ann['annID'] ?>" class="pull-left ann-title"><?php echo $ann['annTitle'] ?></a></td>
          </tr>
          <tr>
            <td><span class="pull-right"><?php echo date("m-d-Y", $ann['annDate']) ?></span></td>
          </tr>
          <tr>
            <td><?php echo $ann['annDesc'] ?></td>
          </tr>
          <?php } ?>
          <tr>
            <td><span class="pull-right"> <a href="/announcements">View all announcements</a></span></td>
          </tr>

        </table>
        </div>
        <!-- <div class="announcements">
          <h5>Announcements</h5>
          <?php
          foreach ($announcements as $ann) { ?>
          <div class="annList">
          <a href="/announcements/view/<?php echo $ann['annID'] ?>" class="pull-left ann-title"><?php echo $ann['annTitle'] ?></a>
            <div class="pull-right"><?php echo date("m-d-Y", $ann['annDate']) ?></div> 
            <div class="pull-left">
              <p>
                <?php echo $ann['annDesc'] ?>
              </p>
            </div>
          </div>
          <?php } ?>

          <div class="annViewAll pull-right">
            <a href="/announcements">View all announcements</a>
          </div>   
        </div> -->
        <?php }else{ ?>
        <div class="announcements-container">
          <br>
          <table width="100%">
          <tr>
            <td class="annhead"><h5>Announcements</h5></td>
          </tr>
          <tr><td>&nbsp;</td>
          </tr>
          <tr>
            <td>No Announcements Posted.</td>
            <tr>
        </table>
      </div>
        <?php } ?>

      </div>            
    </div>
  </div>

  <!-- Programs and Announcements Ends-->

  <div class="container section-feature ">
    <div class="row">
      <div class="tile">
      </div>
      <div class="tile">
      </div>
      <div class="tile">
      </div>
    </div>
  </div>

  <!-- Footer starts -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
            <div class="row">
              <div class="col-md-3 contact-us-footer pull-left">
                  <h3>Contact Us</h3>
                  {% for contact in contacts %}
                  <strong>{{ contact.location }}:</strong> <br/>
                  <i class="icon-home"></i> {{ contact.home }} <br/>
                  <i class="icon-phone"></i> {{ contact.number }} <br/>
                  <i class="icon-envelope-alt"></i> {{ contact.email }} <br/> <br/>
                  <br>
                  {% endfor %}
              </div>

              <div class="col-md-3 more-info-footer pull-left">
                  <h3>More Info</h3>
                  <ul class="more-info">
                    {{ moreInfoLinks }}
                  </ul>
              </div>

              <div class="col-md-3 programs-footer pull-left">
                  <h3>Programs</h3>
                  <ul class="more-info">                
                    {{ programLinks }}
                  </ul>
              </div>
              <div class="col-md-3 others-footer pull-left">
                  <h3>Others</h3>
                  <ul class="more-info">               
                    {{ specialPagesLinks }}
                    <!-- <li> <a href="#">e-Newletter</a> </li>
                    <li> <a href="#">Joing the Forums</a> </li> -->
                  </ul>
                </div>      
              </div>
            </div>
            <!-- Copyright info -->
            <p class="copy">Copyright &copy; 2014 | <a href="#">AngBayanKo Site</a> - <a href="#">Home</a> </p>
      </div>
    </div>
  <div class="clearfix"></div>
  </div>
</footer>   

<!-- Footer ends -->

<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 

<!-- JS -->
<script src="js/jquery.js"></script> <!-- jQuery -->
<script src="js/bootstrap.js"></script> <!-- Bootstrap -->
<script src="js/jquery.isotope.js"></script> <!-- Isotope -->
<script src="js/jquery.prettyPhoto.js"></script> <!-- Pretty Photo -->
<script src="js/filter.js"></script> <!-- Filter for support page -->

<script src="js/jquery.flexslider-min.js"></script> <!-- Flex slider -->
<script src="js/jquery.cslider.js"></script> <!-- Parallax Slider -->
<script src="js/modernizr.custom.28468.js"></script> <!-- Parallax slider extra -->

<script src="js/jquery.carouFredSel-6.1.0-packed.js"></script> <!-- Carousel for recent posts -->
<script src="js/jquery.refineslide.min.js"></script> <!-- Refind slider -->
<script src="js/jquery.backgroundSize.js"></script>
<script src="js/respond.src.js"></script>
{{ javascript_include('js/jquery.maxlength.min.js') }}
<script src="js/custom.js"></script> <!-- Custom codes -->

</body>
</html>