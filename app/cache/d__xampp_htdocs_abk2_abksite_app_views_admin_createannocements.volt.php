<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">
        <!-- page meta -->
        <span class="page-meta">Create New Announcement</span>
    </h2>


    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
        <a href="/admin"><i class="icon-home"></i> Home</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="/admin/announcements">Announcements</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="/admin/createannocements" class="bread-current">Create</a>
    </div>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->
<!-- Matter -->

<div class="matter">
    <div class="container">
        <form name="postform" method="post" action="">
            <div class="row">

                <div class="col-md-12">
                    <?php echo $this->getContent(); ?>
                    <div class="widget">
                        <div class="widget-head">
                            <div class="pull-left">Make Announcement</div>
                            <div class="widget-icons pull-right">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                            <div class="padd">
                              
                                <div class="form-group">
                                <div class="control-label">
                                    <label>Enter Title</label>
                                    <span class="asterisk">*</span>
                                </div>
                                    <div>
                                        <?php echo $form->render('ann_title'); ?>
                                        <?php echo $form->messages('ann_title'); ?>
                                       
                                    </div>
                                    
                                </div>

                                <div class="text-area">
                                    <div class="control-label">
                                    <label>Content</label>
                                    <span class="asterisk">*</span>
                                </div>
                                    <?php echo $form->render('ann_content'); ?>
                                </div>
                                <?php echo $form->messages('ann_content'); ?>

                                <br />
                                <div class="form-group">
                                 <div class="control-label">   
                                    <label>Announcement Duration</label>
                                    <span class="asterisk">*</span>
                                </div>
                                    <div class="input-append">
                                        <span>From </span>
                                        <span id="dateStart">
                                            <?php echo $form->render('ann_start'); ?>
                                            <span class="add-on">
                                                <i class="btn btn-info btn-lg icon-calendar"></i>
                                            </span>
                                        </span>
                                        <span>To </span>
                                        <span id="dateEnd">

                                            <?php echo $form->render('ann_end'); ?>
                                            <span class="add-on">
                                                <i class="btn btn-info btn-lg icon-calendar"></i>
                                            </span>
                                        </span>
                                    </div>
                                    <?php echo $form->messages('ann_start'); ?>
                                    <?php echo $form->messages('ann_end'); ?>
                                </div>
                            </div>
                            <div class="widget-foot">
                                <?php echo $this->tag->submitButton(array('Publish Announcement', 'name' => 'ann_submit', 'class' => 'btn btn-primary pull-right')); ?>
                               <!--  <input type="hidden" name="csrf" value="<?php // echo $this->security->getToken() ?>"/> -->
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Matter ends -->