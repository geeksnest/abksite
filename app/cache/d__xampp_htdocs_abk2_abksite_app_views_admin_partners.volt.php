            <!-- Modal View-->
            <div id="modalView" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">View Record</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> ABK Partner List</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="index.html"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <span>Partners</span>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->

            <div class="matter">
              <?php echo $this->tag->form(array('admin/partners', 'id' => 'main-table-form')); ?>
              <div class="container">

                <div class="row">
                  <div class="col-md-2">
                    <label>Search by Text</label>
                  </div>
                  <div class="col-md-6 form-group">
                    <div class="col-md-12">
                      <?php echo $this->tag->textField(array('search_text', 'class' => 'form-control')); ?>
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    <?php echo $this->tag->submitButton(array('Search', 'class' => 'btn btn-default')); ?>
                    <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Filter by Date</label>
                  </div>
                  <div class="col-md-3 form-group" id="fromDatepicker" class="input-append">
                    <label class="col-md-2 control-label">From</label>
                    <div class="col-md-10">
                      <?php echo $this->tag->textField(array('fromDate', 'data-format' => 'yyyy-MM-dd', 'class' => 'form-control dtpicker', 'data-time-icon' => 'icon-time')); ?>
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-3 form-group" id="toDatepicker" class="input-append">
                    <label class="col-md-2 control-label">To</label>
                    <div class="col-md-10">
                      <?php echo $this->tag->textField(array('toDate', 'data-format' => 'yyyy-MM-dd', 'class' => 'form-control dtpicker', 'data-time-icon' => 'icon-time')); ?>
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <?php echo $this->tag->submitButton(array('Filter', 'class' => 'btn btn-default')); ?>
                  </div>
                </div>

                <!-- Table -->
                <div class="row">

                  <!-- <div class="form-group">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="control-label col-lg-3">Search</label>
                        <div class="col-lg-10"> 
                          
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      
                      <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                    </div>
                  </div> --> 

                  <div class="col-md-12">
                    <?php echo $this->getContent(); ?>
                    
                    
                    <div class="widget">

                      <div class="widget-head">
                        <div class="pull-left">Partners</div>
                        <div class="widget-icons pull-right">
                          <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                          <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                        </div>  
                        <div class="clearfix"></div>
                      </div>


                      <div class="widget-content">
                        <?php echo $this->tag->hiddenField(array('csrf', 'value' => $this->security->getToken())); ?>
                        <input type="hidden" class="tbl-action" name="action" value=""/>
                        <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                        <input type="hidden" class="tbl-edit-url" name="editurl" value="editPartner/"/>
                        <table class="table table-striped table-bordered table-hover tblusers">
                          <thead>
                            <tr>
                              <?php if($userlevel!=2){ ?>
                              <th><?php echo $this->tag->checkField(array('select_all[]', 'class' => 'tbl_select_all')); ?></th>
                              <!-- <th>UserId</th> -->
                              <?php } ?>
                              <th><a href="?sort=<?php echo $partnernameHref; ?>">Partner Name <i class="<?php echo ($partnernameIndicator ? $partnernameIndicator : ''); ?>"></i></a></th>
                              <th><a href="?sort=<?php echo $usernameHref; ?>">Username <i class="<?php echo ($usernameIndicator ? $usernameIndicator : ''); ?>"></i></a></th>
                              <th><a href="?sort=<?php echo $nameHref; ?>">Name <i class="<?php echo ($nameIndicator ? $nameIndicator : ''); ?>"></i></a></th>
                              <th><a href="?sort=<?php echo $datecreatedHref; ?>">Date Created <i class="<?php echo ($datecreatedIndicator ? $datecreatedIndicator : ''); ?>"></i></a></th>
                              <th><a href="?sort=<?php echo $positionHref; ?>">Position <i class="<?php echo ($positionIndicator ? $positionIndicator : ''); ?>"></i></a></th>
                              <th><a href="?sort=<?php echo $statusHref; ?>">Status <i class="<?php echo ($statusIndicator ? $statusIndicator : ''); ?>"></i></a></th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($page->items as $user) {
                            $statustext = ($user['status']=='active')?'<span class="label label-success">Active</span> <a href="#modalPrompt" class="btn btn-xs btn-default tbl_delete_row modal-control-button" data-toggle="modal" data-action="deactivate" data-recorID="'.$user['userID'].'"><i class="icon-ban-circle"></i> </a>':'<span class="label label-default">Inactive</span> <a href="#modalPrompt" class="btn btn-xs btn-success tbl_delete_row modal-control-button" data-toggle="modal" data-action="activate" data-recorID="'.$user['userID'].'"><i class="icon-ok"></i> </a>';
                            $actions = '<a  href="#modalView" data-toggle="modal" class="btn btn-xs btn-success modal-record-view" data-href="ajaxUserView/'.$user['userID'].'" ><i class="icon-ok"></i> </a>
                                <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="'.$user['userID'].'"><i class="icon-pencil"></i> </a>
                                <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="'.$user['userID'].'"><i class="icon-remove"></i> </a>';
                            $checkboxes = '<td>
                                <input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="'.$user['userID'].'"> 
                              </td>';
                            $userids = '<td>'.$user['userID'].'</td>';
                            $partnerNames = '<a href="/admin/partnersinfo/'.$user['partnerID'].'">'.$user['partnerName'].'</a>';
                            if($userlevel==2){
                              $statustext = ($user['status']=='active')?'<span class="label label-success">Active</span>':'<span class="label label-default">Inactive</span>';
                              $actions = '<a  href="#modalView" data-toggle="modal" class="btn btn-xs btn-success modal-record-view" data-href="ajaxUserView/'.$user['userID'].'" ><i class="icon-ok"></i> </a>';
                              $checkboxes = '';
                              $userids = '';
                              $partnerNames = $user['partnerName'];
                            }
                            echo '<tr>
                              '.$checkboxes.'
                              <td class="name">'.$partnerNames.'</td>
                              <td>'.$user['username'].'</td>
                              <td>'.$user['name'].'</td>
                              <td>'.date("F j, Y", $user['dateCreated']).'</td>
                              <td>'.$user['position'].'</td>
                              <td>'.$statustext.'</td>
                              <td>'.$actions.'</td>
                            </tr>';
                          }

                          if(empty($page->items)){
                            echo '<tr><td colspan="6">No partner found</td></tr>';
                          }
                          ?>
                          </tbody>
                          </table>

                          <div class="tblbottomcontrol" style="display:none">
                            <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
                            <a href="#" class="tbl_unselect_all"> Unselect </a>
                          </div>

                            <div class="widget-foot">
                              <?php if ($page->total_pages > 1) { ?>
                             
                              <ul class="pagination pull-right">

                                <?php if ($page->current != 1) { ?>
                                <li><?php echo $this->tag->linkTo(array('admin/partners?page=' . $page->before, 'Prev')); ?></li>
                                <?php } ?>

                                <?php foreach (range(1, $page->total_pages) as $index) { ?>
                                <?php if ($page->current == $index) { ?>
                                <li><?php echo $this->tag->linkTo(array('admin/partners?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                                <?php } else { ?>
                                <li><?php echo $this->tag->linkTo(array('admin/partners?page=' . $index, $index)); ?></li>
                                <?php } ?>
                                <?php } ?>         

                                <?php if ($page->current != $page->total_pages) { ?>                 
                                <li><?php echo $this->tag->linkTo(array('admin/partners?page=' . $page->next, 'Next')); ?></li>
                                <?php } ?>
                              </ul>
                              <?php } ?>
                              
                              <div class="clearfix"></div> 

                            </div>

                          </div>

                        </div>


                      </div>

                    </div>


                  </div>
                </form>
              </div>

<!-- Matter ends -->