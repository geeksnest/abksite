
      <div class="inner-post pull-left">
         <div class="inner-news-content">
            <?php echo $this->getContent(); ?>
            <?php if ($postNone) { ?>
                        <div class="entry">
                                 <h2><a href="<?php echo $this->url->get('post/news/' . $post->postSlug); ?>"><?php echo $post->postTitle; ?></a></h2>
                                 <!-- Meta details -->
                                 <div class="meta">
                                    <div><i class="icon-calendar"></i> <?php echo date('d-m-Y', $post->postPublishDate); ?></div>
                                    <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div> <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-type="button"></div> <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
                                    <span class="pull-right"><i class="icon-comment"></i> <a href="#">2 Comments</a></span>
                                 </div>
                                 
                                 <!-- Thumbnail -->
                                 <?php if ($post->postFeatureImage) { ?>
                                 <div class="bthumb3">
                                    <a href="#"><img src="<?php echo $post->postFeatureImage; ?>" alt="" class="img-responsive"></a>
                                 </div>
                                 <?php } ?>
                                 <!-- Para -->
                                 <?php echo $post->postContent; ?>

                                 <!-- Read more -->
                                 <div class="button"><a href="#">Read More...</a></div>
                        </div> 
            <?php } ?>
         </div>
      </div> 