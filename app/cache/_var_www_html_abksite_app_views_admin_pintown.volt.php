          <?php $token = $this->security->getToken() ?>

          <!-- Page heading -->
          <div class="page-head">
            <h2 class="pull-left"><i class="icon-table"></i> Towns</h2>

            <!-- Breadcrumb -->
            <div class="bread-crumb pull-right">
              <a href="index.html"><i class="icon-home"></i> Home</a> 
              <!-- Divider -->
              <span class="divider">/</span> 
              <?php echo $this->tag->linkTo(array('admin/towns', 'Towns')); ?>
              <span class="divider">/</span> 
              <span><?php echo $head; ?></span>
            </div>

            <div class="clearfix"></div>

          </div>
          <!-- Page heading ends -->

          <!-- Matter -->

          <div class="matter">
            <?php echo $this->tag->form(array('class' => 'form-horizontal', 'id' => 'main-table-form')); ?>
            <div class="container">

              <!-- Table -->

              <div class="row">

                <div class="col-md-12">
                  <?php echo $this->getContent(); ?>


                  <div class="widget">

                    <div class="widget-head">
                      <div class="pull-left"><?php echo $head; ?></div>
                      <div class="widget-icons pull-right">
                      </div>  
                      <div class="clearfix"></div>
                    </div>


                    <div class="widget-content">

                      <!-- <div id="us2" style="width: 100%; height: 500px;"></div>
                      <input type="hidden" id="us2-address" class="form-control" /> -->

                        <div id="mapPanel">
                          <button onclick="addDefaultMarker();" type="button" class="btn btn-primary"><i class="icon-map-marker"></i> Add Marker</button>
                          <span> &nbsp; or click on the map to add marker.</span>
                          <input class="pull-right" onclick="deleteMarkers();" type=button value="Reset Marker" class="btn btn-default">
                        </div>
                        <div style="padding-left:1%;">
                         <em style="color:##d3d3d3; font-size:12px;">*Drag the marker to pin desired town.</em>
                        </div>
                        <div id="map-canvas" style="height:500px; width 100%"></div>

                      <div class="padd">

                          <div class="form-group">
                            <div class="col-lg-2">
                              <label>Latitude</label>
                            </div>
                              <div class="col-lg-8">
                                  <?php if ($editMode) { ?>
                                    <?php echo $form->render('townLat', array( 'value' => $town->townLat)); ?>
                                  <?php } else { ?>
                                  <?php echo $form->render('townLat'); ?>
                                  <?php } ?>
                                  
                                  <?php echo $form->messages('townLat'); ?>
                              </div>

                          </div>
                          <div class="form-group">
                            <div class="col-lg-2">
                              <label>Longtitude</label>
                            </div>
                              <div class="col-lg-8">
                                  <?php if ($editMode) { ?>
                                    <?php echo $form->render('townLong', array( 'value' => $town->townLong)); ?>
                                  <?php } else { ?>
                                  <?php echo $form->render('townLong'); ?>
                                  <?php } ?>
                                  
                                  <?php echo $form->messages('townLong'); ?>
                              </div>
                          </div>

                          <hr />
                          
                          <div class="form-group">
                            <div class="col-lg-2">
                              <label>Town Name <apan class="asterisk">*</span></label>
                            </div>
                              <div class="col-lg-8">
                                  <?php if ($editMode) { ?>
                                    <input type="hidden" name="origTownName" value="<?php echo $town->townName; ?>">
                                    <?php echo $form->render('townName', array( 'value' => $town->townName)); ?>
                                  <?php } else { ?>
                                  <?php echo $form->render('townName'); ?>
                                  <?php } ?>
                                  <?php echo $form->messages('townName'); ?>
                              </div>
                          </div>

                          <!-- <div class="form-group">
                              <label class="col-lg-4 control-label"></label>
                              <div class="col-lg-8">
                                  <a href="#partnerGallery" data-toggle="modal" class="btn btn-default pull-right post-add-media"><i class="icon-paper-clip"></i> Add Media</a>
                              </div>
                          </div> -->


                          <div class="form-group">
                             <div class="col-lg-2">
                              <label>Town Information <span class="asterisk">*</span></label>
                            </div>
                              <div class="col-lg-8">
                              <?php echo $form->messages('townInfo'); ?>
                              <?php if ($editMode) { ?>
                                <?php echo $form->render('townInfo', array( 'value' => $town->townInfo)); ?>
                              <?php } else { ?>
                              <?php echo $form->render('townInfo'); ?>
                              <?php } ?>
                              </div>
                          </div>

                      </div>
                      <div class="widget-foot">
                        <?php echo $cancel; ?>
                          <?php echo $this->tag->submitButton(array($message, 'name' => 'saveTown', 'class' => 'btn btn-primary')); ?>
                          <input type="hidden" name="csrf" value="<?php echo $this->security->getToken() ?>"/>
                          <div class="clearfix"></div>
                      </div>
                    </div>

                  </div>


                </div>

              </div>


            </div>
          </form>
        </div>

<!-- Matter ends -->