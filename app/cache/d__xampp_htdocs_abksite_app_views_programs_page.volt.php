
      <div class="inner-programs pull-left">
		<?php if ($program->programBanner) { ?>
        <div class="dm-latest-news-head">
            <img src="<?php echo $program->programBanner; ?>">
            <div class="dm-latest-news-label">
              <?php echo $program->programTagline; ?>
            </div>
        </div>
		<?php } ?>
        <div class="tabs inner-tabs">
          <ul class="nav nav-tabs">
            <!-- Use unique name in anchor tag -->
            <?php if ($program->programID == 1) { ?>
            <li class="active"><a href="#news" data-toggle="tab">Latest Breaking News</a></li>
            <?php } ?>

            <?php $v12202410401iterator = $prog_pages; $v12202410401incr = 0; $v12202410401loop = new stdClass(); $v12202410401loop->length = count($v12202410401iterator); $v12202410401loop->index = 1; $v12202410401loop->index0 = 1; $v12202410401loop->revindex = $v12202410401loop->length; $v12202410401loop->revindex0 = $v12202410401loop->length - 1; ?><?php foreach ($v12202410401iterator as $page) { ?><?php $v12202410401loop->first = ($v12202410401incr == 0); $v12202410401loop->index = $v12202410401incr + 1; $v12202410401loop->index0 = $v12202410401incr; $v12202410401loop->revindex = $v12202410401loop->length - $v12202410401incr; $v12202410401loop->revindex0 = $v12202410401loop->length - ($v12202410401incr + 1); $v12202410401loop->last = ($v12202410401incr == ($v12202410401loop->length - 1)); ?>
                <?php if ($page->pageActive) { ?>
                    <li class="<?php if ($v12202410401loop->first) { ?> <?php if ($program->programID != 1) { ?> active <?php } ?> <?php } ?> "><a href="#<?php echo $page->pageSlug; ?>" data-toggle="tab"><?php echo $page->pageTitle; ?></a></li>
                <?php } ?>
            <?php $v12202410401incr++; } ?>
          </ul>




          <!-- Tab conten -->
          <div class="tab-content">
                    <?php if ($program->programID == 1) { ?>
                    <div class="tab-pane active inner-news" id="news">
                      <?php if ($post->total_items == 0) { ?>
                        No News Added.
                      <?php } else { ?>
                        <?php $v12202410401iterator = $post->items; $v12202410401incr = 0; $v12202410401loop = new stdClass(); $v12202410401loop->length = count($v12202410401iterator); $v12202410401loop->index = 1; $v12202410401loop->index0 = 1; $v12202410401loop->revindex = $v12202410401loop->length; $v12202410401loop->revindex0 = $v12202410401loop->length - 1; ?><?php foreach ($v12202410401iterator as $p) { ?><?php $v12202410401loop->first = ($v12202410401incr == 0); $v12202410401loop->index = $v12202410401incr + 1; $v12202410401loop->index0 = $v12202410401incr; $v12202410401loop->revindex = $v12202410401loop->length - $v12202410401incr; $v12202410401loop->revindex0 = $v12202410401loop->length - ($v12202410401incr + 1); $v12202410401loop->last = ($v12202410401incr == ($v12202410401loop->length - 1)); ?>
                            <?php if ($p->postStatus == 'publish') { ?>
                            <div class="entry">
                                     <h2><a href="<?php echo $this->url->get('post/news/' . $p->postSlug); ?>"><?php echo $p->postTitle; ?></a></h2>

                                     <!-- Meta details -->
                                     <div class="meta">
                                        <div><i class="icon-calendar"></i> <?php echo date('d-m-Y', $p->postPublishDate); ?></div>
                                        <div class="fb-like" data-href="http://abk.gotitgenius.com/<?php echo $this->url->get('post/news/' . $p->postSlug); ?>" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div> 
                                        <div class="fb-share-button" data-href="<?php echo $this->url->get('post/news/' . $p->postSlug); ?>" data-type="button"></div> <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
                                        <!-- <span class="pull-right"><i class="icon-comment"></i> <a href="#">2 Comments</a></span> -->
                                     </div>
                                     
                                     <!-- Thumbnail -->
                                     <?php if ($p->postFeatureImage) { ?>
                                     <div class="bthumb3">
                                        <a href="#"><img src="<?php echo $p->postFeatureImage; ?>" alt="" class="img-responsive"></a>
                                     </div>
                                     <?php } ?>
                                     <!-- Para -->
                                     <p><?php echo $out = strlen(strip_tags($p->postContent)) > 550 ? substr(strip_tags($p->postContent),0,550)."..." : $p->postContent; ?></p>

                                     <!-- Read more -->
                                     <div class="button"><a href="<?php echo $this->url->get('post/news/' . $p->postSlug); ?>">Read More...</a></div>
                                     <div class="clearfix"></div>
                            </div>    
                            <?php } ?>
                        <?php $v12202410401incr++; } ?>
                        <?php } ?>

                      <?php if ($post->total_items > 1) { ?>
                            <div class="paging">
                              <?php if ($post->current != 1) { ?>
                                <?php echo $this->tag->linkTo(array('admin/users?page=' . $post->before, 'Prev')); ?>
                              <?php } ?>

                              <?php $v12202410401iterator = range(1, $post->total_pages); $v12202410401incr = 0; $v12202410401loop = new stdClass(); $v12202410401loop->length = count($v12202410401iterator); $v12202410401loop->index = 1; $v12202410401loop->index0 = 1; $v12202410401loop->revindex = $v12202410401loop->length; $v12202410401loop->revindex0 = $v12202410401loop->length - 1; ?><?php foreach ($v12202410401iterator as $index) { ?><?php $v12202410401loop->first = ($v12202410401incr == 0); $v12202410401loop->index = $v12202410401incr + 1; $v12202410401loop->index0 = $v12202410401incr; $v12202410401loop->revindex = $v12202410401loop->length - $v12202410401incr; $v12202410401loop->revindex0 = $v12202410401loop->length - ($v12202410401incr + 1); $v12202410401loop->last = ($v12202410401incr == ($v12202410401loop->length - 1)); ?>
                              <?php if ($post->current == $index) { ?>
                                <?php echo $this->tag->linkTo(array('programs/page/' . $program->programPage . '?page=' . $index, $index, 'class' => 'current')); ?>
                              <?php } else { ?>
                                <?php echo $this->tag->linkTo(array('programs/page/' . $program->programPage . '?page=' . $index, $index)); ?>
                              <?php } ?>
                              <?php $v12202410401incr++; } ?>         

                              <?php if ($post->current != $post->total_pages) { ?>                 
                                <?php echo $this->tag->linkTo(array('programs/page/' . $program->programPage . '?page=' . $post->next, 'Next')); ?>
                              <?php } ?>
                            </div>
                      <?php } ?>


                    </div>

                    <?php } ?>
              <?php $v12202410401iterator = $prog_pages; $v12202410401incr = 0; $v12202410401loop = new stdClass(); $v12202410401loop->length = count($v12202410401iterator); $v12202410401loop->index = 1; $v12202410401loop->index0 = 1; $v12202410401loop->revindex = $v12202410401loop->length; $v12202410401loop->revindex0 = $v12202410401loop->length - 1; ?><?php foreach ($v12202410401iterator as $page) { ?><?php $v12202410401loop->first = ($v12202410401incr == 0); $v12202410401loop->index = $v12202410401incr + 1; $v12202410401loop->index0 = $v12202410401incr; $v12202410401loop->revindex = $v12202410401loop->length - $v12202410401incr; $v12202410401loop->revindex0 = $v12202410401loop->length - ($v12202410401incr + 1); $v12202410401loop->last = ($v12202410401incr == ($v12202410401loop->length - 1)); ?>
                <?php if ($page->pageActive) { ?>
                    <div class="tab-pane inside-contents <?php if ($v12202410401loop->first) { ?> <?php if ($program->programID != 1) { ?> active <?php } ?> <?php } ?>" id="<?php echo $page->pageSlug; ?>">
                        <?php echo $page->pageContent; ?>
                    </div>
                <?php } ?>
            <?php $v12202410401incr++; } ?>
          </div>
        </div>        
      </div> 