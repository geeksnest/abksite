  <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> Contact</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="/admin"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <a href="/admin/other">Other</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <span>Edit</span>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <div class="matter">
            <div class="container">

            <div class="row">

             <div class="col-md-12">           
             <div class="widget"> 
          
          <!-- container block  -->
            <div class="widget-head">
            <div class="pull-left">Edit Contact</div>
            <div class="clearfix"></div>
            </div>

            <div class="widget-content">
              <div class="padd">
                 <?php echo $this->getContent(); ?>
              <form class="form-horizontal" method="post">
                              <div class="form-group">
                                  <div class="col-lg-2">
                                  <label class="">Place:</label>
                                  <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                    <?php echo $required; ?>
                                    <input type="text" class="form-control" value="<?php echo $contacts->location; ?>" placeholder="Place Name" name="place">
                                  </div>
                                </div>
                              
                                 <div class="form-group">
                                  <div class="col-lg-2">
                                  <label class="">Address:</label>
                                  <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                    <?php echo $addError; ?>
                                    <input type="text" class="form-control" value="<?php echo $contacts->home; ?>" placeholder="Address" name="home">
                                  </div>
                                </div>

                                 <div class="form-group">
                                  <div class="col-lg-2">
                                  <label class="">Contact Number:</label>
                                  <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                     <?php echo $numError; ?>
                                    <input type="text" class="form-control" value="<?php echo $contacts->number; ?>" placeholder="Contact Number" name="number">
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label class="">Email Address:</label>
                                  <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                     <?php echo $emailError; ?>
                                    <input type="text" class="form-control" value="<?php echo $contacts->email; ?>" placeholder="Email Address" name="email">
                                  </div>
                                </div>

            </div>
          </div>

             <!-- footer start -->
                 <div class="widget-foot">
                  <div class="clearfix"></div> 
                  <input type="Submit" name="save_contact" id="inputEmail" Value="Save Changes" class="btn btn-info">
                 <button type="button" class="btn btn-default" onclick="window.location='/admin/other'">Cancel</button>

               </div>
              <!-- footer end -->
            </form>
          <!-- container end  -->
            </div>
            </div>

            </div>

            </div>
          </div>