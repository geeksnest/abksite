            <!-- Modal View-->
            <div id="modalDigitalAssets" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
              <div class="modal-dialog" style="width: 900px;">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Digital Assets Library</h4>
                    <input type="hidden" id="digital-assets-year" name="digital-assets-year" value="<?php echo $dayear; ?>" />
                    <input type="hidden" id="digital-assets-month" name="digital-assets-month" value="<?php echo $damonth; ?>" />
                  </div> 
                  <div class="modal-body" style=" height: 400px; overflow:scroll;">
                    <span class="btn btn-success fileinput-button">
                      <i class="glyphicon glyphicon-plus"></i>
                      <span>Add files...</span>
                      <!-- The file input field used as target for the file upload widget -->
                      <input id="digitalAssets" type="file" name="files[]" multiple accept="image/*">
                    </span>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                      <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <label>Recently Added</label>
                    <div class="gallery digital-assets-gallery">
                      None
                    </div>
                    <div class="clearfix"></div>
                    <div class="tabbable" style="margin-bottom: 18px;">
                      <ul class="nav nav-tabs">
                        <?php
                        $y = 0;
                        foreach ($folders as $key => $value) {
                          if($y==0){
                            echo '<li class="active"><a href="#'.$key.'" data-toggle="tab" class="post-year-tab">'.$key.'</a></li>';
                          }else{
                            echo '<li><a href="#'.$key.'" data-toggle="tab" class="post-year-tab">'.$key.'</a></li>';
                          }
                          $y++;
                        }
                        ?>
                      </ul>
                      <div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
                        <?php
                        foreach ($folders as $key => $value) {
                          ?>
                          <div class="tab-pane active" id="<?php echo $key?>">
                            <br/>
                            <select class="form-control post-assets-month">
                              <?php
                              $x=0;
                              foreach($value as $v){
                                $jd=gregoriantojd($v,13,1998);
                                if($x==0){
                                  echo '<option value="'.$v.'" selected="selected">'.jdmonthname($jd,0).'</option>';
                                }else{
                                  echo '<option value="'.$v.'">'.jdmonthname($jd,0).'</option>';
                                }
                                $x++;
                              }
                              ?>
                            </select>
                            <br/>
                            <div class="gallery digital-assets-monthly">

                            </div>
                          </div>
                          <?php 
                        }
                        ?>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- Page heading -->
            <div class="page-head">
              <!-- Page heading -->
              <h2 class="pull-left"> 
                <!-- page meta -->
                <span class="page-meta">Make News Post</span>
              </h2>


              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="/admin"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <a href="/admin/allnewspost">News Post</a>
                <!-- Divider -->
                <span class="divider">/</span> 
                <a href="/admin/createpost" class="bread-current">Create</a>          
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->
            <!-- Matter -->
            <?php echo $script; ?>
            <div class="matter">
              <div class="container">
                <form name="postform" method="post" action="">
                  <div class="row">

                    <div class="col-md-8">
                      <?php echo $this->getContent(); ?>
                      <div class="widget">
                        <div class="widget-head">
                          <div class="pull-left">Make Post</div>
                          <div class="widget-icons pull-right">
                          </div>  
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                          <div class="padd">
                            <div class="form-group">
                              <div class="control-label">
                              <label>Enter Title</label>
                              <span class="asterisk">*</span>
                            </div>
                              <div>
                                <?php echo $form->render('post_title'); ?>
                                <?php echo $form->messages('post_title'); ?>
                                <?php echo $this->tag->hiddenField(array('post_slug', 'class' => 'post-slug')); ?>
                                <label>Slug: </label> <span class="post-slug-text"><?php echo $this->tag->getvalue('post_slug'); ?></span>
                              </div>
                              
                            </div>

                            <div class="text-area">
                              <div class="control-label">
                              <label>Content</label>
                              <span class="asterisk">*</span>
                            </div>
                              <?php echo $form->render('post_content'); ?>
                            </div>
                            <?php echo $form->messages('post_content'); ?>
                          </div>
                          <div class="widget-foot">
                          </div>
                        </div>
                      </div>  
                    </div>

                    <!-- post sidebar -->
                   
                       <div class="col-md-4">

                      <div class="widget">
                        <div class="widget-head">
                          <div class="pull-left">Options</div>
                          <div class="widget-icons pull-right">
                          </div>  
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                          <div class="padd">
                            <hr/>
                            <div class="check-box">
                              <label><?php echo $this->tag->checkField(array('post_status', 'class' => 'tbl_select_row', 'value' => 'draft')); ?> Save as Draft</label>
                            </div>
                            <hr/>
                            <div class="form-group">
                              <label>Publish on: </label>
                              <div id="datetimepicker1" class="input-append">
                                <?php echo $this->tag->textField(array('postDatePublish', 'data-format' => 'yyyy-MM-dd', 'class' => 'form-control dtpicker', 'data-time-icon' => 'icon-time')); ?>
                                <span class="add-on">
                                  <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                                </span>
                                <br/>
                                (Empty field will make it current date.)
                              </div>
                            </div>
                            <hr/>
                           
                          </div>
                          <div class="widget-foot">
                            
                            <!-- Footer goes here -->
                          </div>
                        </div>
                      </div>  
                    </div>

                    <!-- page banner start here.  -->
                    <div class="col-md-4">

                      <div class="widget">
                        <div class="widget-head">
                          <div class="pull-left">Featured Image </div> 
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                          <div class="padd">
                        
                            <div class="form-group">
                            <div id="pageBannerImgWrapper"></div>
                            <?php echo $this->tag->textField(array('pageBannerUrl', 'class' => 'form-control', 'placeholder' => 'Image URL here.')); ?>
                            </div>

                          </div>
                          <div class="widget-foot">
                             <button type="button" class="btn btn-default pull-right" id="removeBanner" style="display:none">Reset</button>
                             <div class="clearfix"></div>
                            <!-- Footer goes here -->
                          </div>
                        </div>
                      </div>  
                    </div>
                    <!-- page banner end. -->

                    <div class="col-lg-8">
                         
    <div class="row"><!--start row-->
      <div class="col-lg-12">
        <div class="widget">
          <div class="widget-head">
          <div class="pull-left">Digital Assets (<span id="fileCount"><?php echo count($digital_assets)?></span> files)</div>
            <div class="widget-icons pull-right">
            </div>  
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
            <div class="padd">
              <div id="imageError"></div>
              <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Add files...</span>
                <!-- The file input field used as target for the file upload widget -->
                <input id="postpictures" type="file" name="files[]" multiple accept="image/*">
              </span>
              <!-- The global progress bar -->
              <div id="progress" class="progress">
                <div class="progress-bar progress-bar-success"></div>
              </div>
              <div class="gallery" id="pages-gallery">
                <?php foreach ($digital_assets as $img) { ?>
                <div class="program-digital-assets-library pull-left" style="position: relative">
                  <a href="<?php echo $this->url->get('img/programs/' . $prog . '/' . $img); ?>" class='prettyPhoto[pp_gal]'>
                    <img src="<?php echo $this->url->get('img/programs/' . $prog . '/' . $img); ?>" alt=""></a>
                    <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="<?php echo $this->url->get('img/programs/' . $prog . '/' . $img); ?>">
                    <button type="button" class="btn btn-xs btn-danger digital-assets-delete" data-filename="<?php echo $img; ?>" data-folder="<?php echo $prog; ?>" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
                  </div>
                  <?php } ?>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="widget-foot">
                
              </div>
            </div>
          </div>  
        </div>       
      </div><!--end row--> 
                          </div> 
 <div class="col-md-4">
                      <div class="widget">
                        <div class="widget-head">
                          <div class="pull-left">Other Details</div>
                          <div class="widget-icons pull-right">
                          </div>  
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                          <div class="padd">
                             <?php echo $checkedProgsError; ?>
                            <h6>Programs</h6>
                            <hr />
                            <?php foreach ($programs as $prog) { ?>
                            <div class="check-box">
                              <label>
                                <?php if ($prog->programID == 1) { ?>
                                <?php echo $this->tag->checkField(array('chkProg' . $prog->programID, 'value' => $prog->programID, 'checked' => '')); ?>
                                <?php } else { ?>
                                <?php echo $this->tag->checkField(array('chkProg' . $prog->programID, 'value' => $prog->programID)); ?>
                                <?php } ?>
                                <?php echo $prog->programName; ?>
                              </label>
                              </div>
                              <?php } ?>

                               <h6>Pages</h6>
                               <hr />
                              <?php foreach ($pages as $prog) { ?>

                              <div class="check-box">
                                  <?php if ($prog->pageID == 1) { ?>
                                  <?php echo $this->tag->checkField(array('chkPages' . $prog->pageID, 'value' => $prog->pageID, 'checked' => '')); ?>
                                  <?php } else { ?>
                                  <?php echo $this->tag->checkField(array('chkPages' . $prog->pageID, 'value' => $prog->pageID)); ?> 
                                  <?php } ?>
                                  <?php echo $prog->pageTitle; ?></label>
            
                                </div>
                                <?php } ?>                              
                              
                                <hr />
                                <h6>Keywords</h6>
                                 <?php echo $this->tag->textField(array('post_keywords', 'class' => 'form-control col-lg-12', 'placeholder' => 'keywords..')); ?>
                                Enter Keywords separated by comma. Ex. philippines, news, happening
                              </div>
                              <div class="widget-foot">
                                 <div class="buttons">
                              <?php echo $this->tag->submitButton(array('submit_post', 'name' => 'submit_post', 'value' => 'Publish News', 'class' => 'btn btn-info pull-right')); ?>
                              <!-- <input type="hidden" name="csrf" value="<?php // echo $this->security->getToken() ?>"/> -->
                            </div>
                            <div class="clearfix"></div>
                                <!-- Footer goes here -->
                              </div>
                            </div>

                          </div>  

                        </div>

               





                        </div>
                      </form>
                    </div>
                  </div>

    <!-- Matter ends -->