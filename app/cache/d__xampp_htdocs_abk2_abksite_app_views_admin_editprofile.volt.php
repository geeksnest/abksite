	    <!-- Page heading -->
	    <div class="page-head">
        <!-- Page heading -->
	      <h2 class="pull-left"> 
          <!-- page meta -->
          <span class="page-meta">Account</span>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="/admin"><i class="icon-home"></i> Home</a> 
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="#" class="bread-current">Edit Profile</a>        
        </div>

        <div class="clearfix"></div>

	    </div>
	    <!-- Page heading ends -->



	    <!-- Matter -->

	    <div class="matter">
        <div class="container">

          <div class="row">

            <div class="col-md-12">
              <?php echo $this->getContent(); ?>

              <div class="widget wgreen">
                
                <div class="widget-head">
                  <div class="pull-left">Edit User</div>
                  <div class="widget-icons pull-right">
                    <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                    <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                    <h6>User Account</h6>
                    <hr />
                    <!-- Form starts.  -->
                     <?php echo $this->tag->form(array('admin/editprofile', 'class' => 'form-horizontal')); ?>
                                <?php echo $form->render('huserName', array( 'value' => $user->userName)); ?>
                                <?php echo $form->render('huserEmail', array( 'value' => $user->userEmail)); ?>
                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('username'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('username', array( 'value' => $user->userName )); ?>
                                    <?php echo $form->messages('username'); ?>
                                  </div>
                                </div>
                              
                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('email'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('email', array( 'value' => $user->userEmail )); ?>
                                    <?php echo $form->messages('email'); ?>
                                  </div>
                                </div>
                    
                    <h6>User Profile</h6>
                    <hr />
                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('firstname'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('firstname', array( 'value' => $user->userFirstname)); ?>
                                    <?php echo $form->messages('firstname'); ?>
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('lastname'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('lastname', array( 'value' => $user->userLastname)); ?>
                                    <?php echo $form->messages('lastname'); ?>
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('middlename'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('middlename', array( 'value' => $user->userMiddlename)); ?>
                                    <?php echo $form->messages('middlename'); ?>
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('address'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('address', array( 'value' => $user->userAddress)); ?>
                                    <?php echo $form->messages('address'); ?>
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('company'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('company', array( 'value' => $user->userCompany)); ?>
                                    <?php echo $form->messages('company'); ?>
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('contact'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('contact', array( 'value' => $user->userContact)); ?>
                                    <?php echo $form->messages('contact'); ?>
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('position'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('position', array( 'value' => $user->userPosition)); ?>
                                    <?php echo $form->messages('position'); ?>
                                  </div>
                                </div>  
                                  
                                   
                                <div class="form-group">
                                  <div class="col-lg-offset-1 col-lg-9">
                                    <?php echo $this->tag->submitButton(array('Update', 'class' => 'btn btn-primary', 'name' => 'update_button')); ?>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                  </div>
                                </div>
                              </form>
                  </div>
                </div>
                  <div class="widget-foot">
                    <!-- Footer goes here -->
                  </div>
                </div>  
              <div class="widget wgreen">
                <div class="widget-head">
                  <div class="pull-left">Change Password</div>
                  <div class="widget-icons pull-right">
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                  <h6>Change Password</h6>
                  <hr />
                  <?php echo $this->tag->form(array('admin/editprofile', 'class' => 'form-horizontal')); ?>
                 <!--  <div class="form-group">
                    <label class="col-lg-4 control-label">
                      <?php echo $oldpassError; ?>
                    </div>
                  </div>
 -->
                  <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $form->label('password'); ?></label>
                    <div class="col-lg-8">
                      <?php echo $form->render('password'); ?>
                      <?php echo $passError; ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-lg-4 control-label"><?php echo $form->label('repassword'); ?></label>
                    <div class="col-lg-8">
                      <?php echo $form->render('repassword'); ?>
                      <?php echo $repassError; ?>
                       <div class="label label-danger" id="repasserror"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-lg-offset-1 col-lg-9">
                      <?php echo $this->tag->submitButton(array('Update Password', 'class' => 'btn btn-primary', 'name' => 'update_password', 'id' => 'savepassBtn')); ?>
                    </div>
                    </div>
                    </form>
                  </div>
                </div>
                <div class="widget-foot">
                  <!-- Footer goes here -->
                </div>
              </div>
          </div>

        </div>
		  </div>

		<!-- Matter ends -->