        <!-- Page heading -->
        <div class="page-head">
        <!-- Page heading -->
          <h2 class="pull-left"> 
          <!-- page meta -->
          <span class="page-meta">Users</span>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="index.html"><i class="icon-home"></i> Home</a> 
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="#" class="bread-current">Users</a>
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="#" class="bread-current">Create</a>          
        </div>

        <div class="clearfix"></div>

        </div>
        <!-- Page heading ends -->



        <!-- Matter -->

        <div class="matter">
        <div class="container">

          <div class="row">

            <div class="col-md-12">
              <?php echo $this->getContent(); ?>

              <div class="widget wgreen">
                
                <div class="widget-head">
                  <div class="pull-left">Edit User</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="icon-remove"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                    <h6>User Account</h6>
                    <hr />
                    <!-- Form starts.  -->
                     <?php echo $this->tag->form(array('admin/editPartner/' . $user->userID, 'class' => 'form-horizontal')); ?>
                                <?php echo $form->render('huserName', array( 'value' => $user->userName)); ?>
                                <?php echo $form->render('huserEmail', array( 'value' => $user->userEmail)); ?>
                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('username'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('username', array( 'value' => $user->userName)); ?>
                                    <?php echo $form->messages('username'); ?>
                                  </div>
                                </div>
                              
                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('email'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('email', array( 'value' => $user->userEmail)); ?>
                                    <?php echo $form->messages('email'); ?>
                                  </div>
                                </div>
                    <h6>Change Password</h6>
                    <hr />
                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('password'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('password'); ?>
                                    <?php echo $passError; ?>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('repassword'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('repassword'); ?>
                                    <?php echo $repassError; ?>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-lg-offset-1 col-lg-9">
                                    <?php echo $this->tag->submitButton(array('Update Password', 'class' => 'btn btn-primary', 'name' => 'update_password')); ?>
                                  </div>
                                </div>

                    <h6>User Profile</h6>
                    <hr />
                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('firstname'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('firstname', array( 'value' => $user->userFirstname)); ?>
                                    <?php echo $form->messages('firstname'); ?>
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('lastname'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('lastname', array( 'value' => $user->userLastname)); ?>
                                    <?php echo $form->messages('lastname'); ?>
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('middlename'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('middlename', array( 'value' => $user->userMiddlename)); ?>
                                    <?php echo $form->messages('middlename'); ?>
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('address'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('address', array( 'value' => $user->userAddress)); ?>
                                    <?php echo $form->messages('address'); ?>
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('company'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('company', array( 'value' => $user->userCompany)); ?>
                                    <?php echo $form->messages('company'); ?>
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('contact'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('contact', array( 'value' => $user->userContact)); ?>
                                    <?php echo $form->messages('contact'); ?>
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('position'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('position', array( 'value' => $user->userPosition)); ?>
                                    <?php echo $form->messages('position'); ?>
                                  </div>
                                </div>                                 


                    <h6>Partner Profile</h6>
                    <hr />                                   
                                <div class="form-group">
                                <label class="col-lg-4 control-label"><?php echo $form->label('partnerName'); ?></label>
                                <div class="col-lg-8">
                                  <input type="hidden" name="hiddenPartnerName" value="<?php echo $partner->partnerName ?>">
                                    <?php echo $form->render('partnerName', array( 'value' => $partner->partnerName)); ?>
                                    <?php echo $form->messages('partnerName'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?php echo $form->label('partnerInfo'); ?></label>
                                <div class="col-lg-8">
                                    <?php echo $form->render('partnerInfo', array( 'value' => $partner->partnerInfo)); ?>
                                    <?php echo $form->messages('partnerInfo'); ?>
                                </div>
                            </div>


                            <div class="form-group">
                                  <label class="col-lg-4 control-label">Status</label>
                                  <div class="col-lg-8">
                                    <div class="radio">
                                      <label>
                                      <?php echo $this->tag->radioField(array('status', 'value' => 'active')); ?>
                                        Active
                                      </label>
                                    </div>
                                    <div class="radio">
                                      <label>
                                        <?php echo $this->tag->radioField(array('status', 'value' => 'deactivate')); ?>
                                        Deactivate
                                      </label>
                                    </div>
                                  </div>
                                </div>
                            
                                    <hr />
                                  <?php echo $form->render('csrf', array('value' => $this->security->getToken())); ?>
                                                                
                                <div class="form-group">
                                  <div class="col-lg-offset-1 col-lg-9">
                                    <?php echo $this->tag->submitButton(array('Save User', 'class' => 'btn btn-primary', 'name' => 'update_button')); ?>
                                    
                                    <button type="button" class="btn btn-default">Reset</button>
                                  </div>
                                </div>
                              </form>
                  </div>
                </div>
                  <div class="widget-foot">
                    <!-- Footer goes here -->
                    <a href="<?php echo $this->url->get('admin/partners'); ?>">Back to partner list</a>
                  </div>
              </div>  

            </div>

          </div>

        </div>
          </div>

        <!-- Matter ends -->''