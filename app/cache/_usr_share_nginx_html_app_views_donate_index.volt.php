<div class="inner-programs pull-left">
	<div class="tabs inner-tabs">
	        <?php echo $this->getContent(); ?>

	        <p>
	        	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod iaculis orci, eget varius augue tempus et. Quisque fermentum facilisis mi eu finibus. Quisque dapibus accumsan dignissim. Pellentesque sit amet porttitor massa, sit amet lacinia sapien. Vestibulum non eleifend sem. Pellentesque malesuada id justo ac ornare. Sed sit amet imperdiet leo.
	        </p>

	        <p>
	        	Nunc auctor, ante non mattis vestibulum, augue leo pharetra nisl, non interdum urna mi ac arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque condimentum a sapien a auctor. Pellentesque euismod odio non dui pretium, id cursus neque vehicula. Nunc pulvinar sapien ut libero blandit gravida. Cras tempus eu augue a placerat. Cras vel euismod odio. Pellentesque eget lacinia dolor, id varius sem. Fusce accumsan faucibus accumsan. Cras vel nulla mollis libero varius mattis. Vestibulum laoreet sodales tellus, ac tincidunt dolor cursus sit amet. Quisque eget nunc dignissim, dignissim lorem at, porttitor ligula. Nullam nec odio suscipit, ultrices eros id, suscipit lacus. Ut id magna congue, blandit magna ut, maximus orci.
	        </p>

	        <h3>
	        	I WANT TO SUPPORT ANG BAYAN KO PROGRAMS
	        	<br /><span class="fontNormal">Sample Fund Name</span>
	        </h3>
	        <p>
	        	Your gift to the Sample Fund Name will keep ANG BAYAN KO strong well into the future, providing a continuous stream of income that will fund programs in perpetuity.
	        </p>

	        <br />
	    <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top" class="form-horizontal">
	    	<input type="hidden" name="cmd" value="_donations">
			<input type="hidden" name="business" value="jplacsinto-facilitator@gmail.com">
			<input type="hidden" name="lc" value="US">
			<input type="hidden" name="item_name" value="Ang Bayan Ko">
			<input type="hidden" name="no_note" value="0">
			<input type="hidden" name="currency_code" value="PHP">
			<input type="hidden" name="return" value="http://angbayanko.org/donate/success">
			<input type="hidden" name="cancel_return" value="http://angbayanko.org/donate/canceled">			
			<input type="hidden" name="notify_url" value="http://angbayanko.org/donate/paypalipn">
			<input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
			<input type="hidden" name="amount" value="">

			<button type="submit" name="submit" class="btn btn-primary btn-lg">Donate With <i><strong>Paypal</strong></i></button> 
			<br /><br />
			<p>You will be redirected to the PayPal website to complete your payment.</p>
			
			<!-- <div class="text-center">
				<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt=
				"PayPal - The safer, easier way to pay online!">
				<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
			</div> -->
		</form>


    </div>


</div>
