          <input type="hidden" id="townID" value="<?php echo $townID; ?>">
          <?php if ($action == 'new' || $action == 'edit') { ?>
            <div id="partnerGallery" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
              <div class="modal-dialog" style="width: 900px;">
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h4 class="modal-title">Town Pictures</h4>
                    </div>
                    <div class="modal-body">
                      <div id="partnerPreloader"></div>
                      <?php echo $albumOptions; ?>
                      <div class="gallery" style="max-height:400px; overflow-y:auto" id="partnerGalleryWrapper">
                        <?php echo $allTownPictures; ?>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button id="afterUpload" type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
              </div>
            </div>
          <?php } ?>

          <?php if ($action == 'list') { ?>
          <!-- Modal Prompt-->
          <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Alert!</h4>
                </div>
                <div class="modal-body">
                  <p class="modal-message"></p>
                  <span class="modal-list-names"></span>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                  <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>



          <!-- Page heading -->
          <div class="page-head">
            <h2 class="pull-left">
            Towns
            </h2>

            <!-- Breadcrumb -->
            <div class="bread-crumb pull-right">
              <a href="index.html"><i class="icon-home"></i> Home</a> 
              <!-- Divider -->
              <span class="divider">/</span> 
              <?php echo $this->tag->linkTo(array('admin/towns', 'Towns')); ?>
              <span class="divider">/</span> 
              <span>Events</span>
            </div>

            <div class="clearfix"></div>

          </div>
          <!-- Page heading ends -->

          <!-- Matter -->

          <div class="matter">
            <?php echo $this->tag->form(array('class' => 'form-horizontal', 'id' => 'main-table-form')); ?>
            <div class="container">

              <!-- Table -->

              <div class="row">
                

                <div class="col-md-12">
                  <h2><?php echo $town->townName; ?></h2>
                  <?php echo $this->getContent(); ?>
                  <div class="widget">

                    <div class="widget-head">
                      <div class="pull-left">Town Info</div>
                      <div class="widget-icons pull-right">
                      </div>  
                      <div class="clearfix"></div>
                    </div>

                    <div class="padd">
                    <?php echo $townTab; ?>
                    </div>

                    <?php if ($action == 'list') { ?>
                    <div class="widget-content">
                      <?php echo $this->tag->hiddenField(array('csrf', 'value' => $this->security->getToken())); ?>
                      <input type="hidden" class="tbl-action" name="action" value=""/>
                      <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                      <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
                      <table class="table table-striped table-bordered table-hover tblusers">
                        <thead>
                          <tr>
                            <th colspan="5">
                              
                                <div class="col-lg-6">
                                  <div class="form-group">
                                    <label class="control-label col-lg-3">Search</label>
                                    <div class="col-lg-9"> 
                                      <?php echo $this->tag->textField(array('search_text', 'class' => 'form-control')); ?>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-lg-6">
                                     <?php echo $this->tag->submitButton(array('Search', 'class' => 'btn btn-default')); ?>
                                    <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                                  <a href="<?php echo $this->url->get('admin/townevents/' . $townID . '/new'); ?>" style="margin-right:25px" class="btn btn-primary pull-right" data-toggle="modal">+ Create New Event</a>
                                </div>
                              
                              
                            </th>
                          </tr>
                          <tr>
                            <th width="10"><?php echo $this->tag->checkField(array('select_all[]', 'class' => 'tbl_select_all')); ?></th>
                            <th width="150">Date</th>
                            <th>Event Title</th>
                            <th>Venue</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>

                          <?php if ($page->total_pages == 0) { ?>  
                          <tr>
                            <td colspan="5">No events found</td>
                          </tr>
                          <?php } else { ?>
                            <?php foreach ($page->items as $post) { ?>
                            <tr>
                              <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $post['eventID']; ?>"> </td>
                              <td><?php echo date('F j, Y', $post['eventDate']); ?></td>
                              <td class="name"><strong><?php echo $post['eventTitle']; ?></strong></td>
                              <td><?php echo $post['eventVenue']; ?></td>
                              <td>
                                <a href="<?php echo $this->url->get('admin/townevents/' . $townID . '/view/' . $post['eventID']); ?>" class="btn btn-xs btn-success" data-toggle="modal" data-album-id="<?php echo $post['eventID']; ?>"><i class="icon-ok"></i> </a>

                                <a href="<?php echo $this->url->get('admin/townevents/' . $townID . '/edit/' . $post['eventID']); ?>" class="btn btn-xs btn-warning" data-toggle="modal" data-album-id="<?php echo $post['eventID']; ?>"><i class="icon-pencil"></i> </a>

                                <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $post['eventID']; ?>"><i class="icon-remove"></i> </a>
                              </td>
                            </tr>
                            <?php } ?>
                          <?php } ?>
                        </tbody>
                      </table>

                      <div class="tblbottomcontrol" style="display:none">
                        <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
                        <a href="#" class="tbl_unselect_all"> Unselect </a>
                      </div>

                      <div class="widget-foot">

                        <?php if ($page->total_pages > 1) { ?>

                        <ul class="pagination pull-right">

                          <?php if ($page->current != 1) { ?>
                          <li><?php echo $this->tag->linkTo(array('admin/townevents/' . $townID . '?page=' . $page->before, 'Prev')); ?></li>
                          <?php } ?>

                          <?php foreach (range(1, $page->total_pages) as $index) { ?>
                          <?php if ($page->current == $index) { ?>
                          <li><?php echo $this->tag->linkTo(array('admin/townevents/' . $townID . '?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                          <?php } else { ?>
                          <li><?php echo $this->tag->linkTo(array('admin/townevents/' . $townID . '?page=' . $index, $index)); ?></li>
                          <?php } ?>
                          <?php } ?>         

                          <?php if ($page->current != $page->total_pages) { ?>                 
                          <li><?php echo $this->tag->linkTo(array('admin/townevents/' . $townID . '?page=' . $page->next, 'Next')); ?></li>
                          <?php } ?>
                        </ul>
                        <?php } ?>
                          <a href="/admin/towns" class="btn btn-default">Back to Town List</a>
                        <div class="clearfix"></div> 
                      </div>

                    </div>
                    <?php } ?>




                    <?php if ($action == 'new') { ?>
                    <div class="padd">
                          <h2>Add New Event</h2>
                            <!--code start here-->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Name</label>
                                <div class="col-lg-8">
                                    <?php echo $evtForm->render('event_name'); ?>
                                    <?php echo $evtForm->messages('event_name'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Date</label>
                                <div class="col-lg-8">
                                    <span class="datePicker">
                                        <?php echo $evtForm->render('event_date'); ?>
                                        <span class="add-on">
                                            <i class="btn btn-info btn-lg icon-calendar"></i>
                                        </span>
                                    </span>
                                    <?php echo $evtForm->messages('event_date'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Venue</label>
                                <div class="col-lg-8">
                                    <?php echo $evtForm->render('event_venue'); ?>
                                    <?php echo $evtForm->messages('event_venue'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label"></label>
                                <div class="col-lg-8">
                                    <a href="#partnerGallery" data-toggle="modal" class="btn btn-default pull-right post-add-media"><i class="icon-paper-clip"></i> Add Media</a>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Details</label>
                                <div class="col-lg-8">
                                    <?php echo $evtForm->render('event_details'); ?>
                                    <?php echo $evtForm->messages('event_details'); ?>
                                </div>
                            </div>

                            <hr>
                            <div class="form-group">
                                <div class="col-lg-offset-1 col-lg-9">
                                    <?php echo $this->tag->linkTo(array('admin/townevents/' . $townID, 'Cancel', 'class' => 'btn btn-default')); ?>
                                    <?php echo $this->tag->submitButton(array('Create Event', 'name' => 'savePartnerEvent', 'class' => 'btn btn-primary')); ?>
                                    <?php echo $evtForm->render('csrf', array('value' => $this->security->getToken())); ?>
                                    <?php echo $evtForm->messages('csrf'); ?>
                                </div>
                            </div>
                            <!--code ends here-->
                        </div>
                    <?php } ?>

                    <?php if ($action == 'view') { ?>

                      <div class="padd">
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Title</label>
                            <div class="col-lg-8">
                                <?php echo $event->eventTitle; ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Date</label>
                            <div class="col-lg-8">
                                <?php echo date('F j, Y', $event->eventDate); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Venue</label>
                            <div class="col-lg-8">
                                <?php echo $event->eventVenue; ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Details</label>
                            <div class="col-lg-8">
                                <?php echo $event->eventDetails; ?>
                            </div>
                        </div>
                      
                      </div>

                      <div class="widget-foot">
                        <a href="/admin/townevents/<?php echo $townID; ?>" class="btn btn-default"> Back to event list</a>
                        <div class="clearfix"></div> 
                      </div>
                    <?php } ?>

                    <?php if ($action == 'edit') { ?>
                    <div class="padd">
                            <!--code start here-->
                            <h2>Edit Event</h2>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Name</label>
                                <div class="col-lg-8">
                                    <input type="hidden" name="orig_event_name" value="<?php echo $event->eventTitle ?>">
                                    <?php echo $evtForm->render('event_name', array( 'value' => $event->eventTitle)); ?>
                                    <?php echo $evtForm->messages('event_name'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Date</label>
                                <div class="col-lg-8">
                                    <span class="datePicker">
                                        <?php echo $evtForm->render('event_date', array( 'value' => date("Y-m-d", $event->eventDate))); ?>
                                        <span class="add-on">
                                            <i class="btn btn-info btn-lg icon-calendar"></i>
                                        </span>
                                    </span>
                                    <?php echo $evtForm->messages('event_date'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Venue</label>
                                <div class="col-lg-8">
                                    <?php echo $evtForm->render('event_venue', array( 'value' => $event->eventVenue)); ?>
                                    <?php echo $evtForm->messages('event_venue'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label"></label>
                                <div class="col-lg-8">
                                    <a href="#partnerGallery" data-toggle="modal" class="btn btn-default pull-right post-add-media"><i class="icon-paper-clip"></i> Add Media</a>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Details</label>
                                <div class="col-lg-8">
                                    <?php echo $evtForm->render('event_details', array( 'value' => $event->eventDetails)); ?>
                                    <?php echo $evtForm->messages('event_details'); ?>
                                </div>
                            </div>

                            <hr>
                            <div class="form-group">
                                <div class="col-lg-offset-1 col-lg-9">
                                    <?php echo $this->tag->linkTo(array('admin/townevents/' . $townID, 'Cancel', 'class' => 'btn btn-default')); ?>
                                    <?php echo $this->tag->submitButton(array('Save Changes', 'name' => 'savePartnerEvent', 'class' => 'btn btn-primary')); ?>
                                    <?php echo $evtForm->render('csrf', array('value' => $this->security->getToken())); ?>
                                    <?php echo $evtForm->messages('csrf'); ?>
                                </div>
                            </div>
                            <!--code ends here-->
                        </div>

                        <div class="widget-foot">
                            <a href="/admin/towns" class="btn btn-default">Back to Town List</a>
                        <div class="clearfix"></div> 
                        </div>
                    <?php } ?>


                    <!--end d2-->

                  </div>


                </div>

              </div>


            </div>
          </form>
        </div>

<!-- Matter ends -->