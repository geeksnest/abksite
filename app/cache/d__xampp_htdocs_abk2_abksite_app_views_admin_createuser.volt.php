	    <!-- Page heading -->
	    <div class="page-head">
        <!-- Page heading -->
	      <h2 class="pull-left"> 
          <!-- page meta -->
          <span class="page-meta">Users</span>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="/admin"><i class="icon-home"></i> Home</a> 
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="/admin/users">Users</a>
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="" class="bread-current">Create</a>          
        </div>

        <div class="clearfix"></div>

	    </div>
	    <!-- Page heading ends -->



	    <!-- Matter -->

	    <div class="matter">
        <div class="container">

          <div class="row">

            <div class="col-md-12">
              <?php echo $this->getContent(); ?>

              <div class="widget wgreen">
                
                <div class="widget-head">
                  <div class="pull-left">Create User</div>
                  <div class="widget-icons pull-right">
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">
                    <h6>User Account</h6>
                    <hr />
                    <!-- Form starts.  -->
                     <?php echo $this->tag->form(array('admin/createuser', 'class' => 'form-horizontal')); ?>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label class=""><?php echo $form->label('username'); ?></label>
                                  <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('username'); ?>
                                    <?php echo $form->messages('username'); ?>
                                  </div>
                                </div>
                              
                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label class=""><?php echo $form->label('email'); ?></label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('email'); ?>
                                    <?php echo $form->messages('email'); ?>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label class=""><?php echo $form->label('password'); ?></label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('password'); ?>
                                    <?php echo $form->messages('password'); ?>
                                    <div class="label label-danger" id="passerror"></div>
                                    <div class="label label-danger" id="spaceerror"></div>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label class=""><?php echo $form->label('repassword'); ?></label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('repassword'); ?>
                                    <?php echo $form->messages('repassword'); ?>
                                    <div class="label label-danger" id="repasserror"></div>

                                  </div>
                                </div>

                    <h6>User Profile</h6>
                    <hr />
                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label><?php echo $form->label('firstname'); ?></label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('firstname'); ?>
                                    <?php echo $form->messages('firstname'); ?>
                                  </div>                                  
                                </div>  

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label><?php echo $form->label('lastname'); ?></label>
                                  <span class="asterisk">*</span>
                                   </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('lastname'); ?>
                                    <?php echo $form->messages('lastname'); ?>
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label><?php echo $form->label('middlename'); ?></label>
                                </div>
                                  
                                  <div class="col-lg-8">
                                    <?php echo $form->render('middlename'); ?>
                                    <?php echo $form->messages('middlename'); ?>
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label><?php echo $form->label('address'); ?></label>
                                 </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('address'); ?>
                                    <?php echo $form->messages('address'); ?>
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label><?php echo $form->label('company'); ?></label>
                                  </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('company'); ?>
                                    <?php echo $form->messages('company'); ?>
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label><?php echo $form->label('contact'); ?></label>
                                  <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('contact'); ?>
                                    <?php echo $form->messages('contact'); ?>
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label><?php echo $form->label('position'); ?></label>
                                  </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('position'); ?>
                                    <?php echo $form->messages('position'); ?>
                                  </div>
                                </div>  

                    <h6>User Role</h6>
                    <hr />                                   
                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>Roles</label>
                                  <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                  <?php foreach ($tblroles as $role) { ?>
                                    <label class="checkbox-inline">
                                      <?php echo $this->tag->checkField(array($role->roleCode, 'value' => $role->roleCode)); ?>
                                      <?php echo $role->roleDescription; ?>
                                    </label> <br/>
                                  <?php } ?>
                                    <?php echo $roleError; ?>                                 
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>Status <span class="asterisk">*</span></label>
                                   </div>
                                  <div class="col-lg-8">
                                    <div class="radio">
                                      <label>
                                      <?php echo $this->tag->radioField(array('status', 'value' => 'active', 'checked' => 'checked')); ?>
                                        Active
                                      </label>
                                    </div>
                                    <div class="radio">
                                      <label>
                                        <?php echo $this->tag->radioField(array('status', 'value' => 'deactivate')); ?>
                                        Deactivate
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                    <hr />
                                  
                                                                      
                                <div class="form-group">
                                  <div class="col-lg-offset-1 col-lg-9">
                                    <?php echo $this->tag->submitButton(array('Save User', 'id' => 'btnSubmit', 'class' => 'btn btn-primary', 'id' => 'savepassBtn')); ?>
                                    <button type="reset" id="btnReset" class="btn btn-default">Reset</button>
                                  </div>
                                </div>
                              </form>
                  </div>
                </div>
                  <div class="widget-foot">
                    <!-- Footer goes here -->
                  </div>
              </div>  

            </div>

          </div>

        </div>
		  </div>

		<!-- Matter ends -->