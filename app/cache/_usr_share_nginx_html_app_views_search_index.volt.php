<div class="inner-programs pull-left">
	<div class="tabs inner-tabs">
	        <?php echo $this->getContent(); ?>

	        <?php if(!empty($page)){ ?>
		        <?php if(empty($page->items)){
		        	echo '<h3>No search result found.</h3>Please enter your serach keyword.';
		        } ?>
	            <?php foreach ($page->items as $s) { ?>
	              <h4 class="fontNormal"><a href="<?php echo $s['searchUrl'] ?>"><?php echo $s['searchTitle'] ?></a></h4>
	              <?php echo $s['searchDesc'] ?>
	              <hr />
	            <?php } ?>

	            <div class="">
	              <!-- Footer goes here -->
	              <?php if ($page->total_pages > 1) { ?>   
	              <ul class="pagination pull-right">
	                <?php if ($page->current != 1) { ?>
	                <li><?php echo $this->tag->linkTo(array('search?page=' . $page->before . '&&s=' . $searchString, 'Prev')); ?></li>
	                <?php } ?>

	                <?php foreach (range(1, $page->total_pages) as $index) { ?>
	                <?php if ($page->current == $index) { ?>
	                <li><?php echo $this->tag->linkTo(array('search?page=' . $index . '&&s=' . $searchString, $index, 'style' => 'background-color:#eee')); ?></li>
	                <?php } else { ?>
	                <li><?php echo $this->tag->linkTo(array('search?page=' . $index . '&&s=' . $searchString, $index)); ?></li>
	                <?php } ?>
	                <?php } ?>         

	                <?php if ($page->current != $page->total_pages) { ?>                 
	                <li><?php echo $this->tag->linkTo(array('search?page=' . $page->next . '&&s=' . $searchString, 'Next')); ?></li>
	                <?php } ?>
	              </ul>
	              <div class="clearfix"></div>
	              <?php } ?>
	            </div>
            <?php } ?>

    </div>


</div>