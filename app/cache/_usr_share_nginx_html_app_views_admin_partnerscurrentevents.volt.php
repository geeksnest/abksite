<div id="deleteEventsConf" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Delete Event</h4>
      </div>
      <div class="modal-body">
        <span id="deleteMessage"></span>
      </div>
      <div class="modal-footer" id="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
        <button type="button" id="deleteEventBtn" class="btn btn-danger">Delete</button>
      </div>
    </div>
  </div>
</div>

<!-- Page heading -->
<div class="page-head">
    <h2 class="pull-left"><i class="icon-suitcase"></i> <?php echo $partner->partnerName; ?></h2>

    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
        <a href="<?php echo $this->url->get('admin'); ?>"><i class="icon-home"></i> Home</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="<?php echo $this->url->get('admin/partners'); ?>">Partners</a>
        <span class="divider">/</span>
        <a href="<?php echo $this->url->get('admin/partnersinfo/'); ?><?php echo $partnerID; ?>">View</a>
        <span class="divider">/</span>
        <span>Current Events</span>
    </div>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">
    <?php echo $this->tag->form(array('class' => 'form-horizontal', 'id' => 'eventsForm')); ?>
    <div class="container">

        <!-- Table -->

        <div class="row">



            <div class="col-md-12">

                <div class="widget">

                    <div class="widget-head">
                        <div class="pull-left">ABK Partner Info</div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">
                            <?php echo $tab; ?>
                            <br>
                            <?php echo $this->getContent(); ?>
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label col-lg-3">Search</label>
                                        <div class="col-lg-9"> 
                                            <?php echo $this->tag->textField(array('search_text', 'class' => 'form-control')); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <?php echo $this->tag->submitButton(array('Search', 'name' => 'searchBtn', 'class' => 'btn btn-default')); ?>
                                    <?php echo $this->tag->submitButton(array('Clear Search', 'class' => 'btn btn-default', 'name' => 'clear_search')); ?>
                                    <a href="<?php echo $this->url->get('admin/createpartnerevent/'); ?><?php echo $partnerID; ?>" type="button" class="btn btn-primary pull-right">+ Create New Event</a>
                                </div>
                            </div> 
                            <hr>


                            <?php $v99424650500864573761iterated = false; ?><?php $v99424650500864573761iterator = $page->items; $v99424650500864573761incr = 0; $v99424650500864573761loop = new stdClass(); $v99424650500864573761loop->length = count($v99424650500864573761iterator); $v99424650500864573761loop->index = 1; $v99424650500864573761loop->index0 = 1; $v99424650500864573761loop->revindex = $v99424650500864573761loop->length; $v99424650500864573761loop->revindex0 = $v99424650500864573761loop->length - 1; ?><?php foreach ($v99424650500864573761iterator as $event) { ?><?php $v99424650500864573761loop->first = ($v99424650500864573761incr == 0); $v99424650500864573761loop->index = $v99424650500864573761incr + 1; $v99424650500864573761loop->index0 = $v99424650500864573761incr; $v99424650500864573761loop->revindex = $v99424650500864573761loop->length - $v99424650500864573761incr; $v99424650500864573761loop->revindex0 = $v99424650500864573761loop->length - ($v99424650500864573761incr + 1); $v99424650500864573761loop->last = ($v99424650500864573761incr == ($v99424650500864573761loop->length - 1)); ?><?php $v99424650500864573761iterated = true; ?>
                            <?php if ($v99424650500864573761loop->first) { ?>
                            <!--head of table-->
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                  <tr>
                                    <th width="10">
                                      <span class="uni">
                                        <input type="checkbox" name="select_all[]" class="tbl_select_all">
                                    </span>
                                </th>
                                <th>Date</th>
                                <th>Event Title</th>
                                <th>Venue</th>
                                <th></th>
                            </tr>
                        </thead>
                        <?php } ?>
                        <!--body of table-->
                        <tbody>
                            <tr>
                                <td>                            
                                  <span class="uni">
                                    <input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $event->eventID; ?>">
                                  </span>
                                </td>
                                <td width="150">
                                    <?php echo date('F j, Y', $event->eventDate); ?>
                                </td>
                                <td>
                                    <strong><a href="/admin/editEvent/<?php echo $event->eventID; ?>?viewonly"><?php echo $event->eventTitle; ?></a></strong>
                                </td>
                                <td>
                                    <?php echo $event->eventVenue; ?>
                                </td>
                                <td width="100">

                                  <a href="/admin/editEvent/<?php echo $event->eventID; ?>" class="btn btn-xs btn-default" data-toggle="modal"><i class="icon-pencil"></i></a>
                                  <a data-event-id="<?php echo $event->eventID; ?>" data-eventname = "<?php echo $event->eventTitle; ?>" href="#deleteEventsConf" class="deleteEventLink btn btn-xs btn-danger" data-toggle="modal"><i class="icon-trash"></i></a>
                                </td>
                            </tr>
                        </tbody>
                        <?php if ($v99424650500864573761loop->last) { ?>
                    </table>
                    <?php } ?>
                    
                    <?php $v99424650500864573761incr++; } if (!$v99424650500864573761iterated) { ?>
                    <div class="alert">No events found</div>
                    <?php } ?>
                    <div class="tblbottomcontrol" style="display: none;">
                        <a href="#deleteEventsConf" id="deleteAllSelectedEventLink" data-toggle="modal"> Delete all Selected </a> |
                        <a href="#" class="tbl_unselect_all"> Unselect </a>
                    </div>
                    <input type="hidden" name="delEventSingle" id="delEventSingle">
                </div>
            </div>


            <div class="widget-foot">
                <?php if ($showBackToList) { ?> <?php echo $this->tag->linkTo(array('admin/partners', 'Back to Partner List', 'class' => 'btn btn-default')); ?> <?php } ?>

                <?php if ($page->total_pages > 1) { ?>   
                <ul class="pagination pull-right">
                    <?php if ($page->current != 1) { ?>
                    <li><?php echo $this->tag->linkTo(array('admin/partnersinfo/' . $partnerID . '/events?page=' . $page->before, 'Prev')); ?></li>
                    <?php } ?>

                    <?php $v99424650500864573761iterator = range(1, $page->total_pages); $v99424650500864573761incr = 0; $v99424650500864573761loop = new stdClass(); $v99424650500864573761loop->length = count($v99424650500864573761iterator); $v99424650500864573761loop->index = 1; $v99424650500864573761loop->index0 = 1; $v99424650500864573761loop->revindex = $v99424650500864573761loop->length; $v99424650500864573761loop->revindex0 = $v99424650500864573761loop->length - 1; ?><?php foreach ($v99424650500864573761iterator as $index) { ?><?php $v99424650500864573761loop->first = ($v99424650500864573761incr == 0); $v99424650500864573761loop->index = $v99424650500864573761incr + 1; $v99424650500864573761loop->index0 = $v99424650500864573761incr; $v99424650500864573761loop->revindex = $v99424650500864573761loop->length - $v99424650500864573761incr; $v99424650500864573761loop->revindex0 = $v99424650500864573761loop->length - ($v99424650500864573761incr + 1); $v99424650500864573761loop->last = ($v99424650500864573761incr == ($v99424650500864573761loop->length - 1)); ?>
                    <?php if ($page->current == $index) { ?>
                    <li><?php echo $this->tag->linkTo(array('admin/partnersinfo/' . $partnerID . '/events?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                    <?php } else { ?>
                    <li><?php echo $this->tag->linkTo(array('admin/partnersinfo/' . $partnerID . '/events?page=' . $index, $index)); ?></li>
                    <?php } ?>
                    <?php $v99424650500864573761incr++; } ?>         

                    <?php if ($page->current != $page->total_pages) { ?>                 
                    <li><?php echo $this->tag->linkTo(array('admin/partnersinfo/' . $partnerID . '/events?page=' . $page->next, 'Next')); ?></li>
                    <?php } ?>
                </ul>
                <div class="clearfix"></div> 
                <?php } ?>
            </div>

        </div>


    </div>

</div>


</div>
</form>
</div>

<!-- Matter ends -->