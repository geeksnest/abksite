    <div class="inner-programs pull-left">
      <?php if ($page->pageBanner != '') { ?>
      <div class="dm-latest-news-head">
        <img src="<?php echo $page->pageBanner; ?>">
      </div>
      <?php } ?>
      <div class="tabs inner-tabs">
        <ul class="nav nav-tabs">
          <!-- Use unique name in anchor tag -->

          <?php if ($page->specialPage == 1) { ?>
          <li class="active"><a href="#news" data-toggle="tab">Latest Breaking News</a></li>
          <li><a href="#about" data-toggle="tab">About This Page</a></li>

          <?php 
          if(empty($abk_vol_username)){
            ?>
            <li><a href="#signup" data-toggle="tab">Signup</a></li>
            <?php
          }
          ?>

          <?php } ?>
          
          <!-- <?php echo $subpages; ?> -->
        </ul>
        <div class="tab-content">
          <?php echo $actOptions; ?>
          <?php if ($page->specialPage == 1) { ?>
          <div class="tab-pane inner-news active" id="news">
            <?php if ($post->total_items == 0) { ?>
            No News Added.
            <?php } else { ?>
            <?php foreach ($post->items as $p) { ?>
            <?php if ($p->postStatus == 'publish') { ?>
            <div class="entry">
             <h2><a href="<?php echo $this->url->get('post/news/' . $p->postSlug); ?>"><?php echo $p->postTitle; ?></a></h2>
             <!-- Meta details -->
             <div class="meta">
              <div><i class="icon-calendar"></i> <?php echo date('d-m-Y', $p->postPublishDate); ?></div>
              <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div> <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-type="button"></div> <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
              <span class="pull-right"><i class="icon-comment"></i> <a href="#">2 Comments</a></span>
            </div>

            <!-- Thumbnail -->
            <?php if ($p->postFeatureImage) { ?>
            <div class="bthumb3">
              <a href="#"><img src="<?php echo $p->postFeatureImage; ?>" alt="" class="img-responsive"></a>
            </div>
            <?php } ?>
            <!-- Para -->
            <p><?php echo $out = strlen(strip_tags($p->postContent)) > 550 ? substr(strip_tags($p->postContent),0,550)."..." : $p->postContent; ?></p>

            <!-- Read more -->
            <div class="button"><a href="<?php echo $this->url->get('post/news/' . $p->postSlug); ?>">Read More...</a></div>
            <div class="clearfix"></div>
          </div>    
          <?php } ?>
          <?php } ?>
          <?php } ?>

          <?php if ($post->total_items > 1) { ?>
          <div class="paging">
            <?php if ($post->current != 1) { ?>
            <?php echo $this->tag->linkTo(array('admin/users?page=' . $post->before, 'Prev')); ?>
            <?php } ?>

            <?php foreach (range(1, $post->total_pages) as $index) { ?>
            <?php if ($post->current == $index) { ?>
            <?php echo $this->tag->linkTo(array('site/pages/' . $page->pageSlug . '?page=' . $index, $index, 'class' => 'current')); ?>
            <?php } else { ?>
            <?php echo $this->tag->linkTo(array('site/pages/' . $page->pageSlug . '?page=' . $index, $index)); ?>
            <?php } ?>
            <?php } ?>         

            <?php if ($post->current != $post->total_pages) { ?>                 
            <?php echo $this->tag->linkTo(array('site/pages/' . $page->pageSlug . '?page=' . $post->next, 'Next')); ?>
            <?php } ?>
          </div>
          <?php } ?>
        </div>
        <?php } ?>

        <div class="tab-pane inner-news" id="about">
          <?php echo $page->pageContent; ?>
        </div>
        <div class="tab-pane inner-news" id="signup">
          <div class="text-right">

            I already have my account. <a href="#loginModal" data-toggle="modal">Signin</a></strong>
          </div>



          <form method="post" id="signupForm" action="/site/signup">
            <h3 class="fontNormal">1. Profile Information</h3>
            <small>Step 1 of 2</small>
            <hr />
            <div id="formResult"></div>


            <div class="well well-sm border-flat">
              Personal Profile
            </div>

            <div class="signupRows">
              <div class="form-group">
                <label>Your Title</label>
                <div class="row">
                  <div class="col-xs-4">
                    <select name="title" class="form-control">
                      <option value="Mr.">Mr.</option>
                      <option value="Ms.">Ms.</option>
                      <option value="Mrs.">Mrs.</option>
                      <option value="" selected="selected">Select Title</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label>First Name</label>
                <input name="fname" type="text" class="form-control border-flat" placeholder="Enter first name">
              </div>

              <div class="form-group">
                <label>Middle Name</label>
                <input name="mname" type="text" class="form-control border-flat" placeholder="Enter middle name">
              </div>

              <div class="form-group">
                <label>Last Name</label>
                <input name="lname" type="text" class="form-control border-flat" placeholder="Enter last name">
              </div>

              <div class="form-group">
                <label>Extension Name <small class="text-muted">(optional)</small></label>
                <div class="row">
                  <div class="col-xs-4">
                    <input name="extname" type="text" class="form-control border-flat" placeholder="Enter extension name">
                  </div>
                </div>
              </div>
            </div>  

            <div class="well well-sm border-flat">
              Contact Details
            </div>

            <div class="signupRows">
              <div class="form-group">
                <label>Complete Address</label>
                <input name="address" type="text" class="form-control border-flat" placeholder="Enter complete address">
              </div>

              <div class="form-group">
                <label>Email address</label>
                <input name="email" type="text" class="form-control border-flat" placeholder="Enter email">
              </div>

              <div class="form-group">
                <label>Phone Number <small class="text-muted">(optional)</small></label>
                <input name="phone" type="text" class="form-control border-flat phone" placeholder="Enter phone number">
              </div>
            </div>  

            <div class="well well-sm border-flat">
              Account Information
            </div>

            <div class="signupRows">
              <div class="form-group">
                <label>Username</label>
                <input name="username" type="text" class="form-control border-flat" placeholder="Enter username">
              </div>

              <div class="form-group">
                <label>Password</label>
                <input name="password" type="password" class="form-control border-flat" placeholder="Enter password">
              </div>

              <div class="form-group">
                <label>Confirm Password</label>
                <input name="repassword" type="password" class="form-control border-flat" placeholder="Re-type password">
              </div>
            </div>  


            <button type="submit" class="btn btn-primary" id="signupNext">Next</button>
          </form>

          <div id="finalForm"></div>

        </div>


      </div>
    </div>
  </div>

