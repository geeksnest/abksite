

            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> Newsletter</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="index.html"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <span>Newsletter</span>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->

            <div class="matter">
              <?php echo $this->tag->form(array('admin/createnewsletter', 'class' => 'form-horizontal', 'id' => 'main-table-form')); ?>
              <div class="container">



                <!-- Table -->

                <div class="row">


                  <div class="col-md-12">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="/admin/createnewsletter">Create Newsletter</a></li>
                      <li><a href="/admin/newsletter">Subscribers</a></li>
                    </ul>
                  </div>
                  <br />

                  <div class="col-md-12">
                    <?php echo $this->getContent(); ?>
                    
                    
                        <div class="widget">

                          <div class="widget-head">
                            <div class="pull-left">Create Newsletter</div>
                            <div class="widget-icons pull-right">
                              <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                              <a href="#" class="wclose"><i class="icon-remove"></i></a>
                            </div>  
                            <div class="clearfix"></div>
                          </div>


                          <div class="widget-content">
                            <?php echo $this->tag->hiddenField(array('csrf', 'value' => $this->security->getToken())); ?>

                            <div class="padd">
                              <label>Subject</label>
                              <?php echo $this->tag->textField(array('subject', 'class' => 'form-control', 'placeholder' => 'Subject')); ?>

                              <br />
                              <label>Message</label>
                              <?php echo $this->tag->textArea(array('message', 'placeholder' => 'Message', 'class' => 'programPageText')); ?>
                            </div>

                            <div class="widget-foot">
                              <input type="reset" class="btn btn-default" value="Clear">
                              <!-- <label><input type="checkbox" name="save" value="1" checked="checked"> Save</label> -->
                              <input type="submit" name="send" class="btn btn-primary" value="Send Newsletter">
                              <div class="clearfix"></div> 
                            </div>

                          </div>

                        </div>


                      </div>

                    </div>


                  </div>
                </form>
              </div>

<!-- Matter ends -->