<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <?php echo $this->tag->getTitle(); ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <?php echo $this->tag->javascriptInclude('js/sparklines.js'); ?>
  <?php echo $this->tag->stylesheetLink('css/bootstrap.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/font-awesome.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/bootstrap-responsive.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/jquery-ui.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/fullcalendar.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/prettyPhoto.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/rateit.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/fullcalendar.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/bootstrap-datetimepicker.min.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/bootstrap-switch.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/style/widgets.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/jquery.fileupload.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/abkadmin/style.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/chosen.css'); ?>

  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?php echo $this->url->getBaseUri(); ?>public/img/favicon/favicon.png">
   <script>
         function displayDate() {
         var months = new Array(12);
          months[0] = "January";
          months[1] = "February";
          months[2] = "March";
          months[3] = "April";
          months[4] = "May";
          months[5] = "June";
          months[6] = "July";
          months[7] = "August";
          months[8] = "September";
          months[9] = "October";
          months[10] = "November";
          months[11] = "December";

          var current_date = new Date();
          month_value = current_date.getMonth();
          day_value = current_date.getDate();
          year_value = current_date.getFullYear();
          var today = months[month_value]+' '+day_value+', '+year_value;
          document.getElementById("today").value = today;
         }

        // keep selected option
         $(function() {
        if (localStorage.getItem('form_frame')) {
            $("#form_frame option").eq(localStorage.getItem('form_frame')).prop('selected', true);
        }

        $("#form_frame").on('change', function() {
            localStorage.setItem('form_frame', $('option:selected', this).index());
        });
        });

        $("#reset").on("click", function () {
        $('#form_frame option').prop('selected', function() {
        return this.defaultSelected;
        });
        });
      </script>

      
</head>

<body onload="displayDate()">

  <div class="navbar navbar-fixed-top bs-docs-nav" role="banner">

    <div class="conjtainer">
      <!-- Menu button for smallar screens -->
      <div class="navbar-header">
        <button class="navbar-toggle btn-navbar" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
          <span>Menu</span>
        </button>
        <!-- Site name for smallar screens -->
        <a href="index.html" class="navbar-brand hidden-lg">ABK CMS</a>
      </div>
      
      <!-- Navigation starts -->
      <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">         
        <div class="col-md-3 header-logo">
          <?php echo $this->tag->image(array('img/admintoppart.png')); ?>
        </div>
        <!-- Links -->
        <ul class="nav navbar-nav pull-right">
          <li class="dropdown pull-right">            
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
              <i class="icon-user"></i> <?php echo $abk_user; ?> <b class="caret"></b>              
            </a>
            
            <!-- Dropdown menu -->
            <ul class="dropdown-menu">
              <li><a href="<?php echo $this->url->get('admin/editprofile'); ?>"><i class="icon-user"></i> Profile</a></li>
              <li class="<?php if($abk_userlevel != 1) echo "hide"; ?>"><a href="/admin/settings"><i class="icon-cogs"></i> Settings</a></li>
               <!-- <li><a href="/manual/ABKusermanual.pdf" download="ABK users manual"><i class="icon-question-sign"></i> Help</a></li> -->
               <li><a href="/manual/ABKusermanual.pdf" data="/manual/ABKusermanual.pdf" type="application/pdf"><i class="icon-question-sign"></i> Help</a></li>
              <li><a href="<?php echo $this->url->get('admin/logout'); ?>"><i class="icon-off"></i> Logout</a></li>
            </ul>
          </li>
          
        </ul>
      </nav>

    </div>

  </div>


<!-- Header starts -->
  <header>
    <div class="container">
      <div class="row">

        <!-- Button section -->
        <div class="col-md-4">

          <!-- Buttons -->
          <ul class="nav nav-pills">

            <!-- Comment button with number of latest comments count -->
            <!-- set data-toggle to "dropdown" to show menu -->
            <li class="dropdown dropdown-big">
              <a class="dropdown-toggle" style="color: #FFF; background: #428bca;" href="#" data-toggle="">
                <i class="icon-facebook-sign"></i> Facebook <span style="color: #428bca; background: #FFF; margin-left: 8px;" class="label"><?php echo $fbLikes; ?></span> 
              </a>

                <ul class="dropdown-menu">
                  <li>
                    <!-- Heading - h5 -->
                    <h5><i class="icon-facebook-sign"></i> Timeline</h5>
                    <!-- Use hr tag to add border -->
                    <hr />
                  </li>
                  <li>
                    <!-- List item heading h6 -->
                    <h6><a href="#">Future Integration for ABK</a> <span class="label label-warning pull-right">10:42</span></h6>
                    <div class="clearfix"></div>
                    <hr />
                  </li>
                  <li>
                    <h6><a href="#">Future Integration for ABK</a> <span class="label label-warning pull-right">20:42</span></h6>
                    <div class="clearfix"></div>
                    <hr />
                  </li>
                  <li>Future Integration for ABK</a> <span class="label label-warning pull-right">14:42</span></h6>
                    <div class="clearfix"></div>
                    <hr />
                  </li>                  
                  <li>
                    <div class="drop-foot">
                      <a href="#">View All</a>
                    </div>
                  </li>                                    
                </ul>
            </li>

            <!-- Message button with number of latest messages count-->
            <li class="dropdown dropdown-big">
              <a class="dropdown-toggle" style="color: #FFF; background: #5bc0de;" href="#" data-toggle="">
                <i class="icon-twitter-sign"></i> Twitter <span style="margin-left: 8px; color: #5bc0de; background: #FFF;" class="label"><?php echo $followerCount; ?></span> 
              </a>

                <ul class="dropdown-menu">
                  <li>
                    <!-- Heading - h5 -->
                    <h5><i class="icon-twitter-sign"></i> Tweets</h5>
                    <!-- Use hr tag to add border -->
                    <hr />
                  </li>
                  <li>
                    <!-- List item heading h6 -->
                    <h6><a href="#">Future Integration for ABK?</a></h6>
                    <!-- List item para -->
                    <p>Quisque eu consectetur erat eget  semper...</p>
                    <hr />
                  </li>
                  <li>
                    <h6><a href="#">Future Integration for ABK?</a></h6>
                    <p>Quisque eu consectetur erat eget  semper...</p>
                    <hr />
                  </li>
                  <li>
                    <div class="drop-foot">
                      <a href="#">View All</a>
                    </div>
                  </li>                                    
                </ul>
            </li>

            <!-- Members button with number of latest members count -->
            <li class="dropdown dropdown-big">
              <a class="dropdown-toggle" style="color: #FFF; background: #ff5d5e;" href="#" data-toggle="">
                <i class="icon-google-plus-sign"></i> Google Plus <span style="margin-left: 8px; color: #ff5d5e; background: #FFF;" class="label"><?php echo $plusonersCount; ?></span> 
              </a>

                <ul class="dropdown-menu">
                  <li>
                    <!-- Heading - h5 -->
                    <h5><i class="icon-google-plus-sign"></i> Timeline</h5>
                    <!-- Use hr tag to add border -->
                    <hr />
                  </li>
                  <li>
                    <!-- List item heading h6-->
                    <h6><a href="#">Future Integration for ABK</a> <span class="label label-warning pull-right">Free</span></h6>
                    <div class="clearfix"></div>
                    <hr />
                  </li>
                  <li>
                    <h6><a href="#">Future Integration for ABK</a> <span class="label label-important pull-right">Premium</span></h6>
                    <div class="clearfix"></div>
                    <hr />
                  </li>
                  <li>
                    <h6><a href="#">Future Integration for ABK</a> <span class="label label-warning pull-right">Free</span></h6>
                    <div class="clearfix"></div>
                    <hr />
                  </li>                  
                  <li>
                    <div class="drop-foot">
                      <a href="#">View All</a>
                    </div>
                  </li>                                    
                </ul>
            </li> 

          </ul>

        </div>


        <!-- Logo section -->
        <div class="col-md-3 pull-right text-right">
          <!-- Logo. -->
          <div class="logo">
            <h1><a href="#"><span id="currentDate" class="bold">Admin</span></a></h1>
            <p class="meta" id="currentDateDay">something goes in meta area</p>
          </div>
          <!-- Logo ends -->
        </div>

        <!-- Counts section -->

        <!-- <div class="col-md-5 pull-right">
          <div class="header-data"> -->

            <!-- Members data -->
            <!-- <div class="hdata">
              <div class="mcol-left"> -->
                <!-- Icon with blue background -->
                <!-- <i class="icon-user bblue"></i> 
              </div>
              <div class="mcol-right"> -->
                <!-- Number of visitors -->
                <!-- <p><a href="#"></a> <em>Users</em></p>
              </div>
              <div class="clearfix"></div>
            </div> -->

            <!-- Traffic data -->
            <!-- <div class="hdata">
              <div class="mcol-left"> -->
                <!-- Icon with red background -->
                <!-- <i class="icon-suitcase byellow"></i> 
              </div>
              <div class="mcol-right"> -->
                <!-- Number of visitors -->
                <!-- <p><a href="#"></a> <em>Partners</em></p>
              </div>
              <div class="clearfix"></div>
            </div> -->

            <!-- Traffic data -->
            <!-- <div class="hdata">
              <div class="mcol-left"> -->
                <!-- Icon with red background -->
                <!-- <i class="icon-male borange"></i> 
              </div>
              <div class="mcol-right"> -->
                <!-- Number of visitors -->
                <!-- <p><a href="#"></a> <em>Volunteers</em></p>
              </div>
              <div class="clearfix"></div>
            </div> -->

            <!-- revenue data -->
            <!-- <div class="hdata">
              <div class="mcol-left"> -->
                <!-- Icon with green background -->
                <!-- <i class="icon-money bgreen"></i> 
              </div>
              <div class="mcol-right"> -->
                <!-- Number of visitors -->
                <!-- <p><a href="#"></a><em>Donations</em></p>
              </div>
              <div class="clearfix"></div>
            </div>  --> 

            <!-- Traffic data -->
            <!-- <div class="hdata">
              <div class="mcol-left"> -->
                <!-- Icon with red background -->
                <!-- <i class="icon-envelope bred"></i> 
              </div>
              <div class="mcol-right"> -->
                <!-- Number of visitors -->
                <!-- <p><a href="#"></a> <em>Subscribers</em></p>
              </div>
              <div class="clearfix"></div>
            </div>                      

          </div>
        </div> -->



      </div>
    </div>
  </header>

<!-- Header ends -->

  <!-- Main content starts -->

  <div class="content">

    <!-- Sidebar -->
    <div class="sidebar">
      <div class="sidebar-dropdown"><a href="#">Navigation</a></div>

      <!--- Sidebar navigation -->
      <!-- If the main navigation has sub navigation, then add the class "has_sub" to "li" of main navigation. -->
      <ul id="nav">
        <!-- Main menu with font awesome icon -->
        <li><a href="<?php echo $this->url->get('admin'); ?>" <?php if ($menu['dashboard']) { ?> class="open" <?php } ?>><i class="icon-home"></i> Dashboard</a></li>          
          <?php
           if (in_array('umrole', $userpages)){ 
          ?>
          <li class="has_sub"><a href="#" <?php if ($menu['umrole']) { ?> class="open" <?php } ?>><i class="icon-user-md"></i> Users  <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
            <ul>
              <li><a href="<?php echo $this->url->get('admin/createuser'); ?>">Create User</a></li>
              <li><a href="<?php echo $this->url->get('admin/users'); ?>">All Users <span class="badge"><?php echo $usersCount; ?></span></a></li>
            </ul>
          </li> 
          <?php
          }
           if (in_array('pmrole', $userpages)){ 
          ?>
          <li class="has_sub"><a href="#" <?php if ($menu['pmrole']) { ?> class="open" <?php } ?>><i class="icon-tasks"></i> Programs  <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
            <ul>
              <li class="open"><a class="li-drop" href="<?php echo $this->url->get('admin/programs/0'); ?>">Manage Programs</a></li>
              <li><a class="li-drop" href="<?php echo $this->url->get('admin/suggestedprograms'); ?>">Suggested Programs <span class="badge"><?php echo $suggestedCount; ?></span></a></li>
                 <LI>
                <span><a href="#" <?php if ($menu['pmrole']) { ?> class="open" <?php } ?>> Programs List 
                  <span class="v-down"><i class="icon-chevron-down"></i></span>
                  </a>
                </span>
                <UL>
                    <?php foreach ($programMenu as $pm) { ?>
                    <li><a href="<?php echo $this->url->get('admin/programpages/' . $pm->programID); ?>">&nbsp;&nbsp;&nbsp;&nbsp;
                      - <strong> <?php echo $pm->programName; ?> </strong></a></li>
                    <?php } ?>
                </UL>
            </LI>
            </ul>
          </li> 
          <?php
          }
          if (in_array('postrole', $userpages)){ 
          ?>
          <li class="has_sub"><a href="#" <?php if ($menu['postrole']) { ?> class="open" <?php } ?>><i class="icon-quote-left"></i> News Post  <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
            <ul>
              <li><a href="<?php echo $this->url->get('admin/createpost'); ?>">Create News Post</a></li>
              <li><a href="<?php echo $this->url->get('admin/allnewspost'); ?>">All News Post <span class="badge"><?php echo $allnewspost; ?></span></a></li>
            </ul>
          </li> 

          <?php
        }
           if (in_array('annrole', $userpages)){
          ?>
          <li class="has_sub"><a href="#" <?php if ($menu['annrole']) { ?> class="open" <?php } ?>><i class="icon-bullhorn"></i> Announcements <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
              <ul>
                  <li><a href="<?php echo $this->url->get('admin/createannocements'); ?>">Create Announcement</a></li>
                  <li><a href="<?php echo $this->url->get('admin/announcements'); ?>">All Announcements <span class="badge"><?php echo $annCount; ?></span></a></li>
              </ul>
          </li>

          <?php
          }
          if (in_array('partrole', $userpages)){ 
          ?> 
          <li class="has_sub"><a href="#" <?php if ($menu['partrole']) { ?> class="open" <?php } ?>><i class="icon-suitcase"></i> Partners <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
              <ul>
              <?php echo $partnerSubMenu; ?>
              </ul>
          </li>
          <?php
          }
          if (in_array('townrole', $userpages)){ 
          ?>
          <li class="has_sub"><a href="<?php echo $this->url->get('admin/towns'); ?>" <?php if ($menu['townrole']) { ?> class="open" <?php } ?>><i class="icon-map-marker"></i> Towns <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
            <ul>
               <li><a href="<?php echo $this->url->get('admin/pintown'); ?>">Pin Town</a></li>
              <li><a href="<?php echo $this->url->get('admin/towns'); ?>">All Towns <span class="badge"><?php echo $townscount; ?></span></a></li>
            </ul>
          </li>
          <?php
          }
          if (in_array('conrole', $userpages)){ 
          ?>
          <li><a class="<?php if ($menu['conrole']) { ?>open<?php } ?>" href="<?php echo $this->url->get('admin/inquiries'); ?>"><i class="icon-phone "></i> Contacts <span class="badge"><?php echo $contactscount; ?></span></a></li>
          <?php
          }
          if (in_array('vmrole', $userpages)){ 
          ?>
          <li><a class="<?php if ($menu['vmrole']) { ?>open<?php } ?>" href="<?php echo $this->url->get('admin/volunteers'); ?>"><i class="icon-male"></i> Volunteers  <span class="badge"><?php echo $volunteerscount; ?></span></a></li>
          <?php
          }
          if (in_array('enmrole', $userpages)){ 
          ?>
          <li class="has_sub"><a class="<?php if ($menu['enmrole']) { ?>open<?php } ?>" href="<?php echo $this->url->get('admin/newsletter'); ?>"><i class="icon-envelope"></i> Newsletter <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
            <ul>
                  <li><a href="<?php echo $this->url->get('admin/createnewsletter'); ?>">Create Newsletter</a></li>
                  <li><a href="<?php echo $this->url->get('admin/newsletter'); ?>">View Subscribers <span class="badge"><?php echo $subsCount; ?></span></a></li>
              </ul>
          </li>
          <?php
          }
          if (in_array('pagerole', $userpages)){ 
          ?>
          <li class="has_sub"><a href="<?php echo $this->url->get('admin/pages'); ?>" <?php if ($menu['pagerole']) { ?> class="open" <?php } ?>><i class="icon-bookmark-empty"></i> Pages <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
            <ul>
                  <li><a href="<?php echo $this->url->get('admin/createpage'); ?>">Create Page</a></li>
                  <li><a href="<?php echo $this->url->get('admin/pages'); ?>">All Pages <span class="badge"><?php echo $pagescount; ?></span></a></li>
                  <li><a href="<?php echo $this->url->get('admin/other'); ?>">Other</a></li>
              </ul>
          </li>
          <?php
          }
          if (in_array('donrole', $userpages)){ 
          ?>
          <li><a href="<?php echo $this->url->get('admin/donations'); ?>" <?php if ($menu['donrole']) { ?> class="open" <?php } ?>><i class="icon-money"></i> Donations</a>
          </li>
          <?php
          }
          if (in_array('extrole', $userpages)){ 
          ?>
          <li><a href="#"><i class="icon-list"></i> Extras</a></li>
          <?php
          }
          
          ?>
        </ul>
      </div>

      <!-- Sidebar ends -->

      <!-- Main bar -->
      <div class="mainbar">
        <?php echo $this->getContent(); ?>
      </div>

      <!-- Mainbar ends -->
      <div class="clearfix"></div>

    </div>
    <!-- Content ends -->

    <!-- Footer starts -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <!-- Copyright info -->
            <p class="copy">Copyright &copy; 2014 - Present | <a href="http://www.angbayanko.org">Ang Bayan Ko Foundation</a> </p>
          </div>
        </div>
      </div>
    </footer>   

    <!-- Footer ends -->

    <!-- Scroll to top -->
    <span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 
    <script>
    var BASE_URL = "<?php echo $this->url->get(); ?>";

    </script>

    <!-- JS -->
    <?php echo $this->tag->javascriptInclude('js/jquery.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/bootstrap.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery-ui-1.9.2.custom.min.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/fullcalendar.min.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery.rateit.min.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery.prettyPhoto.js'); ?>

    <!-- jquery fileuploader -->
    <?php echo $this->tag->javascriptInclude('js/vendor/jquery.ui.widget.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/load-image.min.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/canvas-to-blob.min.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery.iframe-transport.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery.fileupload.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery.fileupload-process.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery.fileupload-image.js'); ?>   
    <?php echo $this->tag->javascriptInclude('js/jquery.fileupload-audio.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery.fileupload-video.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery.fileupload-validate.js'); ?>

    <!-- jquery Flot -->
    <?php echo $this->tag->javascriptInclude('js/excanvas.min.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery.flot.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery.flot.resize.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery.flot.pie.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery.flot.stack.js'); ?>

    <!-- Other JS-->
    <?php echo $this->tag->javascriptInclude('js/sparklines.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/ckeditor/ckeditor.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/ckeditor/adapters/jquery.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/bootstrap-datetimepicker.min.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/bootstrap-switch.min.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/filter.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/charts.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery.maskedinput.min.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/admin/custom.js'); ?>

    <?php if($menu['townrole'] && !empty($pintown) && $pintown){ ?>
      <?php echo $this->tag->javascriptInclude('https://maps.googleapis.com/maps/api/js?v=3.exp'); ?> 
      <script>
        // In the following example, markers appear when the user clicks on the map.
        // The markers are stored in an array.
        // The user can then click an option to hide, show or delete the markers.
        var map;
        var markers = [];

        function initialize() {
          var haightAshbury = new google.maps.LatLng(14.594461297930993, 120.99428721289064);
          var mapOptions = {
            zoom: 8,
            center: haightAshbury,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          map = new google.maps.Map(document.getElementById('map-canvas'),
              mapOptions);

          // This event listener will call addMarker() when the map is clicked.
          google.maps.event.addListener(map, 'click', function(event) {
            addMarker(event.latLng);
          });

          

          // Adds a marker at the center of the map.
          //addMarker(haightAshbury);
        }

        // Add a marker to the map and push to the array.
        function addMarker(location) {
          var marker = new google.maps.Marker({
            position: location,
            map: map,
            draggable:true,
            animation: google.maps.Animation.DROP
          });
          deleteMarkers();
          markers[0]=marker;

          /*document.getElementById("us2-lat").value = location.k;
          document.getElementById("us2-lon").value = location.D;*/
          document.getElementById("us2-lat").value = marker.getPosition().lat();
          document.getElementById("us2-lon").value = marker.getPosition().lng();

          google.maps.event.addListener(marker, 'dragend', function (event) {
              document.getElementById("us2-lat").value = this.getPosition().lat();
              document.getElementById("us2-lon").value = this.getPosition().lng();
          });
        }

        // Sets the map on all markers in the array.
        function setAllMap(map) {
          for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
          }
        }
        // Removes the markers from the map, but keeps them in the array.
        function clearMarkers() {
          setAllMap(null);
        }
        clearText = function(){
          document.getElementById('theText').value = "";
}
        // Shows any markers currently in the array.
        function showMarkers() {
          setAllMap(map);
          document.getElementById('theText').value = "";
        }

        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
          clearMarkers();
          markers = [];
          document.getElementById("us2-lat").value = "";
          document.getElementById("us2-lon").value = "";
        }

        function addDefaultMarker(){
          addMarker(map.getCenter());
        }

        google.maps.event.addDomListener(window, 'load', initialize);

      </script>
    <?php }elseif($menu['townrole'] && !empty($viewTown) && $viewTown){ ?>
      <?php echo $this->tag->javascriptInclude('https://maps.googleapis.com/maps/api/js?v=3.exp'); ?> 
      <script type="text/javascript">
      function initialize() {
        var myLatlng = new google.maps.LatLng(<?php echo $townMarkers['townLat'].', '.$townMarkers['townLong'] ?>);
        var mapOptions = {
          zoom: 12,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          center: myLatlng
        }
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Hello World!'
        });
      }

      google.maps.event.addDomListener(window, 'load', initialize);
      </script>
      
    <?php }elseif($menu['townrole'] && !empty($viewTownsMap) && $viewTownsMap){ ?>
      <?php echo $this->tag->javascriptInclude('https://maps.googleapis.com/maps/api/js?v=3.exp'); ?>
      <script type="text/javascript">
        var locations = [
          <?php
            if(!empty($townMarkers)){
              foreach ($townMarkers as $key => $value) {
                echo "['<a href=\"/admin/viewtown/".$value['townID']."\"><h2>".$value['townName']."</h2></a>".preg_replace('/[\n\r]/',"<br/>",$value['townInfo']) ." <a href=\"/admin/viewtown/".$value['townID']."\">Read More</a>', ".$value['townLat'].", ".$value['townLong']."],";
              }
            }
          ?>
        ];

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 7,
          center: new google.maps.LatLng(11.71233062544614, 122.83037730078127),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow({
          maxWidth: 350
        });

        var marker, i;

        for (i = 0; i < locations.length; i++) {  
          marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
          });

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent(locations[i][0]);
              infowindow.open(map, marker);
              window[e.type].innerHTML = e.containerPoint.toString() + ', ' + e.latlng.toString();
            }
          })(marker, i));
        }
      </script>
    <?php }elseif($menu['townrole'] && !empty($townPartners) && $townPartners){ ?>
      <?php echo $this->tag->javascriptInclude('js/chosen.jquery.min.js'); ?>
    <?php } ?>
    
  </body>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-71209033-4', 'auto');
  ga('send', 'pageview');

  </script>
   <script type = "text/javascript">
  // from textBoxes.html
  function editcontact(){
  var textcontid = document.getElementById("contid");
  var txtOutput = document.getElementById("txtOutput");
  var name = textcontid.value;
  window.location="/admin/editcontact/"+name;
  } // end sayHi
 </script>

 <script type="text/javascript">
var allSpan = document.getElementsByTagName('SPAN');
for(var i = 0; i < allSpan.length; i++){
    allSpan[i].onclick=function(){
        if(this.parentNode){
            var childList = this.parentNode.getElementsByTagName('UL');
            for(var j = 0; j< childList.length;j++){
                var currentState = childList[j].style.display;
                if(currentState=="none"){
                    childList[j].style.display="block";
                }else{
                    childList[j].style.display="none";
                }
            }
        }
    }
}
</script>

<script type="text/javascript">
$(document).ready(function () {

    (function ($) {

        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));

});
</script>
  </html>