        <!-- Page heading -->
        <div class="page-head">
        <!-- Page heading -->
          <h2 class="pull-left"> 
          <!-- page meta -->
          <span class="page-meta">Users</span>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="index.html"><i class="icon-home"></i> Home</a> 
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="#" class="bread-current">Users</a>
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="#" class="bread-current">Create</a>          
        </div>

        <div class="clearfix"></div>

        </div>
        <!-- Page heading ends -->



        <!-- Matter -->

        <div class="matter">
        <div class="container">

          <div class="row">

            <div class="col-md-12">
              <?php echo $this->getContent(); ?>

              <div class="widget wgreen">
                
                <div class="widget-head">
                  <div class="pull-left">Create User</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="icon-remove"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                    <h6>User Account</h6>
                    <hr />
                    <!-- Form starts.  -->
                     <?php echo $this->tag->form(array('admin/createpartner', 'class' => 'form-horizontal')); ?>
                                
                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('username'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('username'); ?>
                                    <?php echo $form->messages('username'); ?>
                                  </div>
                                </div>
                              
                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('email'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('email'); ?>
                                    <?php echo $form->messages('email'); ?>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('password'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('password'); ?>
                                    <?php echo $form->messages('password'); ?>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('repassword'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('repassword'); ?>
                                    <?php echo $form->messages('repassword'); ?>
                                  </div>
                                </div>

                    <h6>User Profile</h6>
                    <hr />
                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('firstname'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('firstname'); ?>
                                    <?php echo $form->messages('firstname'); ?>
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('lastname'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('lastname'); ?>
                                    <?php echo $form->messages('lastname'); ?>
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('middlename'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('middlename'); ?>
                                    <?php echo $form->messages('middlename'); ?>
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('address'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('address'); ?>
                                    <?php echo $form->messages('address'); ?>
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('company'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('company'); ?>
                                    <?php echo $form->messages('company'); ?>
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('contact'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('contact'); ?>
                                    <?php echo $form->messages('contact'); ?>
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <label class="col-lg-4 control-label"><?php echo $form->label('position'); ?></label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('position'); ?>
                                    <?php echo $form->messages('position'); ?>
                                  </div>
                                </div>  

                                

                    <h6>Partner Profile</h6>
                    <hr />                                   

                                <div class="form-group">
                                <label class="col-lg-4 control-label"><?php echo $form->label('partnerName'); ?></label>
                                <div class="col-lg-8">
                                    <?php echo $form->render('partnerName'); ?>
                                    <?php echo $form->messages('partnerName'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label"><?php echo $form->label('partnerInfo'); ?></label>
                                <div class="col-lg-8">
                                    <?php echo $form->render('partnerInfo'); ?>
                                    <?php echo $form->messages('partnerInfo'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                  <label class="col-lg-4 control-label">Status</label>
                                  <div class="col-lg-8">
                                    <div class="radio">
                                      <label>
                                      <?php echo $this->tag->radioField(array('status', 'value' => 'active', 'checked' => 'checked')); ?>
                                        Active
                                      </label>
                                    </div>
                                    <div class="radio">
                                      <label>
                                        <?php echo $this->tag->radioField(array('status', 'value' => 'deactivate')); ?>
                                        Deactivate
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                
                                    <hr />
                                  <?php echo $form->render('csrf', array('value' => $this->security->getToken())); ?>
                                  <?php echo $form->messages('csrf'); ?>                                    
                                <div class="form-group">
                                  <div class="col-lg-offset-1 col-lg-9">
                                    <?php echo $this->tag->submitButton(array('Save User', 'class' => 'btn btn-primary')); ?>
                                    <button type="button" class="btn btn-default">Reset</button>
                                  </div>
                                </div>



                              </form>
                  </div>
                </div>
                  <div class="widget-foot">
                    <a href="<?php echo $this->url->get('admin/partners'); ?>">Back to partner list</a>
                  </div>
              </div>  

            </div>

          </div>

        </div>
          </div>

        <!-- Matter ends -->