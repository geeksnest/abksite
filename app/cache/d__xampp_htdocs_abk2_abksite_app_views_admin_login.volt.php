<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>ABK CMS - LOGIN</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  <?php echo $this->tag->stylesheetLink('css/bootstrap.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/font-awesome.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/abkadmin/style.css'); ?>
  <?php echo $this->tag->stylesheetLink('css/bootstrap-responsive.css'); ?>
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?php echo $this->url->getBaseUri(); ?>public/img/favicon/favicon.png">
</head>

<body>


<!-- Form area -->
<div class="admin-form">
  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <!-- Widget starts -->
        <div class="logoHead worange">
          <img src="<?php echo $this->url->getBaseUri(); ?>img/inner-logo.png">
        </div>
        <div class="textHead worange">
          <img src="<?php echo $this->url->getBaseUri(); ?>img/abktxtlogolonglogin.png">
        </div>        
            <div class="widget worange">
              <!-- Widget head -->
              <div class="widget-head">
                <i class="icon-lock"></i> Ang Bayan Ko CMS Login 
              </div>

              <div class="widget-content">
                <div class="padd">
                  <?php echo $this->getContent(); ?>
                  <!-- Login form -->
                  <?php echo $this->tag->form(array('admin/login', 'class' => 'form-horizontal', 'onreset' => 'formReset(this); return false;')); ?>
                    <!-- Username -->
                    <div class="form-group">
                      <label class="control-label col-lg-3" for="inputUsername">Username</label>
                      <div class="col-lg-9">
                        <?php echo $this->tag->textField(array('username', 'size' => '30', 'class' => 'form-control', 'id' => 'inputUsername', 'placeholder' => 'Username')); ?>
                      </div>
                    </div>
                    <!-- Password -->
                    <div class="form-group">
                      <label class="control-label col-lg-3" for="inputPassword">Password</label>
                      <div class="col-lg-9">
                        <?php echo $this->tag->passwordField(array('password', 'size' => '30', 'class' => 'form-control', 'id' => 'inputPassword', 'placeholder' => 'Password')); ?>
                      </div>
                    </div>
                    <!-- Remember me checkbox and sign in button -->
                 
                        <div class="col-lg-9 col-lg-offset-2">
                        <?php echo $this->tag->submitButton(array('Sign in', 'class' => 'btn btn-danger', 'id' => 'btnlogin')); ?>
                        <button type="reset" id="btnreset" class="btn btn-default">Reset</button>
            </div>
                    <br />
                  </form>
          
        </div>
                </div>
              
                <div class="widget-foot">
                  <a href="/admin/forgotpassword">Forgot Password?</a>
                </div>
            </div>  
      </div>
    </div>
  </div> 
</div>    
<!-- JS -->
<?php echo $this->tag->javascriptInclude('js/jquery.js'); ?>
<?php echo $this->tag->javascriptInclude('js/bootstrap.js'); ?>
<script type="text/javascript">
  function formReset(frm) {
  for(var i=0;i<frm.elements.length;i++)
  if(!(frm.elements[i].type && frm.elements[i].type == "submit"))
  frm.elements[i].value = "";
  }
</script>
</body>
</html>