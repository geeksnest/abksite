
        
        
          

            <?php if(empty($page->items)){ ?>
              <div><span>No albums found</span></div>
            <?php } ?>
            

            
            <?php foreach ($page->items as $album) { ?>

              <a href="<?php echo $this->url->get().'partners/showpictures/'.$album['albumID'] ?>">
                <div title="<?php echo $album['albumName'] ?>" class="square bg" style="background-image:url('<?php echo $album['coverFileName'] ?>')">
                 <div class="content">
                      <div class="table">
                          <div class="table-cell">
                              <div class="albumTitle"><?php echo $album['albumName'] ?></div>
                          </div>
                      </div>
                  </div>
              </div>
              </a>

            <?php } ?>
            

            <div class="">
              <!-- Footer goes here -->
              <?php if ($page->total_pages > 1) { ?>
              <ul class="pagination pull-right">
                <?php if ($page->current != 1) { ?>
                <li><?php echo $this->tag->linkTo(array('partners/pictures/' . $partnerID . '?page=' . $page->before, 'Prev')); ?></li>
                <?php } ?>

                <?php foreach (range(1, $page->total_pages) as $index) { ?>
                <?php if ($page->current == $index) { ?>
                <li><?php echo $this->tag->linkTo(array('partners/pictures/' . $partnerID . '?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                <?php } else { ?>
                <li><?php echo $this->tag->linkTo(array('partners/pictures/' . $partnerID . '?page=' . $index, $index)); ?></li>
                <?php } ?>
                <?php } ?>         

                <?php if ($page->current != $page->total_pages) { ?>                 
                <li><?php echo $this->tag->linkTo(array('partners/pictures/' . $partnerID . '?page=' . $page->next, 'Next')); ?></li>
                <?php } ?>
              </ul>
              <div class="clearfix"></div>
              <?php } ?>
            </div>
