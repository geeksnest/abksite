<!-- Modal Prompt-->

<div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Alert!</h4>
            </div>
            <div class="modal-body">
                <p class="modal-message"></p>
                <span class="modal-list-names"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
            </div>
        </div>
    </div>
</div>
<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">
        <!-- page meta -->
        <span class="page-meta">List of Announcements</span>
    </h2>


    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
        <a href="index.html"><i class="icon-home"></i> Home</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="#" class="bread-current">Announcements</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="#" class="bread-current">List</a>
    </div>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->
<div class="matter">
    <div class="container">

        <!-- Table -->

        <div class="row">

            <div class="col-md-12">
                <form name="form1" class="" method="post" action="" id='main-table-form' class="form-horizontal">
                    <?php echo $this->getContent(); ?>
                    <div class="form-group">
                        <label class="col-lg-1 control-label">Search</label>
                        <div class="col-lg-4">
                            <?php echo $this->tag->textField(array('search_text', 'class' => 'form-control')); ?>
                        </div>
                        <div class="col-lg-7">
                            <?php echo $this->tag->submitButton(array('Search', 'class' => 'btn btn-default')); ?>
                            <?php echo $this->tag->submitButton(array('Clear Search', 'class' => 'btn btn-default', 'name' => 'clear_search')); ?>
                            <a href="/admin/createannocements" type="button" class="btn btn-primary pull-right">+ Add New</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="widget">

                        <div class="widget-head">
                            <div class="pull-left">All Announcements</div>
                            <div class="widget-icons pull-right">
                                <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                                <a href="#" class="wclose"><i class="icon-remove"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="widget-content">

                            <?php echo $this->tag->hiddenField(array('csrf', 'value' => $this->security->getToken())); ?>
                            <input type="hidden" class="tbl-action" name="action" value=""/>
                            <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                            <input type="hidden" class="tbl-edit-url" name="editurl" value="editannouncement/"/>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th width="2%"><?php echo $this->tag->checkField(array('select_all[]', 'class' => 'tbl_select_all')); ?></th>
                                    <th width="35%">Title</th>
                                    <th>Date Created</th>
                                    <th>Date Starts</th>
                                    <th>Date Ends</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($page->total_pages == 0) { ?>  
                                    <tr>
                                        <td colspan="7">No announcement found</td>
                                    </tr>
                                <?php } else { ?>
                                    <?php foreach ($page->items as $post) { ?>
                                        <tr>
                                            <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $post->annID; ?>"> </td>
                                            <td class="name"><a href="viewannouncement/<?php echo $post->annID; ?>"><?php echo $post->annTitle; ?></a></td>
                                            <td><?php echo date('F j, Y', $post->annDate); ?></td>
                                            <td><?php echo date('F j, Y', $post->annStart); ?></td>
                                            <td><?php echo date('F j, Y', $post->annEnd); ?></td>
                                            <td>
                                                <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="<?php echo $post->annID; ?>"><i class="icon-pencil"></i> </a>
                                                <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $post->annID; ?>"><i class="icon-remove"></i> </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                                
                                </tbody>
                            </table>
                            <div class="tblbottomcontrol" style="display:none">
                                <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> |
                                <a href="#" class="tbl_unselect_all"> Unselect </a>
                            </div>

                            <div class="widget-foot">



                                    <?php if ($page->total_pages != 0) { ?>   
                                    <ul class="pagination pull-right">

                                        <?php if ($page->current != 1) { ?>
                                            <li><?php echo $this->tag->linkTo(array('admin/announcements?page=' . $page->before, 'Prev')); ?></li>
                                        <?php } ?>

                                        <?php foreach (range(1, $page->total_pages) as $index) { ?>
                                            <?php if ($page->current == $index) { ?>
                                                <li><?php echo $this->tag->linkTo(array('admin/announcements?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                                            <?php } else { ?>
                                                <li><?php echo $this->tag->linkTo(array('admin/announcements?page=' . $index, $index)); ?></li>
                                            <?php } ?>
                                        <?php } ?>

                                        <?php if ($page->current != $page->total_pages) { ?>
                                            <li><?php echo $this->tag->linkTo(array('admin/announcements?page=' . $page->next, 'Next')); ?></li>
                                        <?php } ?>
                                    </ul>

                                    <div class="clearfix"></div>
                                    <?php } ?>

                            </div>

                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>