      <!-- Page heading -->
      <div class="page-head">
        <h2 class="pull-left"><i class="icon-home"></i> Dashboard</h2>

        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="index.html"><i class="icon-home"></i> Home</a> 
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="#" class="bread-current">Dashboard</a>
        </div>

        <div class="clearfix"></div>

      </div>
      <!-- Page heading ends -->
      <!-- Matter -->

      <div class="matter">
        <div class="container">
          <div class="row">
            <div class="col-md-8">
              <div class="widget">
                <div class="widget-head">
                  <div class="pull-left">Session Stats</div>
                  <div class="widget-icons pull-right">
                    <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                    <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                  </div>  
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                    <div id="auth-button"></div>
                    <div style="display:none;" id="view-selector"></div>
                    <div id="timeline"></div>
                  </div>
                </div>
              </div>
            </div>
              <div class="col-md-4">

              <div class="widget">

                <div class="widget-head">
                  <div class="pull-left">Today Status</div>
                  <div class="widget-icons pull-right">
                    <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                    <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                  </div>  
                  <div class="clearfix"></div>
                </div>             

                <div class="widget-content">
                  <div class="padd">
                    <!-- Visitors, pageview, bounce rate, etc., Sparklines plugin used -->
                    <ul class="current-status">
                      <li>
                        <span id="status1"></span> <span class="bold">Users : <span id="usersCount">N/A</span></span>
                      </li>
                      <li>
                        <span id="status2"></span> <span class="bold">% New Sessions : <span id="newSessionsCount">N/A</span></span>
                      </li>
                      <li>
                        <span id="status3"></span> <span class="bold">Sessions : <span id="sessionsCount">N/A</span></span>
                      </li>
                      <li>
                        <span id="status4"></span> <span class="bold">Bounce Rate : <span id="bounceRateCount">N/A</span></span>
                      </li>
                      <li>
                        <span id="status5"></span> <span class="bold">Avg. Session Duration : <span id="AvgSessionDurationCount">N/A</span></span>
                      </li>
                      <li>
                        <span id="status6"></span> <span class="bold">Pageviews : <span id="pageviewsCount">N/A</span></span>
                      </li>   
                      <li>
                        <span id="status7"></span> <span class="bold">Pages / Session : <span id="pagesPerSectionCount">N/A</span></span>
                      </li>                                                                                                            
                    </ul>

                  </div>
                </div>

              </div>

            </div>
          </div>
          </div>
          <div class="container">
          
          <!-- Dashboard Graph starts -->

          <div class="row">
            <div class="col-md-6">
              <div class="widget">
                <div class="widget-head">
                  <div class="pull-left">Quick Announcement Post</div>
                  <div class="widget-icons pull-right">
                    <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                  </div>  
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                      <div class="form quick-post">
                                      <!-- Edit profile form (not working)-->
                                      <form class="form-horizontal" method="post">
                                          <!-- Title -->
                                          <div class="form-group">
                                            <div class="col-lg-3">
                                              <label for="title">Title</label>
                                               <span class="asterisk">*</span>
                                            </div>
                                            <div class="col-lg-9"> 
                                              <input class="form-control" id="title" type="text">
                                            </div>
                                          </div>   
                                          <!-- Content -->
                                          <div class="form-group">
                                            <div class="col-lg-3">
                                              <label for="content">Content</label>
                                               <span class="asterisk">*</span>
                                            </div>
                                            <div class="col-lg-9">
                                              <textarea class="form-control" id="content"></textarea>
                                            </div>
                                          </div>                           
                                          <!-- Cateogry -->
                                          <div class="form-group">
                                            <div class="col-lg-3">
                                              <label for="category">Category</label>
                                               <span class="asterisk">*</span>
                                            </div>
                                            <div class="col-lg-9">                               
                                                <select class="form-control" name="ann_cat">
                                                  <option value="">- Choose Cateogry -</option>
                                                  <option value="1">General</option>
                                                  <option value="2">News</option>
                                                  <option value="3">Media</option>
                                                  <option value="4">Funny</option>
                                                </select>  
                                            </div>
                                          </div>              
                                          <!-- Tags -->
                                          <div class="form-group">
                                            <div class="col-lg-3">
                                              <label for="tags">Tags</label>
                                               <span class="asterisk">*</span>
                                            </div>
                                            <div class="col-lg-9">
                                              <input class="form-control" id="tags" type="text" name="ann_tag">
                                            </div>
                                          </div>
                                          
                                          <!-- Buttons -->
                                          <div class="form-group">
                                             <!-- Buttons -->
                       <div class="col-lg-offset-2 col-lg-9">
                          <?php echo $this->tag->submitButton(array('Publish', 'name' => 'ann_submit', 'class' => 'btn btn-primary')); ?>
                        <button type="submit" class="btn btn-danger">Save Draft</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                       </div>
                                          </div>
                                      </form>
                                    </div>
                  

                  </div>
                  <div class="widget-foot">
                    <!-- Footer goes here -->
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">

              <div class="widget">

                <div class="widget-head">
                  <div class="pull-left">Visitor Status</div>
                  <div class="widget-icons pull-right">
                    <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                  </div>  
                  <div class="clearfix"></div>
                </div>             

                <div class="widget-content">
                  <div class="padd">
                  <div id="piechart"></div>
                    <!-- Visitors, pageview, bounce rate, etc., Sparklines plugin used -->
                    <!-- <ul class="current-status">
                      <li>
                        <span id="status1"></span> <span class="bold">Visits : 2000</span>
                      </li>
                      <li>
                        <span id="status2"></span> <span class="bold">Unique Visitors : 1,345</span>
                      </li>
                      <li>
                        <span id="status3"></span> <span class="bold">Pageviews : <span id="pageviewsCount">N/A</span></span>
                      </li>
                      <li>
                        <span id="status4"></span> <span class="bold">Pages / Visit : 2000</span>
                      </li>
                      <li>
                        <span id="status5"></span> <span class="bold">Avg. Visit Duration : 2000</span>
                      </li>
                      <li>
                        <span id="status6"></span> <span class="bold">Bounce Rate : 2000</span>
                      </li>   
                      <li>
                        <span id="status7"></span> <span class="bold">% New Visits : 2000</span>
                      </li>                                                                                                            
                    </ul> -->

                  </div>
                </div>

              </div>

            </div>
          <!-- Dashboard graph ends -->
        </div>
      </div>
        </div>
        

    <!-- Matter ends -->

<script>
(function(w,d,s,g,js,fjs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(cb){this.q.push(cb)}};
  js=d.createElement(s);fjs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fjs.parentNode.insertBefore(js,fjs);js.onload=function(){g.load('analytics')};
}(window,document,'script'));
</script>

<script>
google.load("visualization", "1", {packages:["corechart"]});
gapi.analytics.ready(function() {

  // Step 3: Authorize the user.

  var CLIENT_ID = '400725802740-2uvmhtb39kj53d4oc10b8e0oshibk4oo.apps.googleusercontent.com';

  var client = gapi.analytics.auth.authorize({
    container: 'auth-button',
    clientid: CLIENT_ID,
  });

  // Step 4: Create the view selector.

  var viewSelector = new gapi.analytics.ViewSelector({
    container: 'view-selector'
  });

  // Step 5: Create the timeline chart.

  var timeline = new gapi.analytics.googleCharts.DataChart({
    reportType: 'ga',
    query: {
      'dimensions': 'ga:date',
      'metrics': 'ga:sessions',
      'start-date': '30daysAgo',
      'end-date': 'yesterday',
    },
    chart: {
      type: 'LINE',
      container: 'timeline',
      options: {
        backgroundColor: 'transparent',
        width: '100%',
      }
    }
  });

  // Step 6: Hook up the components to work together.

  gapi.analytics.auth.on('success', function(response) {
    viewSelector.set().execute();
    function getData(response) {
      var jsonData = $.ajax({
        url: 'https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A113820133&start-date=3750daysAgo&end-date=yesterday&metrics=ga%3Ausers%2Cga%3Apageviews&access_token='+response.access_token,
        dataType: 'json',
        async: false
      }).responseText;
      var obj = $.parseJSON(jsonData);
      console.log(obj.totalsForAllResults['ga:pageviews']);
      // document.getElementById('pageviewsCount').innerHTML = obj.totalsForAllResults['ga:pageviews'];
    }
    google.setOnLoadCallback(drawChart(response.access_token));
    function drawChart(token) {
      var jsonData = function(){
        return $.ajax({
          url: 'https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A113820133&start-date=30daysAgo&end-date=today&metrics=ga%3Ausers&dimensions=ga%3AuserType&access_token='+token,
          dataType: 'json',
          async: false
        }).responseText;
      }
      var jsonData2 = function(){
        return $.ajax({
          url: 'https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A113820133&start-date=9daysAgo&end-date=today&metrics=ga%3Ausers%2Cga%3ApercentNewSessions%2Cga%3Asessions%2Cga%3AbounceRate%2Cga%3AavgSessionDuration%2Cga%3Apageviews%2Cga%3ApageviewsPerSession&dimensions=ga%3Adate&access_token='+token,
          dataType: 'json',
          async: false
        }).responseText;
      }
      var jsonData3 = function(){
        return $.ajax({
          url: 'https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A113820133&start-date=30daysAgo&end-date=today&metrics=ga%3Ausers%2Cga%3ApercentNewSessions%2Cga%3Asessions%2Cga%3AbounceRate%2Cga%3AavgSessionDuration%2Cga%3Apageviews%2Cga%3ApageviewsPerSession&dimensions=ga%3Adate&access_token='+token,
          dataType: 'json',
          async: false
        }).responseText;
      }
      var obj = $.parseJSON(jsonData());
      var obj2 = $.parseJSON(jsonData2());
      var obj3 = $.parseJSON(jsonData3());
      var nv = parseInt(obj.rows[0][1]);
      var rv = parseInt(obj.rows[1][1]);
      var data = google.visualization.arrayToDataTable([
        ['Stats', 'Counts'],
        ['New Visitor', nv],
        ['Returning Visitor', rv]
      ]);

      var usersC = [];
      var newSessionsC = [];
      var sessionsC = [];
      var bounceRateC = [];
      var AvgSessionDurationC = [];
      var pageviewsC = [];
      var pagesPerSectionC = [];
      document.getElementById('usersCount').innerHTML = obj3.totalsForAllResults['ga:users'];
      document.getElementById('newSessionsCount').innerHTML = Number(obj3.totalsForAllResults['ga:percentNewSessions']).toFixed(2);
      document.getElementById('sessionsCount').innerHTML = obj3.totalsForAllResults['ga:sessions'];
      document.getElementById('bounceRateCount').innerHTML = Number(obj3.totalsForAllResults['ga:bounceRate']).toFixed(2);
      document.getElementById('AvgSessionDurationCount').innerHTML = Number(obj3.totalsForAllResults['ga:avgSessionDuration']).toFixed(2);
      document.getElementById('pageviewsCount').innerHTML = obj3.totalsForAllResults['ga:pageviews'];
      document.getElementById('pagesPerSectionCount').innerHTML = Number(obj3.totalsForAllResults['ga:pageviewsPerSession']).toFixed(2);

      for(var i = 0; i < 10; i++) {
        usersC.push(parseInt(obj2.rows[i][1]));
        newSessionsC.push(Number(obj2.rows[i][2]).toFixed(2));
        sessionsC.push(parseInt(obj2.rows[i][3]));
        bounceRateC.push(Number(obj2.rows[i][4]).toFixed(2));
        AvgSessionDurationC.push(Number(obj2.rows[i][5]).toFixed(2));
        pageviewsC.push(parseInt(obj2.rows[i][6]));
        pagesPerSectionC.push(Number(obj2.rows[i][7]).toFixed(2));
      }

      $("#status1").sparkline(usersC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status2").sparkline(newSessionsC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status3").sparkline(sessionsC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status4").sparkline(bounceRateC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status5").sparkline(AvgSessionDurationC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status6").sparkline(pageviewsC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status7").sparkline(pagesPerSectionC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});

      var options = {
        chartArea : { height: '100%' },
        backgroundColor: 'transparent',
        legend : { 'position': 'right', 'alignment' : 'center' }
      };

      var chart = new google.visualization.PieChart(document.getElementById('piechart'));

      chart.draw(data, options);
    }
  });

  viewSelector.on('change', function(ids) {
    var newIds = {
      query: {
        ids: 'ga:113820133', // View IDs eintragen
        metrics: 'ga:sessions',
        dimensions: 'ga:date',
        'start-date': '30daysAgo',
        'end-date': 'yesterday'
      }
    }
    timeline.set(newIds).execute();
  });
});
</script>
<script type="text/javascript">
  
</script>