<!--Modal View-->
            <div id="modalDigitalAssets" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
              <div class="modal-dialog" style="width: 900px;">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Upload picture to <span>Album: <strong id="uploadAlbumLabel"></strong></span></h4>
<!--                     <input type="hidden" id="digital-assets-year" name="digital-assets-year" value="<?php echo $dayear; ?>" />
                    <input type="hidden" id="digital-assets-month" name="digital-assets-month" value="<?php echo $damonth; ?>" /> -->

                    <input type="hidden" id="partner-id" name="partner-id" value="<?php echo $partner->partnerID; ?>" />
                    <input type="hidden" id="partner-album" name="partner-album" value="<?php echo $albumCurrent; ?>" />
                    <input type="hidden" id="album-id" name="album-id" value="<?php echo $albumID; ?>" />
                  </div>
                  <div class="modal-body" style=" height: 400px; overflow:scroll;">
                    <span class="btn btn-success fileinput-button">
                      <i class="glyphicon glyphicon-plus"></i>
                      <span>Add files...</span>
                      <!-- The file input field used as target for the file upload widget -->
                      <input id="digitalAssets2" type="file" name="files[]" multiple>
                    </span>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                      <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <div class="gallery digital-assets-gallery">
                      <span id="selectImg">Please select files to upload</span>
                    </div>
                    <div class="clearfix"></div>
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true" id="closeUpload">Close</button>
                  </div>
                </div>
              </div>
            </div>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" id="uploadPictureForm" name="uploadPictureForm" enctype="multipart/form-data">
        <input type="hidden" name="partnerID" id="partnerID" value="<?php echo $partnerID; ?>">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Upload Picture</h4>
        </div>
        <div class="modal-body">
          <div id="partnerPreloader"></div>
          <div class="">
            <span>Album: <strong id="uploadAlbumLabel"></strong></span>
            <input type="hidden" id="uploadAlbumSelect">
          </div>
          <br />
          <input type="file" name="files[]" id="picturesFile" multiple>
          <div class="progress progress-animated">
            <div id="progressBar" class="progress-bar progress-bar-info" data-percentage="0" style="width: 0%;"></div>
          </div>
        </div>
        <div class="modal-footer">
          <button id="afterUpload" type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
          <button type="submit" class="btn btn-primary" disabled id="startUploadBtn">Start Upload</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div id="editCaptionModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Edit Picture</h4>
      </div>
      <form method="post" name="editPictureCaptionForm" id="editPictureCaptionForm">
        <div class="modal-body">
          <div id="editCaptionBody"></div>
        </div>
        <div class="modal-footer" id="modal-footer" style="display:none">
          <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
          <button type="submit" class="btn btn-primary" id="saveCaption">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div id="deletePictureConf" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Delete Picture</h4>
      </div>
      <div class="modal-body">
        <span id="deletePictureConfMessage"></span>
      </div>
      <div class="modal-footer" id="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <input type="button" id="deletePicture" value="Delete" class="btn btn-danger">
      </div>
    </div>
  </div>
</div>


<div id="createAlbumModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Create Album</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="partnerID" value="<?php echo $partnerID; ?>">
        <div id="albumResult"></div>
        <label>Album Name</label>
        <input type="text" name="albumName" id="albumName" placeholder="Album Name" class="form-control">
      </div>
      <div class="modal-footer" id="modal-footer">
        <span id="createAlbumClose">
          <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
        </span>
        <button type="button" id="createAlbumBtn" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>

<div id="editAlbumModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Edit Album</h4>
      </div>
      <div class="modal-body">
        <div id="editAlbumContent"></div>
      </div>
      <div class="modal-footer" id="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
        <button style="display:none" type="button" id="editAlbumBtn" class="btn btn-primary" data-partner-id="<?php echo $partnerID; ?>">Save</button>
      </div>
    </div>
  </div>
</div>

<div id="deleteAlbumConf" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Delete Album</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="deleteHiddenAlbumID" id="deleteHiddenAlbumID">
        <span id="deleteMessage"></span>
      </div>
      <div class="modal-footer" id="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
        <button type="button" id="deleteAlbumBtn" class="btn btn-danger">Delete</button>
      </div>
    </div>
  </div>
</div>

<div id="movePictureModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Move Picture(s)</h4>
      </div>
      
      <div class="modal-body">
        <label>Move selected pictures(s) to</label>
        <select id="moveAlbumSelect" class="form-control albums">
          <option value="">--Album--</option>
          <?php foreach ($albums as $key => $value) {
            if(!empty($selectedAlbum) && $selectedAlbum == $value->albumID){
              $selected = 'selected';
            }else{
              $selected = '';
            }
            echo '<option '.$selected.' value="'.$value->albumID.'">'.$value->albumName.'</option>';
          } ?>
        </select>
      </div>

      <div class="modal-footer" id="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button type="submit" class="btn btn-primary" id="movePictureBtn">Move</button>
      </div>
      
    </div>
  </div>
</div>

<div id="deleteSelectedAlbumConf" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Delete Album</h4>
      </div>
      <div class="modal-body">
        <span id="deleteMessage">Are you sure you want to delete selected album(s)?</span>
      </div>
      <div class="modal-footer" id="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
        <button type="button" id="deleteSelectedAlbumBtn" class="btn btn-danger">Delete</button>
      </div>
    </div>
  </div>
</div>


<!-- Page heading -->
<div class="page-head">
  <h2 class="pull-left"><i class="icon-suitcase"></i> ABK Partner</h2>

  <!-- Breadcrumb -->
  <div class="bread-crumb pull-right">
    <a href="<?php echo $this->url->get('admin'); ?>"><i class="icon-home"></i> Home</a>
    <!-- Divider -->
    <span class="divider">/</span>
    <a href="<?php echo $this->url->get('admin/partners'); ?>">Partners</a>
    <span class="divider">/</span>
    <span>Pictures</span>
  </div>

  <div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">
  <?php echo $this->tag->form(array('class' => 'form-horizontal', 'id' => 'picturesForm')); ?>
  <input type="hidden" id="pictureHiddenID" name="pictureHiddenID">
  <input type="hidden" id="pictureAlbumHiddenID" name="pictureAlbumHiddenID">
  <div class="container">

    <!-- Table -->

    <h2><?php echo $partner->partnerName; ?></h2>

    <div class="row">
      <div class="col-md-12">

        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">ABK Partner Info</div>
            <div class="widget-icons pull-right">
              <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
              <a href="#" class="wclose"><i class="icon-remove"></i></a>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
            <div class="padd">
              <ul class="nav nav-tabs" role="tablist">
                <li><a href="/admin/partnersinfo/<?php echo $partnerID; ?>/">Program Information</a></li>
                <li><a href="/admin/partnersinfo/<?php echo $partnerID; ?>/events">Current Events</a></li>
                <li class="active"><a href="/admin/partnersPictures/<?php echo $partnerID; ?>">Pictures</a></li>
              </ul>
              <br>
              <?php echo $this->getContent(); ?>


              <?php if ($albumView == true) { ?>
              <div class="form-group">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="control-label col-lg-3">Search</label>
                    <div class="col-lg-9"> 
                      <?php echo $this->tag->textField(array('search_album_text', 'class' => 'form-control')); ?>
                    </div>
                  </div>
                </div>

                <div class="col-lg-6">
                  <?php echo $this->tag->submitButton(array('Search', 'name' => 'searchBtn', 'class' => 'btn btn-default')); ?>
                  <?php echo $this->tag->submitButton(array('Clear Search', 'class' => 'btn btn-default', 'name' => 'clear_search')); ?>
                  <a href="#createAlbumModal" style="margin-right:25px" class="btn btn-primary pull-right" data-toggle="modal">+ Create New Album</a>
                </div>
              </div>

              <hr>

              <table class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th width="10">
                      <span class="uni">
                        <input type="checkbox" name="select_all[]" class="tbl_select_all">
                      </span>
                    </th>
                    <th>Album Name</th>
                    <th>Pictures</th>
                    <th>Date Created</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($page->items as $al) { ?>
                  <tr id="album<?php echo $al['albumID'] ?>">
                    <td>                            
                      <span class="uni">
                        <input type="checkbox" name="tbl_id[]" data-albumName="<?php echo $al['albumName'] ?>" class="tbl_select_row" value="<?php echo $al['albumID'] ?>">
                      </span>
                    </td>
                    <td>
                      <a id="albumLink<?php echo $al['albumID'] ?>" href="/admin/partnersPictures/<?php echo $partnerID; ?>/<?php echo $al['albumID'] ?>"><?php echo $al['albumName'] ?></a>
                    </td>
                    <td><?php echo $al['picCount'] ?></td>
                    <td><?php echo date("m-d-Y", $al['dateCreated']) ?></td>
                    <td>
                      <a data-album-id="<?php echo $al['albumID']?>" data-partner-id="<?php echo $partnerID; ?>" href="#editAlbumModal" class="editAlbumNameLink btn btn-xs btn-default" data-toggle="modal"><i class="icon-pencil"></i></a>
                      <?php
                      $notempty = ($al['picCount'] != 'empty')?0:1;
                      ?>
                      <a data-empty="<?php echo $notempty ?>" data-album-id="<?php echo $al['albumID'] ?>" data-album-name="<?php echo $al['albumName'] ?>" href="#deleteAlbumConf" class="deleteAlbumLink btn btn-xs btn-danger" data-toggle="modal"><i class="icon-trash"></i></a>
                    </td>
                  </tr>
                  <?php } ?>
                  <?php if(empty($page->items)){ ?>
                    <tr><td colspan="5">No album found</td></tr>
                  <?php } ?>
                </tbody>
              </table>

              <div class="tblbottomcontrol" style="display: none;">
                <a href="#deleteSelectedAlbumConf" id="deleteAllSelectedAlbumLink" data-toggle="modal"> Delete all Selected </a> |
                <a href="#" class="tbl_unselect_all"> Unselect </a>
              </div>

            </div>

            <div class="widget-foot">
              <!-- Footer goes here -->
              <?php if ($showBackToList) { ?> <?php echo $this->tag->linkTo(array('admin/partners', 'Back to Partner List', 'class' => 'btn btn-default')); ?> <?php } ?>
              <?php if ($page->total_pages > 1) { ?>   
              <ul class="pagination pull-right">
                <?php if ($page->current != 1) { ?>
                <li><?php echo $this->tag->linkTo(array('admin/partnersPictures/' . $partnerID . '?page=' . $page->before, 'Prev')); ?></li>
                <?php } ?>

                <?php foreach (range(1, $page->total_pages) as $index) { ?>
                <?php if ($page->current == $index) { ?>
                <li><?php echo $this->tag->linkTo(array('admin/partnersPictures/' . $partnerID . '?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                <?php } else { ?>
                <li><?php echo $this->tag->linkTo(array('admin/partnersPictures/' . $partnerID . '?page=' . $index, $index)); ?></li>
                <?php } ?>
                <?php } ?>         

                <?php if ($page->current != $page->total_pages) { ?>                 
                <li><?php echo $this->tag->linkTo(array('admin/partnersPictures/' . $partnerID . '?page=' . $page->next, 'Next')); ?></li>
                <?php } ?>
              </ul>
              <div class="clearfix"></div>
              <?php } ?>
            </div>


          </div>

          <?php } else { ?>
          <div class="form-group">
            <div class="col-lg-6">
              <div class="form-group">
                <label class="control-label col-lg-3">Search</label>
                <div class="col-lg-9"> 
                  <?php echo $this->tag->textField(array('search_text', 'class' => 'form-control')); ?>
                </div>
              </div>
            </div>

            <div class="col-lg-6">
              <?php echo $this->tag->submitButton(array('Search', 'name' => 'searchBtn', 'class' => 'btn btn-default')); ?>
              <?php echo $this->tag->submitButton(array('Clear Search', 'class' => 'btn btn-default', 'name' => 'clear_search')); ?>
              <a id="addPictures" data-album-name="<?php echo $albumCurrent ?>" data-album-id="<?php echo $currentAlbum ?>" href="#modalDigitalAssets" class="btn btn-primary pull-right" data-toggle="modal">+ Add Pictures</a>
            </div>
          </div> 

          <div class="">
            <select id="albumSelect" name="album" class="form-control albums">
              <option value="">--Album--</option>
              <?php foreach ($albums as $key => $value) {
                if(!empty($selectedAlbum) && $selectedAlbum == $value->albumID){
                  $selected = 'selected';
                }else{
                  $selected = '';
                }
                echo '<option '.$selected.' value="/admin/partnersPictures/'.$value->partnerID.'/'.$value->albumID.'">'.$value->albumName.'</option>';
              } ?>
            </select>
            <!--<a href="#createAlbumModal" data-toggle="modal">Create Album</a>-->
          </div>

          <hr>

          <table class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th>
                  <span class="uni">
                    <input type="checkbox" name="select_all[]" class="tbl_select_all">
                  </span>
                </th>
                <th>Picture</th>
                <th>Caption</th>
                <th>Date</th>
                <th>Size</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($page->items as $pic) {
                ?>
                <tr id="picture<?php echo $pic['pictureID'] ?>">
                  <td>                            
                    <span class="uni">
                      <input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $pic['pictureID'] ?>">
                    </span>
                  </td>
                  <td><a class="prettyPhoto[pp_gal]" href="<?php echo $pic['path'] ?>"><img src="<?php echo $pic['thumbPath'] ?>" alt="<?php echo $pic['pictureCaption'] ?>" title="<?php echo $pic['pictureCaption'] ?>"></a></td>
                  <td><p id="caption<?php echo $pic['pictureID'] ?>"><?php echo $pic['pictureCaption'] ?></p></td>
                  <td><?php echo date("m-d-Y", $pic['dateUploaded']) ?></td>
                  <td><?php echo $pic['pictureSize'] ?></td>
                  <td>

                    <a data-picture-id="<?php echo $pic['pictureID']?>" href="#editCaptionModal" class="editCaptionLink btn btn-xs btn-default" data-toggle="modal"><i class="icon-pencil"></i></a>

                    <a data-picture-id="<?php echo $pic['pictureID'] ?>" data-picture-album-id="<?php echo $pic['albumID'] ?>" href="#deletePictureConf" class="deletePictureLink btn btn-xs btn-danger" data-toggle="modal"><i class="icon-trash"></i></a>

                  </td>
                </tr>
                <?php
              } ?>

              <?php if(empty($page->items)){ ?>
                    <tr><td colspan="6">No pictures found</td></tr>
                  <?php } ?>
            </tbody>
          </table>
          <div class="tblbottomcontrol" style="display: none;">
            <a href="#deletePictureConf" id="deleteAllPictureSelectedLink" data-toggle="modal"> Delete all Selected </a> |
            <input type="hidden" name="moveALbumID" id="moveALbumID">
            <a href="#movePictureModal" data-toggle="modal" id="moveLink"> Move </a> |
            <a href="#" class="tbl_unselect_all"> Unselect </a>
          </div>

          <?php } ?>

        </div>
      </div>

      <?php if ($albumView == true) { ?>

      <?php } else { ?>
      <div class="widget-foot">
        <!-- Footer goes here -->
        <?php echo $this->tag->linkTo(array('admin/partnersPictures/' . $partnerID, 'Back to Album List', 'class' => 'btn btn-default')); ?>
        <?php if ($page->total_pages > 1) { ?>   
        <ul class="pagination pull-right">
          <?php if ($page->current != 1) { ?>
          <li><?php echo $this->tag->linkTo(array('admin/partnersPictures/' . $partnerID . '/' . $currentAlbum . '?page=' . $page->before, 'Prev')); ?></li>
          <?php } ?>

          <?php foreach (range(1, $page->total_pages) as $index) { ?>
          <?php if ($page->current == $index) { ?>
          <li><?php echo $this->tag->linkTo(array('admin/partnersPictures/' . $partnerID . '/' . $currentAlbum . '?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
          <?php } else { ?>
          <li><?php echo $this->tag->linkTo(array('admin/partnersPictures/' . $partnerID . '/' . $currentAlbum . '?page=' . $index, $index)); ?></li>
          <?php } ?>
          <?php } ?>         

          <?php if ($page->current != $page->total_pages) { ?>                 
          <li><?php echo $this->tag->linkTo(array('admin/partnersPictures/' . $partnerID . '/' . $currentAlbum . '?page=' . $page->next, 'Next')); ?></li>
          <?php } ?>
        </ul>
        <div class="clearfix"></div>
        <?php } ?>
      </div>
      <?php } ?>

    </div>


  </div>

</div>


</div>



</form>
</div>

<!-- Matter ends