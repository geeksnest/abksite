
<!-- Modal Prompt-->
<div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Alert!</h4>
			</div>
			<div class="modal-body">
				<p class="modal-message"></p>
				<?php if ($viewlist == false) { ?> <strong><?php echo $inquiry->inqSubject; ?></strong><?php } ?>
				<span class="modal-list-names"></span>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
				<a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
			</div>
		</div>
	</div>
</div>
<!-- Page heading -->
<div class="page-head">
	<h2 class="pull-left"><i class="icon-table"></i> Contacts Module</h2>

	<!-- Breadcrumb -->
	<div class="bread-crumb pull-right">
		<a href="<?php echo $this->url->get('admin'); ?>"><i class="icon-home"></i> Home</a> 
		<!-- Divider -->
		<span class="divider">/</span> 
		<span>Contacts Module</span>
	</div>

	<div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">

	<?php if ($viewlist == true) { ?>

	<?php echo $this->tag->form(array('admin/inquiries', 'class' => 'form-horizontal', 'id' => 'main-table-form')); ?>
	<div class="container">

		<!-- Table -->

		<div class="row">

			<div class="form-group">
				<div class="col-lg-6">
					<div class="form-group">
						<label class="control-label col-lg-3">Search</label>
						<div class="col-lg-9"> 
							<?php echo $this->tag->textField(array('search_text', 'class' => 'form-control')); ?>
						</div>
					</div>
				</div>

				<div class="col-lg-6">
					<?php echo $this->tag->submitButton(array('Search', 'class' => 'btn btn-default')); ?>
					<?php echo $this->tag->submitButton(array('Clear Search', 'class' => 'btn btn-default', 'name' => 'clear_search')); ?>
				</div>
			</div> 

			<div class="col-md-12">
				<?php echo $this->getContent(); ?>


				<div class="widget">

					<div class="widget-head">
						<div class="pull-left">Inquiries</div>
						<div class="widget-icons pull-right">
							<a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
							<a href="#" class="wclose"><i class="icon-remove"></i></a>
						</div>  
						<div class="clearfix"></div>
					</div>


					<div class="widget-content">
						<?php echo $this->tag->hiddenField(array('csrf', 'value' => $this->security->getToken())); ?>
						<input type="hidden" class="tbl-action" name="action" value=""/>
						<input type="hidden" class="tbl-recordID" name="recordID" value=""/>
						<input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
						<table class="table table-striped table-bordered table-hover tblusers">
							<thead>
								<tr>
									<th><?php echo $this->tag->checkField(array('select_all[]', 'class' => 'tbl_select_all')); ?></th>
									<th>Subject</th>
									<th>Sender Name</th>
									<th>Email</th>
									<th>Date</th>
									<th>Control</th>
								</tr>
							</thead>
							<tbody>

								<?php if ($page->total_pages == 0) { ?>  
								<tr>
									<td colspan="6">No inquiries found</td>
								</tr>
								<?php } else { ?>
								<?php foreach ($page->items as $post) { ?>
								<tr>
									<td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $post->inqID; ?>"> </td>
									<td class="name">
										<?php if ($post->inqStatus == 0) { ?>
										<span class="label label-success">New</span>
										<?php } ?>
										<?php echo $this->tag->linkTo(array('admin/inquiries/' . $post->inqID, $post->inqSubject)); ?>

									</td>
									<td><?php echo $post->inqSender; ?></td>
									<td><?php echo $post->inqEmail; ?></td>
									<td><?php echo date('F j, Y', $post->inqDate); ?></td>
									<td>
										<a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $post->inqID; ?>"><i class="icon-remove"></i> </a>
									</td>
								</tr>
								<?php } ?>
								<?php } ?>
							</tbody>
						</table>

						<div class="tblbottomcontrol" style="display:none">
							<a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
							<a href="#" class="tbl_unselect_all"> Unselect </a>
						</div>

						<div class="widget-foot">

							<?php if ($page->total_pages > 1) { ?>

							<ul class="pagination pull-right">

								<?php if ($page->current != 1) { ?>
								<li><?php echo $this->tag->linkTo(array('admin/inquiries/pages?page=' . $page->before, 'Prev')); ?></li>
								<?php } ?>

								<?php foreach (range(1, $page->total_pages) as $index) { ?>
								<?php if ($page->current == $index) { ?>
								<li><?php echo $this->tag->linkTo(array('admin/inquiries/pages?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
								<?php } else { ?>
								<li><?php echo $this->tag->linkTo(array('admin/inquiries/pages?page=' . $index, $index)); ?></li>
								<?php } ?>
								<?php } ?>         

								<?php if ($page->current != $page->total_pages) { ?>                 
								<li><?php echo $this->tag->linkTo(array('admin/inquiries/pages?page=' . $page->next, 'Next')); ?></li>
								<?php } ?>
							</ul>
							<?php } ?>

							<div class="clearfix"></div> 

						</div>

					</div>

				</div>


			</div>

		</div>


	</div>
</form>
<?php } else { ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php echo $this->getContent(); ?>
			<div class="widget">
				<?php echo $this->tag->form(array('admin/inquiries', 'class' => 'form-horizontal', 'id' => 'main-table-form')); ?>
				<div class="widget-head">
					<div class="pull-left">Inquiry</div>
					<div class="widget-icons pull-right">
						<a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
						<a href="#" class="wclose"><i class="icon-remove"></i></a>
					</div>  
					<div class="clearfix"></div>
				</div>

				<div class="widget-content">
					<div class="padd">
						<?php if (($inquiry == false)) { ?>
						<div class="alert alert-danger">Inquiry not found</div>
						<?php } else { ?>
						<span class="pull-right recent-meta"><?php echo date('F j, Y', $inquiry->inqDate); ?></span>
						<h3 class="name"><?php echo $inquiry->inqSubject; ?></h3>
						<span class="recent-meta">From: <?php echo $inquiry->inqSender; ?></span><span class="recent-meta"> < <?php echo $inquiry->inqEmail; ?> ></span>
						<div class="clearfix"></div>
						<br />
						<p>
							<?php echo nl2br($inquiry->inqMessage); ?>
						</p>
						<?php } ?>

						<br />
						<hr>
						<br />
						<?php
						$count = count($inquiryReplies);
						foreach ($inquiryReplies as $key => $value) {
							$count--;
							$hr = $count > 0? '<hr />':'';
							echo '
								<div class="text-right">
									<p>'.date('F j, Y', $value->dateReply).'</p>
									<p>Reply from: '.$value->sender.'</p>
									<br />
									<p>'.nl2br($value->message).'</p>
									<div class="clearfix"></div>
								</div>
							'.$hr;
						}
						?>
						
						<br />
						<?php echo $this->tag->textArea(array('replyMessage', 'name' => 'replyMessage', 'class' => 'form-control', 'rows' => '5', 'placeholder' => 'Send a reply')); ?>

					</div>
				</div>

				<div class="widget-foot">
					
					<?php echo $this->tag->hiddenField(array('csrf', 'value' => $this->security->getToken())); ?>
					<input type="hidden" class="tbl-action" name="action" value=""/>
					<input type="hidden" class="tbl-recordID" name="recordID" value=""/>
					<input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
					<input type="hidden" name="sendToID" value="<?php echo $inquiry->inqID; ?>"/>
					<?php echo $this->tag->linkTo(array('admin/inquiries', 'Back to inquiry list', 'class' => 'btn btn-default')); ?>
					<a href="#modalPrompt" class="btn btn-warning tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $inquiry->inqID; ?>">Delete </a>
					<?php echo $this->tag->submitButton(array('sendReply', 'class' => 'btn btn-primary pull-right', 'value' => 'Send Reply', 'name' => 'sendReply', 'id' => 'sendReply')); ?>
				
				</div>  
				</form>

		</div> 
	</div>
</div>  
</div>
<?php } ?>

</div>

<!-- Matter ends -->