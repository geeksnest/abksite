	    <!-- Modal Prompt-->
      <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title">Alert!</h4>
            </div>
            <div class="modal-body">
              <p class="modal-message"></p>
              <span class="modal-list-names"></span>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
              <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
            </div>
          </div>
        </div>
      </div>
      <!-- Page heading -->
	    <div class="page-head">
        <!-- Page heading -->
	      <h2 class="pull-left"> 
          <!-- page meta -->
          <span class="page-meta">Settings</span>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="/admin"><i class="icon-home"></i> Home</a> 
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="#" class="bread-current">Settings</a>        
        </div>

        <div class="clearfix"></div>

	    </div>
	    <!-- Page heading ends -->
      <!-- Matter -->

        <div class="matter">
          <div class="container">
            <?php echo $this->getContent(); ?>
            <div class="row">

            <div class="col-md-6">              
              <div class="widget">
                <div class="widget-head">
                  <div class="pull-left"><?php echo ($edit ? 'Edit' : 'Create'); ?> Slider</div>
                  <div class="widget-icons pull-right">
                  </div>  
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                    <!-- Form starts.  -->
                      <?php if ($edit) { ?>
                        <form action="<?php echo $this->url->get('admin/settings/' . $eslide->slideID); ?>" method="post" class="form-horizontal">
                      <?php } else { ?>
                        <?php echo $this->tag->form(array('admin/settings', 'class' => 'form-horizontal')); ?>
                      <?php } ?>
                      <div class="form-group" id="imageBanner">
                        <div class="col-lg-3">
                          <label class="">Preview</label>
                        </div>
                        <div class="col-lg-9" id="imagePreview">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-3">
                          <label class=""><?php echo $form->label('imageUrl'); ?></label>
                          <span class="asterisk">*</span>
                        </div>
                        <div class="col-lg-9">
                        <?php if ($edit) { ?>
                          <?php echo $form->render('imageUrl', array( 'value' => $eslide->slideImgUrl )); ?>
                        <?php } else { ?>
                          <?php echo $form->render('imageUrl'); ?>
                        <?php } ?>
                          <?php echo $form->messages('imageUrl'); ?><?php echo $imgUrlError; ?>
                        </div>
                      </div>
                      <div class="form-group" id="embedVideo">
                        <div class="col-lg-3">
                          <label class="">Preview</label>
                        </div>
                        <div class="col-lg-9" id="videoPreview" class="embed-responsive embed-responsive-16by9">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-3">
                        <label class=""><?php echo $form->label('videoUrl'); ?></label>
                        </div>
                        <div class="col-lg-9">
                          <?php if ($edit) { ?>
                            <?php echo $form->render('videoUrl', array( 'value' => $eslide->slideVideoUrl )); ?>
                          <?php } else { ?>
                            <?php echo $form->render('videoUrl'); ?>
                          <?php } ?>
                          <?php echo $form->messages('videoUrl'); ?><?php echo $vidUrlError; ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-lg-3">
                        <label>Video Alignment</label>
                         </div>
                        <div class="col-lg-9">
                          <div class="radio">
                            <label>
                            <?php if ($edit) { ?>
                              <?php if ($eslide->slideVideoAlignment == 'left') { ?>
                                <?php echo $this->tag->radioField(array('alignment', 'value' => 'left', 'checked' => 'checked')); ?>
                              <?php } else { ?>
                                <?php echo $this->tag->radioField(array('alignment', 'value' => 'left')); ?>
                              <?php } ?>
                            <?php } else { ?>
                              <?php echo $this->tag->radioField(array('alignment', 'value' => 'left', 'checked' => 'checked')); ?>
                            <?php } ?>
                              Left
                            </label>
                          </div>
                          <div class="radio">
                            <label>
                              <?php if ($edit) { ?>
                                <?php if ($eslide->slideVideoAlignment == 'center') { ?>
                                  <?php echo $this->tag->radioField(array('alignment', 'value' => 'center', 'checked' => 'checked')); ?>
                                <?php } else { ?>
                                  <?php echo $this->tag->radioField(array('alignment', 'value' => 'center')); ?>
                                <?php } ?>
                              <?php } else { ?>
                                <?php echo $this->tag->radioField(array('alignment', 'value' => 'center', 'checked' => 'checked')); ?>
                              <?php } ?>
                              Center
                            </label>
                          </div>
                          <div class="radio">
                            <label>
                              <?php if ($edit) { ?>
                                <?php if ($eslide->slideVideoAlignment == 'right') { ?>
                                  <?php echo $this->tag->radioField(array('alignment', 'value' => 'right', 'checked' => 'checked')); ?>
                                <?php } else { ?>
                                  <?php echo $this->tag->radioField(array('alignment', 'value' => 'right')); ?>
                                <?php } ?>
                              <?php } else { ?>
                                <?php echo $this->tag->radioField(array('alignment', 'value' => 'right', 'checked' => 'checked')); ?>
                              <?php } ?>
                              Right
                            </label>
                          </div>
                        </div>
                      </div>

                      <hr />
                      <?php echo $form->render('csrf', array('value' => $this->security->getToken())); ?>
                      <?php echo $form->messages('csrf'); ?>                                    
                      <div class="form-group">
                        <div class="col-lg-offset-1 col-lg-9">
                          <?php if ($edit) { ?>
                            <?php echo $this->tag->submitButton(array('Save Changes', 'id' => 'btnSubmit', 'class' => 'btn btn-primary')); ?>
                          <?php } else { ?>
                            <?php echo $this->tag->submitButton(array('Create Slider', 'id' => 'btnSubmit', 'class' => 'btn btn-primary')); ?>
                          <?php } ?>
                          <?php if ($edit) { ?>
                            <a href="/admin/settings" class="btn btn-default">Cancel</a>
                          <?php } else { ?>
                            <button class="btn btn-danger" type="reset" onclick="javascript:removePreview()">Reset</button>
                          <?php } ?>
                        </div>
                      </div>
                     </form>
                  </div>
                </div>
                <div class="widget-foot">
                  <!-- Footer goes here -->
                </div>
              </div>
            </div>

            <div class="col-md-6">              
              <div class="widget">
                <div class="widget-head">
                  <div class="pull-left">Manage Sliders</div>
                  <div class="widget-icons pull-right">
                  </div>  
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                    <?php echo $this->tag->form(array('admin/settings', 'class' => 'form-horizontal', 'id' => 'main-table-form')); ?>
                      <?php echo $form->render('csrf', array('value' => $this->security->getToken())); ?>
                      <?php echo $form->messages('csrf'); ?>
                      <!-- Widget content -->
                      <input type="hidden" class="tbl-action" name="action" value=""/>
                      <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                      <input type="hidden" class="tbl-edit-url" name="editurl" value=""/>
                      <table class="table table-striped table-bordered table-hover">
                        <tbody>
                          <?php if($slides == true){ ?>
                          <?php foreach ($slides as $s) { ?>
                          <tr>
                            <td class="name">
                              <label>Banner URL:</label> 
                                <?php if ($this->length($s->slideImgUrl) > 50) { ?>
                                  <span><?php echo substr($s->slideImgUrl, 0, 49) ?>...</span>
                                <?php } else { ?>
                                  <span><?php echo $s->slideImgUrl; ?></span>
                                <?php } ?>
                              <br/>
                              <label>Video URL:</label> <span><?php echo ($s->slideVideoUrl ? $s->slideVideoUrl : 'None'); ?></span>
                              <?php
                                if( !is_null($s->slideVideoUrl)) {
                                  echo "<br><label>Video Alignment:</label> <span>".ucwords($s->slideVideoAlignment)."</span>";
                                }
                              ?>
                            </td>
                            <td>
                              <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit-slide" data-recorID="<?php echo $s->slideID; ?>"><i class="icon-pencil"></i> </a>
                              <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $s->slideID; ?>"><i class="icon-remove"></i> </a>
                            </td>
                          </tr> 

                          <?php } ?>      
                          <?php } else {
                                  echo "<tr><td>No Slides Yet.</td></tr>";
                                } ?>
                        </tbody></table>
                      </form>
                  </div>
                </div>
                <div class="widget-foot">
                  <!-- Footer goes here -->
                </div>
              </div>
            </div>

          </div> <!-- row -->
        </div> <!-- container -->
      </div> <!-- matter -->
