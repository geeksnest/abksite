
            <?php if(empty($page->items)){ ?>
              <span>No events found</span>
            <?php } ?>
            <?php foreach ($page->items as $ann) { ?>

              <span class="pull-right"><?php echo $ann['eventDate'] ?></span>
              <h4 class="fontNormal"><a href="/partners/viewevent/<?php echo $ann['eventID'] ?>"><?php echo $ann['eventTitle'] ?></a></h4>
              <?php echo $ann['eventDetails'] ?>
              <hr />
            <?php } ?>

            <div class="">
              <!-- Footer goes here -->
              <?php if ($page->total_pages > 1) { ?>   
              <ul class="pagination pull-right">
                <?php if ($page->current != 1) { ?>
                <li><?php echo $this->tag->linkTo(array('partners/currentevents/' . $partnerID . '?page=' . $page->before, 'Prev')); ?></li>
                <?php } ?>

                <?php foreach (range(1, $page->total_pages) as $index) { ?>
                <?php if ($page->current == $index) { ?>
                <li><?php echo $this->tag->linkTo(array('partners/currentevents/' . $partnerID . '?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                <?php } else { ?>
                <li><?php echo $this->tag->linkTo(array('partners/currentevents/' . $partnerID . '?page=' . $index, $index)); ?></li>
                <?php } ?>
                <?php } ?>         

                <?php if ($page->current != $page->total_pages) { ?>                 
                <li><?php echo $this->tag->linkTo(array('partners/currentevents/' . $partnerID . '?page=' . $page->next, 'Next')); ?></li>
                <?php } ?>
              </ul>
              <div class="clearfix"></div>
              <?php } ?>
            </div>