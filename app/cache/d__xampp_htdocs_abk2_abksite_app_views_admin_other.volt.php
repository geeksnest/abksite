        <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>

             <!-- Modal Prompt FOR contact-->
            <div id="modalPromptcont" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title-contact">Edit Warning Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message">Are you sure you want to edit record?</p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" onclick = "editcontact()" class="btn btn-primary modal-btn-contact" data-form='userform'>Yes</a>
                  </div>
                    <input type = "hidden" id = "txtOutput" />

                </div>
              </div>
            </div>
          <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> Other</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="/admin"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <span>Other</span>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->
 
            <div class="matter">
            <div class="container">

             <!-- Table -->
                  
             <div class="row">

             <div class="col-md-12">           
             <div class="widget"> 


            <div class="widget-head">
            <div class="pull-left">Other</div>
            <div class="clearfix"></div>
            </div>

              <div class="widget-content">
                 <?php echo $this->tag->hiddenField(array('csrf', 'value' => $this->security->getToken())); ?>
                        <input type="hidden" class="tbl-action" name="action" value=""/>
                        <input type="hidden" class="tbl-recordID" id="contid" name="recordID" value=""/>
                        <input type="hidden" class="tbl-edit-url" name="editurl" value="editother/"/>
                        <input type="hidden" class="tbl-edit-url-contact" name="editurl" value="editcontact/"/>

                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <th>Title</th>
                    <th>Content</th>
                    <th>Action</th>
                  </thead>

                  <tbody class="searchable">
                    <tr>
                       <?php foreach ($others as $other) { ?>
                      <td width="200"><strong><?php echo $other->title; ?></strong></td>
                      <?php if ($this->length($other->content) > 500) { ?>
                        <td><?php echo substr($other->content, 0, 499) ?>...</td>
                      <?php } else { ?>
                        <td><?php echo $other->content; ?></td>
                      <?php } ?>
                      <td width="40">
                        <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button pull-right" data-toggle="modal" data-action="edit" data-recorID="<?php echo $other->id; ?>"><i class="icon-pencil"></i> </a></td>
                    </tr>
                    <?php } ?>
                     <tr>
                      <td rowspan="2"><strong>Contact</strong></td>
                       <?php foreach ($contacts as $contact) { ?>
                      <td>           
                        <p>Place: <?php echo $contact->location; ?></p>
                        <p>Address: <?php echo $contact->home; ?> </p>
                        <p>Number: <?php echo $contact->number; ?></p>
                        <p>Email: <?php echo $contact->email; ?></p>
                      </td>
                      <td><a href="#modalPromptcont" class="btn btn-xs btn-warning tbl_edit_row modal-control-button pull-right" data-toggle="modal" data-action="editcontact" data-recorID="<?php echo $contact->id; ?>"><i class="icon-pencil"></i> </a></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>

               
                  <!-- footer table -->
                 <div class="widget-foot">
                  <div class="clearfix"></div> 
                  <!-- end -->
               </div>

              </div>

            </div>
            </div>

            </div>
            <!-- end table -->

            </div>
            </div>
