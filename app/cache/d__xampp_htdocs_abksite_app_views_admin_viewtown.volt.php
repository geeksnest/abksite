          <?php $token = $this->security->getToken() ?>

          <!-- Page heading -->
          <div class="page-head">
            <h2 class="pull-left">Towns</h2>

            <!-- Breadcrumb -->
            <div class="bread-crumb pull-right">
              <a href="index.html"><i class="icon-home"></i> Home</a> 
              <!-- Divider -->
              <span class="divider">/</span>
              <span>Towns</span>
            </div>

            <div class="clearfix"></div>

          </div>
          <!-- Page heading ends -->

          <!-- Matter -->

          <div class="matter">
            <?php echo $this->tag->form(array('admin/viewtown/' . $town->townID, 'class' => 'form-horizontal', 'id' => 'main-table-form')); ?>
            <div class="container">

              <!-- Table -->

              <div class="row">

                

                <div class="col-md-12">
                  
                  <div class="padd"><h2><?php echo $town->townName; ?></h2></div>

                  <?php echo $this->getContent(); ?>

                  <div class="widget">

                    <div class="widget-head">
                      <div class="pull-left">Town info</div>
                      <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                        <a href="#" class="wclose"><i class="icon-remove"></i></a>
                      </div>  
                      <div class="clearfix"></div>
                    </div>

                    <div class="padd">
                    <?php echo $townTab; ?>
                    </div>

                    <div id="map-canvas" style="height:500px"></div>

                    <div class="widget-content">

                      <div class="padd">
                        <span class="pull-right">Map Coordinates - Latitude : <?php echo $town->townLat; ?>, Longitutde : <?php echo $town->townLong; ?></span>
                        
                        <div class="clearfix"></div>
                        <div>
                          <?php echo $town->townInfo; ?>
                        </div>
                      </div>      

                      <div class="widget-foot">
                        <?php echo $this->tag->linkTo(array('admin/towns', 'Back to Town List', 'class' => 'btn btn-default')); ?>
                        <?php echo $this->tag->linkTo(array('admin/pintown/' . $town->townID, 'Edit town', 'class' => 'btn btn-warning')); ?>
                        <div class="clearfix"></div> 
                      </div>

                    </div>

                  </div>


                </div>

              </div>


            </div>
          </form>
        </div>

<!-- Matter ends -->