            <!-- Modal View-->
            <div id="modalView" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">View Record</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> User List</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="index.html"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <a href="#" class="bread-current">Users</a>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->

            <div class="matter">

              <div class="container">

                <!-- Table -->

                <div class="row">

                  <div class="col-md-12">
                    <?php echo $this->tag->form(array('admin/users', 'class' => 'form-horizontal', 'id' => 'main-table-form')); ?>
                    <?php echo $this->getContent(); ?>
                    <div class="form-group">
                      <label class="col-lg-1 control-label">Search</label>
                      <div class="col-lg-4">
                            <?php echo $this->tag->textField(array('search_text', 'class' => 'form-control')); ?>
                        </div>
                      <div class="col-lg-6">
                        <?php echo $this->tag->submitButton(array('Search', 'class' => 'btn btn-default')); ?>
                        <?php echo $this->tag->submitButton(array('Clear Search', 'class' => 'btn btn-default', 'name' => 'clear_search')); ?>
                        <a href="<?php echo $this->url->get('admin/createuser'); ?>" type="button" class="btn btn-primary pull-right">+ Add User</a>
                      </div>

                    </div>   
                    
                    <div class="widget">

                      <div class="widget-head">
                        <div class="pull-left">Users</div>
                        <div class="widget-icons pull-right">
                          <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                          <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>  
                        <div class="clearfix"></div>
                      </div>

                      <?php echo $this->tag->hiddenField(array('csrf', 'value' => $this->security->getToken())); ?>
                      <div class="widget-content">
                        <?php $v36980745311iterated = false; ?><?php $v36980745311iterator = $page->items; $v36980745311incr = 0; $v36980745311loop = new stdClass(); $v36980745311loop->length = count($v36980745311iterator); $v36980745311loop->index = 1; $v36980745311loop->index0 = 1; $v36980745311loop->revindex = $v36980745311loop->length; $v36980745311loop->revindex0 = $v36980745311loop->length - 1; ?><?php foreach ($v36980745311iterator as $user) { ?><?php $v36980745311loop->first = ($v36980745311incr == 0); $v36980745311loop->index = $v36980745311incr + 1; $v36980745311loop->index0 = $v36980745311incr; $v36980745311loop->revindex = $v36980745311loop->length - $v36980745311incr; $v36980745311loop->revindex0 = $v36980745311loop->length - ($v36980745311incr + 1); $v36980745311loop->last = ($v36980745311incr == ($v36980745311loop->length - 1)); ?><?php $v36980745311iterated = true; ?>
                        <?php if ($v36980745311loop->first) { ?>
                        
                        <input type="hidden" class="tbl-action" name="action" value=""/>
                        <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                        <input type="hidden" class="tbl-edit-url" name="editurl" value="edituser/"/>
                        <table class="table table-striped table-bordered table-hover tblusers">
                          <thead>
                            <tr>
                              <th><?php echo $this->tag->checkField(array('select_all[]', 'class' => 'tbl_select_all')); ?></th>
                              <th>UserId</th>
                              <th>UserName</th>
                              <th>Name</th>
                              <th>Date Created</th>
                              <th>Position</th>
                              <th>Status</th>
                              <th>Control</th>
                            </tr>
                          </thead>
                          <?php } ?>                      
                          <tbody>
                            <tr>
                              <td>
                                <input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $user->userID; ?>"> 
                                <!-- <?php echo $this->tag->checkField(array('tbl_id', 'class' => 'tbl_select_row', 'value' => $user->userID)); ?> -->
                              </td>
                              <td><?php echo $user->userID; ?></td>
                              <td class="name"><?php echo $user->userName; ?></td>
                              <td><?php echo $user->userLastname; ?>, <?php echo $user->userFirstname; ?> <?php echo $user->userMiddlename; ?></td>
                              <td><?php echo date('F j, Y', $user->dateCreated); ?></td>
                              <td><?php echo $user->userPosition; ?></td>
                              <td>
                                <?php if ($user->userStatus == 'active') { ?>
                                <span class="label label-success">Active</span>
                                <?php } else { ?>
                                <span class="label label-warning">Inactive</span>
                                <?php } ?>
                              </td>
                              <td>

                                <a  href="#modalView" data-toggle="modal" class="btn btn-xs btn-success modal-record-view" data-href="ajaxUserView/<?php echo $user->userID; ?>" ><i class="icon-ok"></i> </a>
                                <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="<?php echo $user->userID; ?>"><i class="icon-pencil"></i> </a>
                                <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $user->userID; ?>"><i class="icon-remove"></i> </a>
                                
                              </td>
                            </tr>                                                        

                          </tbody>
                          <?php if ($v36980745311loop->last) { ?>                      
                          <tbody>
                            <tr>
                              <td colspan="10" align="right">
                                <div class="btn-group">

                                </div>
                              </td>
                            </tr>
                            <tbody>
                            </table>
                            <div class="tblbottomcontrol" style="display:none">
                              <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
                              <a href="#" class="tbl_unselect_all"> Unselect </a>
                            </div>
                            <?php } ?>
                            <?php $v36980745311incr++; } if (!$v36980745311iterated) { ?>
                            No users are recorded
                            <?php } ?>

                            <div class="widget-foot">

                              <ul class="pagination pull-right">

                                <?php if ($page->current != 1) { ?>
                                <li><?php echo $this->tag->linkTo(array('admin/users?page=' . $page->before, 'Prev')); ?></li>
                                <?php } ?>

                                <?php $v36980745311iterator = range(1, $page->total_pages); $v36980745311incr = 0; $v36980745311loop = new stdClass(); $v36980745311loop->length = count($v36980745311iterator); $v36980745311loop->index = 1; $v36980745311loop->index0 = 1; $v36980745311loop->revindex = $v36980745311loop->length; $v36980745311loop->revindex0 = $v36980745311loop->length - 1; ?><?php foreach ($v36980745311iterator as $index) { ?><?php $v36980745311loop->first = ($v36980745311incr == 0); $v36980745311loop->index = $v36980745311incr + 1; $v36980745311loop->index0 = $v36980745311incr; $v36980745311loop->revindex = $v36980745311loop->length - $v36980745311incr; $v36980745311loop->revindex0 = $v36980745311loop->length - ($v36980745311incr + 1); $v36980745311loop->last = ($v36980745311incr == ($v36980745311loop->length - 1)); ?>
                                <?php if ($page->current == $index) { ?>
                                <li><?php echo $this->tag->linkTo(array('admin/users?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                                <?php } else { ?>
                                <li><?php echo $this->tag->linkTo(array('admin/users?page=' . $index, $index)); ?></li>
                                <?php } ?>
                                <?php $v36980745311incr++; } ?>         

                                <?php if ($page->current != $page->total_pages) { ?>                 
                                <li><?php echo $this->tag->linkTo(array('admin/users?page=' . $page->next, 'Next')); ?></li>
                                <?php } ?>
                              </ul>

                              <div class="clearfix"></div> 

                            </div>

                          </div>

                        </div>
                      </form>

                    </div>

                  </div>


                </div>
                
              </div>

<!-- Matter ends -->