
            <?php if(empty($page->items)){ ?>
              <span>No partners found</span>
            <?php } ?>
            <?php foreach ($page->items as $ann) { ?>
              <h4 class="fontNormal"><a href="/partners/view/<?php echo $ann['partnerID'] ?>"><?php echo $ann['partnerName'] ?></a></h4>
              <?php echo $ann['partnerInfo'] ?>
              <hr />
            <?php } ?>

            <div class="">
              <!-- Footer goes here -->
              <?php if ($page->total_pages > 1) { ?>   
              <ul class="pagination pull-right">
                <?php if ($page->current != 1) { ?>
                <li><?php echo $this->tag->linkTo(array('partners?page=' . $page->before, 'Prev')); ?></li>
                <?php } ?>

                <?php foreach (range(1, $page->total_pages) as $index) { ?>
                <?php if ($page->current == $index) { ?>
                <li><?php echo $this->tag->linkTo(array('partners?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                <?php } else { ?>
                <li><?php echo $this->tag->linkTo(array('partners?page=' . $index, $index)); ?></li>
                <?php } ?>
                <?php } ?>         

                <?php if ($page->current != $page->total_pages) { ?>                 
                <li><?php echo $this->tag->linkTo(array('partners?page=' . $page->next, 'Next')); ?></li>
                <?php } ?>
              </ul>
              <div class="clearfix"></div>
              <?php } ?>
            </div>