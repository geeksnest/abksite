<div class="inner-programs pull-left">
	<div class="tabs inner-tabs">
  		<?php echo $townTab; ?>
	        
    	<?php if ($list) { ?>
	    	<?php if ($page->total_pages == 0) { ?>
            	<p>No albums found</p>
        	<?php } else { ?>
            <?php foreach ($page->items as $post) { ?>
			    <a href="<?php echo $post['albumURL']; ?>">
			    <div title="<?php echo $post['albumName']; ?>" class="square bg" style="background-image:url('<?php echo $post['coverFileName']; ?>')">
				   <div class="content">
				        <div class="table">
				            <div class="table-cell">
				                <div class="albumTitle"><?php echo $post['albumName']; ?></div>
				            </div>
				        </div>
				    </div>
				</div>
				</a>
            <?php } ?>
            <div class="widget-foot">

                <?php if ($page->total_pages > 1) { ?>

                <ul class="pagination pull-right">

                  <?php if ($page->current != 1) { ?>
                  <li><?php echo $this->tag->linkTo(array('towns/gallery/' . $townID . '?page=' . $page->before, 'Prev')); ?></li>
                  <?php } ?>

                  <?php foreach (range(1, $page->total_pages) as $index) { ?>
                  <?php if ($page->current == $index) { ?>
                  <li><?php echo $this->tag->linkTo(array('towns/gallery/' . $townID . '?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                  <?php } else { ?>
                  <li><?php echo $this->tag->linkTo(array('towns/gallery/' . $townID . '?page=' . $index, $index)); ?></li>
                  <?php } ?>
                  <?php } ?>         

                  <?php if ($page->current != $page->total_pages) { ?>                 
                  <li><?php echo $this->tag->linkTo(array('towns/gallery/' . $townID . '?page=' . $page->next, 'Next')); ?></li>
                  <?php } ?>
                </ul>
                <?php } ?>

                <div class="clearfix"></div> 

              </div>

          <?php } ?>
      <?php } else { ?>
      	<?php if ($page->total_pages == 0) { ?>
        	<p>No pictures found</p>
    	<?php } else { ?>
    		<h3><?php echo $albumInfo->albumName; ?></h3>
            <?php foreach ($page->items as $post) { ?>
				<a class="prettyPhoto[pp_gal]" href="<?php echo $this->url->get('img/towns/' . $townID . '/' . $albumInfo->albumName); ?>/<?php echo $post->pictureFilename; ?>" title="<?php echo $post->pictureCaption; ?>"><div title="<?php echo $post->pictureCaption; ?>" class="square bg" style="background-image:url('<?php echo $this->url->get('img/towns/' . $townID . '/' . $albumInfo->albumName); ?>/<?php echo $post->pictureFilename; ?>')"></div></a>
            <?php } ?>
		<?php } ?>	
		<div class="clearfix"></div>
		<div><?php echo $this->tag->linkTo(array('towns/gallery/' . $townID, 'Back to Album List')); ?></div>
		
      <?php } ?>

    </div>
</div>