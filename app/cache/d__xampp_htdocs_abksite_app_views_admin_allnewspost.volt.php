            <!-- Modal View-->
            <div id="modalView" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">View Record</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>
      <!-- Page heading -->
      <div class="page-head">
        <!-- Page heading -->
        <h2 class="pull-left"> 
          <!-- page meta -->
          <span class="page-meta">List of News</span>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="index.html"><i class="icon-home"></i> Home</a> 
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="#" class="bread-current">News Post</a>
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="#" class="bread-current">All List</a>          
        </div>

        <div class="clearfix"></div>

      </div>
      <!-- Page heading ends -->
<div class="matter">
  <div class="container">

    <!-- Table -->

    <div class="row">

      <div class="col-md-12">
        <form name="form1" class="" method="post" action"" id='main-table-form' class="form-horizontal"> 
        <?php echo $this->getContent(); ?>
                                  <div class="form-group">
                                  <label class="col-lg-1 control-label">Search</label>
                                  <div class="col-lg-4">
                                      <?php echo $this->tag->textField(array('search_text', 'class' => 'form-control')); ?>
                                  </div>
                                  <div class="col-lg-7">
                                      <?php echo $this->tag->submitButton(array('Search', 'class' => 'btn btn-default')); ?>
                                      <?php echo $this->tag->submitButton(array('Clear Search', 'class' => 'btn btn-default', 'name' => 'clear_search')); ?>
                                      <a href="/admin/createpost" type="button" class="btn btn-primary pull-right">+ Add Post</a>
                                  </div>
                                </div>    
                                <div class="clearfix"></div>
        <div class="widget">

          <div class="widget-head">
            <div class="pull-left">All Posts</div>
            <div class="widget-icons pull-right">
              <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
              <a href="#" class="wclose"><i class="icon-remove"></i></a>
            </div>  
            <div class="clearfix"></div>
          </div>

          <div class="widget-content">
          
                        <?php echo $this->tag->hiddenField(array('csrf', 'value' => $this->security->getToken())); ?>
                        <input type="hidden" class="tbl-action" name="action" value=""/>
                        <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                        <input type="hidden" class="tbl-edit-url" name="editurl" value="editnews/"/>
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th width="2%"><?php echo $this->tag->checkField(array('select_all[]', 'class' => 'tbl_select_all')); ?></th>
                  <th width="35%">Title</th>
                  <th>Keywords</th>
                  <th>Date Created</th>
                  <th>Date Published</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($page->items as $post) { ?>
                <tr>
                  <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $post->postID; ?>"> </td>
                  <td class="name"><?php echo $post->postTitle; ?></td>
                  <td><?php echo $post->postKeyword; ?></td>
                  <td><?php echo date('F j, Y', $post->postDate); ?></td>
                  <td>
                      <?php if ($post->postStatus == 'publish') { ?>
                        <span class="label label-success">Published</span>
                      <?php } else { ?>
                        <span class="label label-warning">Draft</span>
                      <?php } ?>
                      <br/>
                      <?php echo date('F j, Y', $post->postPublishDate); ?>
                  </td>
                  <td>
                            <!--    <a  href="#modalView" data-toggle="modal" class="btn btn-xs btn-success modal-record-view" data-href="ajaxUserView/<?php echo $post->postID; ?>" ><i class="icon-ok"></i> </a> -->
                                <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="<?php echo $post->postID; ?>"><i class="icon-pencil"></i> </a>
                                <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $post->postID; ?>"><i class="icon-remove"></i> </a>
                  </td>
                </tr>    
              <?php } ?>                                               
              </tbody>
            </table>
                          <div class="tblbottomcontrol" style="display:none">
                            <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
                            <a href="#" class="tbl_unselect_all"> Unselect </a>
                          </div>
            
            <div class="widget-foot">


                          <div class="widget-foot">

                           
                            <ul class="pagination pull-right">

                              <?php if ($page->current != 1) { ?>
                              <li><?php echo $this->tag->linkTo(array('admin/allnewspost?page=' . $page->before, 'Prev')); ?></li>
                              <?php } ?>

                              <?php foreach (range(1, $page->total_pages) as $index) { ?>
                              <?php if ($page->current == $index) { ?>
                              <li><?php echo $this->tag->linkTo(array('admin/allnewspost?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                              <?php } else { ?>
                              <li><?php echo $this->tag->linkTo(array('admin/allnewspost?page=' . $index, $index)); ?></li>
                              <?php } ?>
                              <?php } ?>         

                              <?php if ($page->current != $page->total_pages) { ?>                 
                              <li><?php echo $this->tag->linkTo(array('admin/allnewspost?page=' . $page->next, 'Next')); ?></li>
                              <?php } ?>
                            </ul>
                            
                            <div class="clearfix"></div> 

                          </div>
            </div>

          </div>
        </div>
        
        </form>
      </div>

    </div>
  </div>
</div>