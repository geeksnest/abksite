             <?php 
                  $token = $this->security->getToken();
             ?>

            <div id="delActConf" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="ddModalTitle">Delete Activity</h4>
                  </div>
                  <div class="modal-body">
                    Are you sure you want to <span id="ddActionSpan"></span> activity "<strong id="activityLabelConf"></strong>" ?
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel
                    </button>
                    <button type="button" class="btn btn-danger" id="delActivitybtn">Delete</button>
                  </div>
                </div>
              </div>
            </div>

            <div id="editActConf" class="modal fade modalView" tabindex="-1" role="dialog" aria-hidden="true" >
              <div class="modal-dialog">
                <div class="modal-content">
                  <?php echo $this->tag->form(array('admin/programpages/' . $prog->programID, 'class' => 'form-horizontal')); ?>
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Edit Activity</h4>
                  </div>
                  <div class="modal-body">
                    
                    <?php echo $this->tag->hiddenField(array('heditActivity', 'id' => 'heditActivity')); ?>
                    <textarea name="activitytextArea" id="activitytextArea" class="form-control"></textarea>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel
                    </button>
                    <?php echo $this->tag->submitButton(array('saveEditActivity', 'class' => 'btn btn-primary', 'value' => 'Save', 'name' => 'saveEditActivity', 'id' => 'saveEditActivity')); ?>
                    
                  </div>
                </div>
                </form>
              </div>
            </div>


            <!-- Modal View-->
            <div id="modalView" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
              <div class="modal-dialog" style="width: 800px;">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">View Content</h4>
                  </div>
                  <div class="modal-body" style=" height: 400px; overflow:scroll;">
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
              <?php echo $this->tag->form(array('admin/programpages/' . $prog->programID, 'class' => 'form-horizontal')); ?>
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                    <input type="hidden" name="page_program_delete_id" class="page_program_delete_id" value="" >
                  </div>
                  <div class="modal-body">
                    <p class="modal-message">Are you sure you want to delete this page? All data will be lost and cannot be recovered.</p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <button type="submit" class="btn btn-primary modal-btn-yes" data-form='userform' name="program_page_delete_yes" value="Yes">Yes</button>
                    <input type="hidden" name="csrf"
        value="<?php echo $token ?>"/>
                  </div>
                </div>
                </form>
              </div>
            </div>
            <?php echo $script; ?>
            <!-- Page heading -->
            <div class="page-head">
              <!-- Page heading -->
              <h2 class="pull-left"> 
                <!-- page meta -->
                <span class="page-meta"><?php echo $prog->programName; ?></span>
              </h2>


              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="index.html"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <a href="#" class="bread-current">Programs</a>          
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->



            <!-- Matter -->

            <div class="matter">
              <div class="container">
                <?php echo $this->getContent(); ?>
                <div class="row">
                  <div class="col-md-8">
                    <div class="widget">

                      <div class="widget-head">
                        <div class="pull-left">Pages</div>
                        <div class="widget-icons pull-right">
                          <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                          <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>  
                        <div class="clearfix"></div>
                      </div>
                      <div class="widget-content">
                        <div class="padd">

                          <ul id="myTab" class="nav nav-tabs">
                            <?php $v15880181371iterator = $pages; $v15880181371incr = 0; $v15880181371loop = new stdClass(); $v15880181371loop->length = count($v15880181371iterator); $v15880181371loop->index = 1; $v15880181371loop->index0 = 1; $v15880181371loop->revindex = $v15880181371loop->length; $v15880181371loop->revindex0 = $v15880181371loop->length - 1; ?><?php foreach ($v15880181371iterator as $pa) { ?><?php $v15880181371loop->first = ($v15880181371incr == 0); $v15880181371loop->index = $v15880181371incr + 1; $v15880181371loop->index0 = $v15880181371incr; $v15880181371loop->revindex = $v15880181371loop->length - $v15880181371incr; $v15880181371loop->revindex0 = $v15880181371loop->length - ($v15880181371incr + 1); $v15880181371loop->last = ($v15880181371incr == ($v15880181371loop->length - 1)); ?>
                            <li class="<?php if ($v15880181371loop->first) { ?>active<?php } ?>"><a href="#<?php echo $pa->pageID; ?>" data-toggle="tab"><?php echo $pa->pageTitle; ?></a></li>
                            <?php $v15880181371incr++; } ?>
                          </ul>
                          <div id="myTabContent" class="tab-content">
                          <?php
                           echo !count($pages)  ? 'Add pages using the right side "Add Page" button' : '' ?>
                            <?php $v15880181371iterator = $pages; $v15880181371incr = 0; $v15880181371loop = new stdClass(); $v15880181371loop->length = count($v15880181371iterator); $v15880181371loop->index = 1; $v15880181371loop->index0 = 1; $v15880181371loop->revindex = $v15880181371loop->length; $v15880181371loop->revindex0 = $v15880181371loop->length - 1; ?><?php foreach ($v15880181371iterator as $pa) { ?><?php $v15880181371loop->first = ($v15880181371incr == 0); $v15880181371loop->index = $v15880181371incr + 1; $v15880181371loop->index0 = $v15880181371incr; $v15880181371loop->revindex = $v15880181371loop->length - $v15880181371incr; $v15880181371loop->revindex0 = $v15880181371loop->length - ($v15880181371incr + 1); $v15880181371loop->last = ($v15880181371incr == ($v15880181371loop->length - 1)); ?>

                            <div class="tab-pane fade in <?php if ($v15880181371loop->first) { ?>active<?php } ?> " id="<?php echo $pa->pageID; ?>">
                              <?php echo $this->tag->form(array('admin/programpages/' . $prog->programID, 'class' => 'form-horizontal', 'name' => $pa->pageID)); ?>
                              <input type="hidden" name="pageID" value="<?php echo $pa->pageID; ?>" >
                              <div class="form-group">
                                <label class="control-label">Program Title</label>
                                <div class="col-lg-12">
                                  <?php echo $this->tag->textField(array('program_page_title', 'name' => 'program_page_title', 'value' => $pa->pageTitle, 'placeholder' => 'Enter title', 'class' => 'form-control program_page_title')); ?>
                                  <label>Slug:</label> <span class="program_page_url"><?php echo $pa->pageSlug; ?></span>
                                        <?php echo $this->tag->hiddenField(array('program_page_slug', 'id' => 'program_page_slug', 'value' => $pa->pageSlug)); ?>
                                        <?php echo $this->tag->hiddenField(array('program_pageID', 'id' => 'program_pageID', 'value' => $pa->pageID)); ?>
                                </div>
                              </div>
                              <label>Program Keywords</label>
                              <div class="form-group">
                                <div class="col-lg-12">
                                  <?php echo $this->tag->textField(array('program_page_keyword', 'class' => 'form-control program_page_title', 'placeholder' => 'Enter Keywords separated by comma. Ex. disaster, philippine climate change, philippine emergency', 'value' => $pa->pageKeywords)); ?>
                                  
                                </div>
                              </div>
                              <label>Content</label>
                              <textarea id="programPageText<?php echo $pa->pageID; ?>" name="programPageText" class="programPageText"><?php echo $pa->pageContent; ?></textarea>
                              <em>Last Updated: <?php echo date('F j, Y h:i:s a', $pa->pageLastUpdated); ?></em>
                              <br/>
                              <br/>
                              <?php if ($pa->pageActive == 1) { ?>
                                <?php echo $this->tag->checkField(array('program_page_active', 'value' => '1', 'checked' => 'checked')); ?> <label>Page Active</label>
                              <?php } else { ?>
                                <?php echo $this->tag->checkField(array('program_page_active', 'value' => '1')); ?> <label>Page Active</label>
                              <?php } ?>
                              <br/> <br/>
                              <?php echo $this->tag->submitButton(array('program_page_update', 'class' => 'btn btn-primary', 'value' => 'Update ' . $pa->pageTitle, 'name' => 'program_page_update')); ?>
                              <?php echo $this->tag->submitButton(array('program_page_update', 'class' => 'btn btn-default', 'value' => 'Cancel ', 'name' => 'program_page_cancel')); ?>
                              <a href="#modalView" data-toggle="modal" type="button" class="btn btn-info view-page-sample" data-pageid="<?php echo $pa->pageID; ?>">Info</a>
                              <a href="#modalPrompt" data-toggle="modal" class="btn btn-danger pull-right delete-program-page" data-pageid="<?php echo $pa->pageID; ?>">Delete this page</a>
                              <input type="hidden" name="csrf" value="<?php echo $token ?>"/>
                              </form>

                            </div>
                            <?php $v15880181371incr++; } ?>
                          </div>
                        </div>
                        <div class="widget-foot"></div>
                      </div>

                    </div>  

                    <!--start program activities-->
                    <div class="widget">

                    <div class="widget-head">
                      <div class="pull-left">Program Activities</div>
                      <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                        <a href="#" class="wclose"><i class="icon-remove"></i></a>
                      </div>  
                      <div class="clearfix"></div>
                    </div>

                      <div class="widget-content">

                        <?php echo $this->tag->form(array('admin/programpages/' . $prog->programID, 'id' => 'activityForm', 'class' => 'form-horizontal')); ?>

                          <?php echo $this->tag->hiddenField(array('hdelActivity', 'id' => 'hdelActivity')); ?>
                          <?php echo $this->tag->hiddenField(array('ddhiddenaction', 'id' => 'ddhiddenaction')); ?>

                          
                          <table class="table table-striped table-bordered table-hover">
                            <tbody>
                              <?php if(count($activities) != 0){ ?>
                               <?php $v15880181371iterator = $activities; $v15880181371incr = 0; $v15880181371loop = new stdClass(); $v15880181371loop->length = count($v15880181371iterator); $v15880181371loop->index = 1; $v15880181371loop->index0 = 1; $v15880181371loop->revindex = $v15880181371loop->length; $v15880181371loop->revindex0 = $v15880181371loop->length - 1; ?><?php foreach ($v15880181371iterator as $act) { ?><?php $v15880181371loop->first = ($v15880181371incr == 0); $v15880181371loop->index = $v15880181371incr + 1; $v15880181371loop->index0 = $v15880181371incr; $v15880181371loop->revindex = $v15880181371loop->length - $v15880181371incr; $v15880181371loop->revindex0 = $v15880181371loop->length - ($v15880181371incr + 1); $v15880181371loop->last = ($v15880181371incr == ($v15880181371loop->length - 1)); ?>
                               <tr>
                                <td>
                                  <?php if (($act->status == 1)) { ?><span class="label label-success">Active</span><?php } else { ?><span class="label label-default">Disabled</span><?php } ?>
                                  <?php echo $act->activity; ?>

                                  <span class="pull-right">
                                  <a href="#editActConf" data-activity-id ="<?php echo $act->activityID; ?>" data-activity-title="<?php echo $act->activity; ?>" data-toggle="modal" class="editActLink btn btn-xs btn-warning"><i class="icon-pencil"></i></a>
                                  <?php if (($act->status == 1)) { ?>
                                    <a href="#delActConf" data-activity-id ="<?php echo $act->activityID; ?>" data-activity-title="<?php echo $act->activity; ?>" data-action="disable" data-toggle="modal" class="disableActConfLink btn btn-xs btn-default"><i class="icon-ban-circle"></i> </a>
                                  <?php } else { ?>
                                    <a href="#delActConf" data-activity-id ="<?php echo $act->activityID; ?>" data-activity-title="<?php echo $act->activity; ?>" data-action="activate" data-toggle="modal" class="disableActConfLink btn btn-xs btn-success"><i class="icon-ok"></i> </a>
                                  <?php } ?>

                                  <!-- <a href="#delActConf" data-activity-id ="<?php echo $act->activityID; ?>" data-activity-title="<?php echo $act->activity; ?>" data-action="delete" data-toggle="modal" class="delActConfLink btn btn-xs btn-danger"><i class="icon-remove"></i> </a>
                                  </span> -->
                                </td>
                              </tr>
                               <?php $v15880181371incr++; } ?>
                               <?php }else{ ?>
                               <tr>
                                  <td>
                                    No activity found.
                                  </td>
                                </tr>
                               <?php } ?>

                              
                            </tbody>
                          </table>

                          <div class="padd">
                            <?php echo !empty($activityFormResult)?$activityFormResult:'' ?>
                            <?php echo $this->tag->textField(array('activity', 'name' => 'activity', 'placeholder' => 'New activity', 'class' => 'form-control')); ?>
                          </div>

                          <div class="widget-foot">
                            <input type="submit" name="saveActivity" class="btn btn-primary" value="Save Activity">
                            <input type="hidden" name="actCsrf" value="<?php echo $token ?>"/>
                          </div>
                        </form>
                      </div>
                    </div>
                  <!--end program activities-->

                  </div>

                <!-- Task widget -->
                <div class="col-md-4">
                  <div class="widget">
                    <div class="widget-head">
                      <div class="pull-left">Options</div>
                      <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                        <a href="#" class="wclose"><i class="icon-remove"></i></a>
                      </div>  
                      <div class="clearfix"></div>
                    </div>
                    <div class="widget-content">
                      <div class="padd">

                        <?php echo $this->tag->form(array('admin/programpages/' . $prog->programID, 'class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <div class="col-lg-8">
                            <?php echo $titleError; ?>  
                            <?php echo $this->tag->textField(array('program_page_title', 'id' => 'program_page_title', 'class' => 'form-control', 'placeholder' => 'Page Title')); ?>
                            <?php echo $this->tag->hiddenField(array('program_page_slug', 'id' => 'program_page_slug')); ?>
                            </div>
                            <div class="col-lg-4">
                              <?php echo $this->tag->submitButton(array('program_page_add', 'class' => 'form-control btn btn-primary', 'value' => 'Add Page', 'name' => 'program_page_add')); ?>
                            </div>
                              <input type="hidden" name="csrf" value="<?php echo $token ?>"/>
                        </div>
                        </form>
                        <hr/>
                        <h6>Update Page Sorting</h6>
                        <hr/>

                          <?php echo $this->tag->form(array('admin/programpages/' . $prog->programID, 'class' => 'form-horizontal')); ?>
                            <?php $v15880181371iterator = $pages; $v15880181371incr = 0; $v15880181371loop = new stdClass(); $v15880181371loop->length = count($v15880181371iterator); $v15880181371loop->index = 1; $v15880181371loop->index0 = 1; $v15880181371loop->revindex = $v15880181371loop->length; $v15880181371loop->revindex0 = $v15880181371loop->length - 1; ?><?php foreach ($v15880181371iterator as $ps) { ?><?php $v15880181371loop->first = ($v15880181371incr == 0); $v15880181371loop->index = $v15880181371incr + 1; $v15880181371loop->index0 = $v15880181371incr; $v15880181371loop->revindex = $v15880181371loop->length - $v15880181371incr; $v15880181371loop->revindex0 = $v15880181371loop->length - ($v15880181371incr + 1); $v15880181371loop->last = ($v15880181371incr == ($v15880181371loop->length - 1)); ?>
                              <div class="form-group">
                                <div class="col-lg-3"><?php echo $this->tag->textField(array('sort_field' . $ps->pageID, 'placeholder' => '0', 'class' => 'form-control', 'value' => $ps->pageOrder)); ?></div>
                                <label> <?php echo $ps->pageTitle; ?> </label>
                              </div>
                            <?php $v15880181371incr++; } ?>
                          <?php 
                          if (!count($pages)){
                            echo "No page to sort.";
                          }else{
                          ?>
                          <?php echo $this->tag->submitButton(array('updatesort', 'class' => 'btn btn-primary pull-right', 'name' => 'updatesort', 'value' => 'Update Sort')); ?>
                          <?php }?>
                              <input type="hidden" name="csrf" value="<?php echo $token ?>"/>
                          </form>
                          <div class="clearfix"></div>
                      </div>
                      <div class="widget-foot"></div>
                    </div>
                  </div>
                </div>

                <!-- Task widget -->
                <div class="col-md-4">
                  <div class="widget">
                    <div class="widget-head">
                      <div class="pull-left">Program Banner</div>
                      <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                        <a href="#" class="wclose"><i class="icon-remove"></i></a>
                      </div>  
                      <div class="clearfix"></div>
                    </div>
                    <div class="widget-content">
                      <div class="padd">
                          <?php echo $this->tag->form(array('admin/programpages/' . $prog->programID, 'class' => 'form-horizontal')); ?>
                              <div class="form-group">
                                <div class="col-lg-12">
                                  <?php if ($prog->programBanner) { ?>
                                    <img src="<?php echo $prog->programBanner; ?>" style="width: 100%" />
                                  <?php } ?>
                                    <?php echo $this->tag->textField(array('program_banner_url', 'placeholder' => 'Place banner link here.', 'class' => 'form-control', 'value' => $prog->programBanner)); ?>

                                </div>
                              </div>
                          <?php echo $this->tag->submitButton(array('updatebanner', 'class' => 'btn btn-primary pull-right', 'name' => 'updatebanner', 'value' => 'Update Banner')); ?> <input type="hidden" name="csrf" value="<?php echo $token ?>"/>
                          <div class="clearfix"> </div>
                      </div>
                      <div class="widget-foot"></div>
                    </div>
                  </div>
                </div>

                </div>

                


                <div class="row">
                <div class="col-lg-12">
                    <div class="widget">
                      <div class="widget-head">
                      <div class="pull-left">Digital Assets (<?php echo count($digital_assets).' files'?>)</div>
                        <div class="widget-icons pull-right">
                          <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                          <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>  
                        <div class="clearfix"></div>
                      </div>
                      <div class="widget-content">
                        <div class="padd">
                            <span class="btn btn-success fileinput-button">
                              <i class="glyphicon glyphicon-plus"></i>
                              <span>Add files...</span>
                              <!-- The file input field used as target for the file upload widget -->
                              <input id="digitalAssets" type="file" name="files[]" multiple>
                            </span>
                            <!-- The global progress bar -->
                            <div id="progress" class="progress">
                              <div class="progress-bar progress-bar-success"></div>
                            </div>
                            <div class="gallery digital-assets-gallery">
                              <?php $v15880181371iterator = $digital_assets; $v15880181371incr = 0; $v15880181371loop = new stdClass(); $v15880181371loop->length = count($v15880181371iterator); $v15880181371loop->index = 1; $v15880181371loop->index0 = 1; $v15880181371loop->revindex = $v15880181371loop->length; $v15880181371loop->revindex0 = $v15880181371loop->length - 1; ?><?php foreach ($v15880181371iterator as $img) { ?><?php $v15880181371loop->first = ($v15880181371incr == 0); $v15880181371loop->index = $v15880181371incr + 1; $v15880181371loop->index0 = $v15880181371incr; $v15880181371loop->revindex = $v15880181371loop->length - $v15880181371incr; $v15880181371loop->revindex0 = $v15880181371loop->length - ($v15880181371incr + 1); $v15880181371loop->last = ($v15880181371incr == ($v15880181371loop->length - 1)); ?>
                                <div class="program-digital-assets-library pull-left" style="position: relative">
                                  <a href="<?php echo $this->url->get('img/programs/' . $prog->programID . '/' . $img); ?>" class='prettyPhoto[pp_gal]'>
                                  <img src="<?php echo $this->url->get('img/programs/' . $prog->programID . '/' . $img); ?>" alt=""></a>
                                  <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="<?php echo $this->url->get('img/programs/' . $prog->programID . '/' . $img); ?>">
                                  <button type="button" class="btn btn-xs btn-danger digital-assets-delete" data-filename="<?php echo $img; ?>" data-folder="<?php echo $prog->programID; ?>" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
                                </div>
                              <?php $v15880181371incr++; } ?>
                              <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="widget-foot">
                        </div>
                      </div>
                    </div>  
                    </div>       
                </div>
              </div>
            </div>  


		<!-- Matter ends -->