          <input type="hidden" id="townID" value="<?php echo $townID; ?>">
          <div id="createAlbumModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title" id="townAlbumModalTitle">Create Album</h4>
                </div>
                <div class="modal-body">
                  <div id="albumResult"></div>
                  <label>Album Name</label>
                  <input type="hidden" id="editAlbumID">
                  <input type="hidden" id="origAlbumName">
                  <input type="text" name="albumName" id="albumName" placeholder="Album Name" class="form-control">
                </div>
                <div class="modal-footer" id="modal-footer">
                  <span id="createAlbumClose">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  </span>
                  <button type="button" id="createTownAlbumBtn" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>
          </div>

          <!-- Modal Prompt-->
          <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Alert!</h4>
                </div>
                <div class="modal-body">
                  <p class="modal-message"></p>
                  <span class="modal-list-names"></span>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                  <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                </div>
              </div>
            </div>
          </div>
          <!-- Page heading -->
          <div class="page-head">
            <h2 class="pull-left"><i class="icon-table"></i> Towns</h2>

            <!-- Breadcrumb -->
            <div class="bread-crumb pull-right">
              <a href="index.html"><i class="icon-home"></i> Home</a> 
              <!-- Divider -->
              <span class="divider">/</span> 
              <?php echo $this->tag->linkTo(array('admin/towns', 'Towns')); ?>
              <span class="divider">/</span> 
              <span>Gallery</span>
            </div>

            <div class="clearfix"></div>

          </div>
          <!-- Page heading ends -->

          <!-- Matter -->

          <div class="matter">
            <?php echo $this->tag->form(array('class' => 'form-horizontal', 'id' => 'main-table-form')); ?>
            <div class="container">

              <!-- Table -->

              <div class="row">

                <div class="col-md-12">
                  <?php echo $this->getContent(); ?>

                  <h2><?php echo $town->townName; ?></h2>

                  <div class="widget">

                    <div class="widget-head">
                      <div class="pull-left">Town Info</div>
                      <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                        <a href="#" class="wclose"><i class="icon-remove"></i></a>
                      </div>  
                      <div class="clearfix"></div>
                    </div>


                    <div class="widget-content">
                      <div class="padd">
                        <?php echo $townTab; ?>
                      </div>

                      <?php echo $this->tag->hiddenField(array('csrf', 'value' => $this->security->getToken())); ?>
                      <input type="hidden" class="tbl-action" name="action" value=""/>
                      <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                      <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
                      <table class="table table-striped table-bordered table-hover tblusers">
                        <thead>
                          <tr>
                            <th colspan="5">
                              <div class="col-lg-6">
                                <div class="form-group">
                                  <label class="control-label col-lg-3">Search</label>
                                  <div class="col-lg-9"> 
                                    <?php echo $this->tag->textField(array('search_album_text', 'class' => 'form-control')); ?>
                                  </div>
                                </div>
                              </div>

                              <div class="col-lg-6">
                                <?php echo $this->tag->submitButton(array('Search', 'name' => 'searchBtn', 'class' => 'btn btn-default')); ?>
                                <?php echo $this->tag->submitButton(array('Clear Search', 'class' => 'btn btn-default', 'name' => 'clear_search')); ?>
                                <a id="createTownAlbumLink" href="#createAlbumModal" style="margin-right:25px" class="btn btn-primary pull-right" data-toggle="modal">+ Create New Album</a>
                              </div>
                            </th>
                          </tr>
                          <tr>
                            <th width="10"><?php echo $this->tag->checkField(array('select_all[]', 'class' => 'tbl_select_all')); ?></th>
                            <th>Album Name</th>
                            <th>Pictures</th>
                            <th>Date Created</th>
                            <th>Control</th>
                          </tr>
                        </thead>
                        <tbody>

                          <?php if ($page->total_pages == 0) { ?>  
                          <tr>
                            <td colspan="5">No albums found</td>
                          </tr>
                          <?php } else { ?>
                          <?php foreach ($page->items as $post) { ?>
                          <tr>
                            <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $post['albumID']; ?>"> </td>
                            <td class="name"><?php echo $this->tag->linkTo(array('admin/townAlbum/' . $townID . '/' . $post['albumID'], $post['albumName'])); ?></td>
                            <td><?php echo $post['picCount']; ?></td>
                            <td><?php echo date('F j, Y', $post['dateCreated']); ?></td>
                            <td>
                              <a href="#createAlbumModal" class="btn btn-xs btn-warning editTownAlbumLink" data-toggle="modal" data-album-name="<?php echo $post['albumName']; ?>" data-album-id="<?php echo $post['albumID']; ?>"><i class="icon-pencil"></i> </a>

                              <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $post['albumID']; ?>"><i class="icon-remove"></i> </a>
                            </td>
                          </tr>
                          <?php } ?>
                          <?php } ?>
                        </tbody>
                      </table>

                      <div class="tblbottomcontrol" style="display:none">
                        <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
                        <a href="#" class="tbl_unselect_all"> Unselect </a>
                      </div>

                      <div class="widget-foot">

                        <?php if ($page->total_pages > 1) { ?>

                        <ul class="pagination pull-right">

                          <?php if ($page->current != 1) { ?>
                          <li><?php echo $this->tag->linkTo(array('admin/townsgallery/pages?page=' . $page->before, 'Prev')); ?></li>
                          <?php } ?>

                          <?php foreach (range(1, $page->total_pages) as $index) { ?>
                          <?php if ($page->current == $index) { ?>
                          <li><?php echo $this->tag->linkTo(array('admin/townsgallery/pages?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                          <?php } else { ?>
                          <li><?php echo $this->tag->linkTo(array('admin/townsgallery/pages?page=' . $index, $index)); ?></li>
                          <?php } ?>
                          <?php } ?>         

                          <?php if ($page->current != $page->total_pages) { ?>                 
                          <li><?php echo $this->tag->linkTo(array('admin/townsgallery/pages?page=' . $page->next, 'Next')); ?></li>
                          <?php } ?>
                        </ul>
                        <?php } ?>

                        <div class="clearfix"></div> 

                      </div>

                    </div>

                  </div>


                </div>

              </div>


            </div>
          </form>
        </div>

<!-- Matter ends -->