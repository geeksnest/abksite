
            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> Suggested Programs / Activities</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="<?php echo $this->url->get('admin'); ?>"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <a href="" class="bread-current">Suggested Programs</a>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->

            <div class="matter">


<?php echo $this->tag->form(array('admin/suggestedprograms', 'class' => 'form-horizontal', 'id' => 'main-table-form')); ?>
  <div class="container">


      <?php if($viewMode){ ?>
        <!-- Modal Prompt-->
            <div id="modalPromptview" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message">Are you sure you want to delete this suggested program?</p>
                    
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>

        <div class="col-md-12">
        <?php echo $this->getContent(); ?>
        
        
        <div class="widget">

          <div class="widget-head">
            <div class="pull-left">Program Information</div>
            <div class="widget-icons pull-right">
            </div>  
            <div class="clearfix"></div>
          </div>


              <div class="widget-content">
                <?php echo $this->tag->hiddenField(array('csrf', 'value' => $this->security->getToken())); ?>
              <input type="hidden" class="tbl-action" name="action" value=""/>
              <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
              <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
                <div class="padd">
                  <label>Title</label><br />
                  <span class="name"><?php echo $suggested['progTitle'] ?></span>
                  <br /><br />
                  <label>Description</label><br />
                  <?php echo nl2br($suggested['progDesc']) ?>

                  <hr />
                  <label>Suggested By:</label><br />
                  <strong><?php echo $suggested['suggBy'] ?></strong>
                  <br />
                  Email: <?php echo $suggested['email'] ?> <br />
                  Address: <?php echo $suggested['address'] ?> <br />
                  Phone: <?php echo $suggested['phone'] ?> <br />

                </div>
                <div class="widget-foot">
                  <a href="/admin/suggestedprograms" class="btn btn-default">Back to Suggested Program List</a>
                  
                  <div class="clearfix"></div> 
                </div>

                
              </div>

            </div>

          </div>

        </div>
      <?php }else{ ?>

      <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>
      
         <?php echo $this->getContent(); ?>        
      <div class="form-group">
        <label class="col-lg-1 control-label">Search</label>
        <div class="col-lg-4">
              <?php echo $this->tag->textField(array('search_text', 'class' => 'form-control')); ?>
          </div>
        <div class="col-lg-6">
          <?php echo $this->tag->submitButton(array('Search', 'class' => 'btn btn-default')); ?>
          <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
        </div>

      </div>  
      
      <div class="col-md-12">
        
        
        <div class="widget">

          <div class="widget-head">
            <div class="pull-left">Programs List</div>
            <div class="widget-icons pull-right">
            </div>  
            <div class="clearfix"></div>
          </div>


          <div class="widget-content">
            <?php echo $this->tag->hiddenField(array('csrf', 'value' => $this->security->getToken())); ?>
            <input type="hidden" class="tbl-action" name="action" value=""/>
            <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
            <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>

            

            <table class="table table-striped table-bordered table-hover tblusers">
              <thead>
                <tr>
                  <th width="10"><?php echo $this->tag->checkField(array('select_all[]', 'class' => 'tbl_select_all')); ?></th>
                  <th>Program / Activity</th>
                  <th width="100">Username</th>
                  <th width="210">Name</th>
                  <th width="150">Date</th>
                  <th width="100">Action</th>
                </tr>
              </thead>
              <tbody>

                <?php
                if($page->items > 0){
                  foreach ($page->items as $key => $value) {
                    ?>

                    <tr>
                      <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $value['vActivityID'] ?>"></td>
                        <td class="name">
                          <?php echo $value['program'] ?>
                        </td>
                        <td><?php echo $value['username'] ?></td>
                        <td><?php echo $value['name'] ?></td>
                        <td><?php echo date("F j, Y", $value['dateAdded']) ?></td>
                        <td width="10">
                           <a href="/admin/suggestedprograms/<?php echo $value['vActivityID'] ?>" class="btn btn-xs btn-success modal-control-button"><i class="icon-ok"></i></a>
                           <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $value['vActivityID'] ?>"><i class="icon-remove"></i> </a>
                        </td>
                    </tr>

                <?php
                  }
                }else{
                  echo '
                    <tr>
                        <td colspan="6">No suggested programs found</td>
                    </tr>
                  ';
                }
                ?>
              </tbody>
              </table>

              <div class="tblbottomcontrol" style="display:none">
                <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
                <a href="#" class="tbl_unselect_all"> Unselect </a>
              </div>

                <div class="widget-foot">

                  <?php if ($page->total_pages > 1) { ?>
                 
                  <ul class="pagination pull-right">

                    <?php if ($page->current != 1) { ?>
                    <li><?php echo $this->tag->linkTo(array('admin/suggestedprograms/pages?page=' . $page->before, 'Prev')); ?></li>
                    <?php } ?>

                    <?php foreach (range(1, $page->total_pages) as $index) { ?>
                    <?php if ($page->current == $index) { ?>
                    <li><?php echo $this->tag->linkTo(array('admin/suggestedprograms/pages?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                    <?php } else { ?>
                    <li><?php echo $this->tag->linkTo(array('admin/suggestedprograms/pages?page=' . $index, $index)); ?></li>
                    <?php } ?>
                    <?php } ?>         

                    <?php if ($page->current != $page->total_pages) { ?>                 
                    <li><?php echo $this->tag->linkTo(array('admin/suggestedprograms/pages?page=' . $page->next, 'Next')); ?></li>
                    <?php } ?>
                  </ul>
                  <?php } ?>
                  
                  <div class="clearfix"></div> 
                </div>


              </div>

            </div>

          </div>

        </div>
        <?php } ?>

      </div>
    </form>

</div>

<!-- Matter ends -->