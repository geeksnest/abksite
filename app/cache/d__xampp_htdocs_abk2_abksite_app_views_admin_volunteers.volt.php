
            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <?php if ($viewlist == false) { ?> <strong><?php echo $volunteer->inqSubject; ?></strong><?php } ?>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> Volunteer List</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="<?php echo $this->url->get('admin'); ?>"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <a href="/admin/volunteers" class="bread-current">Volunteers</a>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->

            <div class="matter">

              <?php if ($viewlist == true) { ?>

                <?php echo $this->tag->form(array('admin/volunteers', 'class' => 'form-horizontal', 'id' => 'main-table-form')); ?>
                <div class="container">

                  <!-- Table -->
                  <div class="row">
                    <div class="padd">
                      <div class="col-lg-12">
                        <?php echo $this->getContent(); ?> 
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    
                    <div class="form-group">
                      <div class="col-lg-8">
                        <div class="form-group">
                          <label class="control-label col-lg-3">Search</label>
                          <div class="col-lg-4"> 
                            <?php echo $this->tag->textField(array('search_text', 'class' => 'form-control')); ?>
                          </div>
                          <label class="control-label col-lg-3">Filter</label>
                          <div class="col-lg-4"> 
                            <select name="volFilter" class="form-control"><?php echo $activityOptions; ?></select>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-4">
                        <?php echo $this->tag->submitButton(array('Search', 'class' => 'btn btn-default')); ?>
                        <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                      </div>
                    </div> 

                    <div class="col-md-12">
                      
                      
                      <div class="widget">

                        <div class="widget-head">
                          <div class="pull-left">Volunteers</div>
                          <div class="widget-icons pull-right">
                            <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                            <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                          </div>  
                          <div class="clearfix"></div>
                        </div>


                        <div class="widget-content">
                          <?php echo $this->tag->hiddenField(array('csrf', 'value' => $this->security->getToken())); ?>
                          <input type="hidden" class="tbl-action" name="action" value=""/>
                          <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                          <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>

                          

                          <table class="table table-striped table-bordered table-hover tblusers">
                            <thead>
                              <tr>
                                <th><?php echo $this->tag->checkField(array('select_all[]', 'class' => 'tbl_select_all')); ?></th>
                                <th>Name </th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Date Registered</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
   
                              <?php
                              if($page->items > 0){
                                foreach ($page->items as $key => $value) {
                                  ?>

                                  <tr>
                                    <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $value['volunteerID'] ?>"></td>
                                      <td class="name"><?php echo $value['name'] ?>
                                      </td>
                                      <td><?php echo $value['username'] ?></td>
                                      <td><?php echo $value['email'] ?></td>
                                      <td><?php echo $value['phone'] ?></td>
                                      <td><?php echo date("F j, Y", $value['dateAdded']) ?></td>

                                      <td>
                                          <a href="/admin/volunteers/<?php echo $value['volunteerID'] ?>" class="btn btn-xs btn-success"><i class="icon-ok"></i> </a>

                                          <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $value['volunteerID'] ?>"><i class="icon-remove"></i> </a>
                                      </td>
                                  </tr>

                              <?php
                                }
                              }else{
                                echo '
                                  <tr>
                                      <td colspan="6">No volunteers found</td>
                                  </tr>
                                ';
                              }
                              ?>
                            </tbody>
                            </table>

                            <div class="tblbottomcontrol" style="display:none">
                              <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
                              <a href="#" class="tbl_unselect_all"> Unselect </a>
                            </div>

                              <div class="widget-foot">

                                <?php if ($page->total_pages > 1) { ?>
                               
                                <ul class="pagination pull-right">

                                  <?php if ($page->current != 1) { ?>
                                  <li><?php echo $this->tag->linkTo(array('admin/volunteers?page=' . $page->before, 'Prev')); ?></li>
                                  <?php } ?>

                                  <?php foreach (range(1, $page->total_pages) as $index) { ?>
                                  <?php if ($page->current == $index) { ?>
                                  <li><?php echo $this->tag->linkTo(array('admin/volunteers?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                                  <?php } else { ?>
                                  <li><?php echo $this->tag->linkTo(array('admin/volunteers?page=' . $index, $index)); ?></li>
                                  <?php } ?>
                                  <?php } ?>         

                                  <?php if ($page->current != $page->total_pages) { ?>                 
                                  <li><?php echo $this->tag->linkTo(array('admin/volunteers?page=' . $page->next, 'Next')); ?></li>
                                  <?php } ?>
                                </ul>
                                <?php } ?>
                                
                                <div class="clearfix"></div> 

                              </div>

                            </div>

                          </div>


                        </div>

                      </div>


                    </div>
                  </form>
                <?php } else { ?>
                  <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="widget">

                        <div class="widget-head">
                          <div class="pull-left">Volunteer Profile</div>
                          <div class="widget-icons pull-right">
                            <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                            <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                          </div>  
                          <div class="clearfix"></div>
                        </div>

                        <div class="widget-content">
                          <div class="padd">
                          <!-- <?php // echo $volunteer->fname ."sadfsad" ?> -->

                            <?php if (($volunteer == false)) { ?>
                              <div class="alert alert-danger">Volunteer not found</div>
                            <?php } else { ?>
                              <div>
                                Name: <strong><?php echo $volunteer->title; ?> <?php echo $volunteer->fname; ?> <?php echo $volunteer->mname; ?> <?php echo $volunteer->lname; ?> <?php echo $volunteer->extname; ?></strong>
                              </div>
                              <div>
                                Username: <strong><?php echo $volunteer->username; ?></strong>
                              </div>
                              <div>
                                Address: <strong><?php echo $volunteer->address; ?></strong>
                              </div>
                              <div>
                                Email: <strong><?php echo $volunteer->email; ?></strong>
                              </div>
                              <div>
                                Phone: <strong><?php echo $volunteer->phone; ?></strong>
                              </div>
                              <div>
                                Date registered: <strong><?php echo date("F j, Y", $volunteer->dateAdded) ?></strong>
                              </div>

                              
                              <?php echo $volunteerActivities; ?>
                              <?php echo $volunteerSuggActivities; ?>
                              

                            <?php } ?>
                          </div>
                        </div>

                        <div class="widget-foot">
                          <?php echo $this->tag->form(array('admin/volunteers', 'class' => 'form-horizontal', 'id' => 'main-table-form')); ?>
                            <?php echo $this->tag->hiddenField(array('csrf', 'value' => $this->security->getToken())); ?>
                            <input type="hidden" class="tbl-action" name="action" value=""/>
                            <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                            <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
                            <?php echo $this->tag->linkTo(array('admin/volunteers', 'Back to volunteer list', 'class' => 'btn btn-default')); ?>
                            <!-- <a href="#modalPrompt" class="btn btn-warning tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="">Delete </a> -->
                          </form>
                        </div>  
                      </div> 
                    </div>
                  </div>  
                  </div>
                <?php } ?>

              </div>

<!-- Matter ends -->