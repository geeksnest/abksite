            <!-- Modal View-->
            <div id="modalView" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">View Record</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> Pages</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="index.html"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <span>Pages</span>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->

            <div class="matter">
              <?php echo $this->tag->form(array('admin/pages', 'class' => 'form-horizontal', 'id' => 'main-table-form')); ?>
              <div class="container">

                <!-- Table -->

                <div class="row">

                  <div class="form-group">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="control-label col-lg-3">Search</label>
                        <div class="col-lg-9"> 
                          <?php echo $this->tag->textField(array('search_text', 'class' => 'form-control')); ?>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <?php echo $this->tag->submitButton(array('Search', 'class' => 'btn btn-default')); ?>
                      <?php echo $this->tag->submitButton(array('Clear Search', 'class' => 'btn btn-default', 'name' => 'clear_search')); ?>
                      <a href="<?php echo $this->url->get('admin/createpage'); ?>" type="button" class="btn btn-primary">+ Add New Page</a>
                    </div>
                  </div> 

                  <div class="col-md-12">
                    <?php echo $this->getContent(); ?>
                    
                    
                    <div class="widget">

                      <div class="widget-head">
                        <div class="pull-left">Pages</div>
                        <div class="widget-icons pull-right">
                          <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                          <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>  
                        <div class="clearfix"></div>
                      </div>


                      <div class="widget-content">
                        <?php echo $this->tag->hiddenField(array('csrf', 'value' => $this->security->getToken())); ?>
                        <input type="hidden" class="tbl-action" name="action" value=""/>
                        <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                        <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
                        <table class="table table-striped table-bordered table-hover tblusers">
                          <thead>
                            <tr>
                              <th><?php echo $this->tag->checkField(array('select_all[]', 'class' => 'tbl_select_all')); ?></th>
                              <th>Page Title</th>
                              <th>Page Slug</th>
                              <th>Last Updated</th>
                              <th>Page Status</th>
                              <th>Control</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($page->items as $user) {
                            $statustext = ($user['pageActive']=='1')?'<span class="label label-success">Active</span>':'<span class="label label-warning">Inactive</span>';

                            $deleteLink = !$user['specialPage']?'<a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="'.$user['pageID'].'"><i class="icon-remove"></i> </a>':'';

                            $pageCheck = !$user['specialPage']?'<input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="'.$user['pageID'].'"> ':'<i class="icon-ellipsis-vertical"></i>';

                            $pageTitlelabel = !$user['specialPage']?$user['pageTitle']:'<strong>'.$user['pageTitle'].'</strong>';

                            echo '<tr>
                              <td>
                                '.$pageCheck.'
                              </td>
                              <td class="name">'.$user['stages'].' '.$pageTitlelabel.'</td>
                              <td>'.$user['pageSlug'].'</td>
                              <td>'.date("F j, Y", $user['pageLastUpdated']).'</td>
                              <td>'.$statustext.'</td>
                              <td>
                   
                                <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="'.$user['pageID'].'"><i class="icon-pencil"></i> </a>
                                '.$deleteLink.'
                              </td>
                            </tr>';
                          } 
                          if(empty($page->items)){
                            echo '<tr><td colspan="6">No page found</td></tr>';
                          }
                          ?>
                          </tbody>
                          </table>

                          <div class="tblbottomcontrol" style="display:none">
                            <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
                            <a href="#" class="tbl_unselect_all"> Unselect </a>
                          </div>

                            <div class="widget-foot">

                              <?php if ($page->total_pages > 1) { ?>
                             
                              <ul class="pagination pull-right">

                                <?php if ($page->current != 1) { ?>
                                <li><?php echo $this->tag->linkTo(array('admin/pages?page=' . $page->before, 'Prev')); ?></li>
                                <?php } ?>

                                <?php foreach (range(1, $page->total_pages) as $index) { ?>
                                <?php if ($page->current == $index) { ?>
                                <li><?php echo $this->tag->linkTo(array('admin/pages?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                                <?php } else { ?>
                                <li><?php echo $this->tag->linkTo(array('admin/pages?page=' . $index, $index)); ?></li>
                                <?php } ?>
                                <?php } ?>         

                                <?php if ($page->current != $page->total_pages) { ?>                 
                                <li><?php echo $this->tag->linkTo(array('admin/pages?page=' . $page->next, 'Next')); ?></li>
                                <?php } ?>
                              </ul>
                              <?php } ?>
                              
                              <div class="clearfix"></div> 

                            </div>

                          </div>

                        </div>


                      </div>

                    </div>


                  </div>
                </form>
              </div>

<!-- Matter ends -->