<!-- Page heading -->
<div class="page-head">
    <h2 class="pull-left"><i class="icon-suitcase"></i> ABK Partner</h2>

    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
        <a href="<?php echo $this->url->get('admin'); ?>"><i class="icon-home"></i> Home</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="<?php echo $this->url->get('admin/partners'); ?>">Partners</a>
        <span class="divider">/</span>
        <span>View</span>
    </div>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">
    <?php echo $this->tag->form(array('class' => 'form-horizontal', 'id' => 'main-table-form')); ?>
    <div class="container">

        <!-- Table -->

        <h2><?php echo $partner->partnerName; ?></h2>
        <?php echo $this->getContent(); ?>

        <div class="row">
            <div class="col-md-12">

                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">ABK Partner Info</div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">
                            <?php echo $tab; ?>
                            <?php echo $partner->partnerInfo; ?>
                        </div>
                    </div>

                    <div class="widget-foot">
                        <!-- Footer goes here -->
                        <?php echo $this->tag->linkTo(array('admin/partnersinfo/' . $partnerID . '/edit', 'Edit Information', 'class' => 'btn btn-warning')); ?>
                        <?php if ($showBackToList) { ?> <?php echo $this->tag->linkTo(array('admin/partners', 'Back to Partner List', 'class' => 'btn btn-default')); ?> <?php } ?>
                    </div>
                </div>

                
            </div>

        </div>


    </div>



    </form>
</div>

<!-- Matter ends -->