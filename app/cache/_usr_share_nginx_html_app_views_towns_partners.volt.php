<div class="inner-programs pull-left">
	<div class="tabs inner-tabs">
  		<?php echo $townTab; ?>
  		<div class="tab-content">
	        <div class="tab-pane inner-news active">
	        	
		    	<?php if ($page->total_pages == 0) { ?>  
	            	<p>No events found</p>
	        	<?php } else { ?>

	            <?php foreach ($page->items as $post) { ?>
		              <h4 class="fontNormal">
		              	<?php echo $this->tag->linkTo(array('partners/view/' . $post['partnerID'], $post['partnerName'])); ?>
		              </h4>
		              <!-- <div><?php echo $post['eventVenue']; ?></div> -->
		              <?php echo $post['partnerInfo']; ?>
		              <hr>
	            <?php } ?>

	            <div class="widget-foot">

                    <?php if ($page->total_pages > 1) { ?>

                    <ul class="pagination pull-right">

                      <?php if ($page->current != 1) { ?>
                      <li><?php echo $this->tag->linkTo(array('towns/events/' . $post['townID'] . '?page=' . $page->before, 'Prev')); ?></li>
                      <?php } ?>

                      <?php foreach (range(1, $page->total_pages) as $index) { ?>
                      <?php if ($page->current == $index) { ?>
                      <li><?php echo $this->tag->linkTo(array('towns/events/' . $post['townID'] . '?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                      <?php } else { ?>
                      <li><?php echo $this->tag->linkTo(array('towns/events/' . $post['townID'] . '?page=' . $index, $index)); ?></li>
                      <?php } ?>
                      <?php } ?>         

                      <?php if ($page->current != $page->total_pages) { ?>                 
                      <li><?php echo $this->tag->linkTo(array('towns/events/' . $post['townID'] . '?page=' . $page->next, 'Next')); ?></li>
                      <?php } ?>
                    </ul>
                    <?php } ?>

                    <div class="clearfix"></div> 

                  </div>

	          <?php } ?>

		    </div>
	    </div>
    </div>
</div>