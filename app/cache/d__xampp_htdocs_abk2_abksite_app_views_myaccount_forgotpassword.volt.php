<div class="inner-programs pull-left">
	<div class="tabs inner-tabs">
        <?php echo $this->getContent(); ?>
        <div class="panel panel-default border-flat">
			<div class="panel-heading">Forgot Password</div>
			<div class="panel-body">
				<div id="forgotpassResult"></div>
		  		<form method="post" id="forgotpassForm" action="/myaccount/forgotpassword">
		  			<div class="form-group">
		                <label>Email Address</label>
		                <input name="email" type="email" class="form-control border-flat" placeholder="Enter email address">
		            </div>
		            <button type="submit" class="btn btn-success" id="submitForgotpass">Submit</button>
        			<a type="button" class="btn btn-default" href="/">Cancel</a>
		  		</form>
			</div>
		</div>
	</div>
</div>