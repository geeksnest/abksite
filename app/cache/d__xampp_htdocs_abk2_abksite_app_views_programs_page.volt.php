
      <div class="inner-programs pull-left">
		<?php if ($program->programBanner) { ?>
        <div class="dm-latest-news-head">
            <img src="<?php echo $program->programBanner; ?>">
            <div class="dm-latest-news-label">
              <?php echo $program->programTagline; ?>
            </div>
        </div>
		<?php } ?>
        <div class="tabs inner-tabs">
 <!-- this is for scrollable menu  -->
 <div id="displaymenu">          
<div class="scroller scroller-left"><i class="icon icon-chevron-left"></i></div>
  <div class="scroller scroller-right"><i class="icon icon-chevron-right"></i></div>
<div class="wrapper">
    <ul class="nav nav-tabs list" id="myTab">
       <?php if ($program->programID == 1) { ?>
            <li class="active"><a href="#news" data-toggle="tab">Latest Breaking News</a></li>
            <?php } ?>
       <?php $v8931014201iterator = $prog_pages; $v8931014201incr = 0; $v8931014201loop = new stdClass(); $v8931014201loop->length = count($v8931014201iterator); $v8931014201loop->index = 1; $v8931014201loop->index0 = 1; $v8931014201loop->revindex = $v8931014201loop->length; $v8931014201loop->revindex0 = $v8931014201loop->length - 1; ?><?php foreach ($v8931014201iterator as $page) { ?><?php $v8931014201loop->first = ($v8931014201incr == 0); $v8931014201loop->index = $v8931014201incr + 1; $v8931014201loop->index0 = $v8931014201incr; $v8931014201loop->revindex = $v8931014201loop->length - $v8931014201incr; $v8931014201loop->revindex0 = $v8931014201loop->length - ($v8931014201incr + 1); $v8931014201loop->last = ($v8931014201incr == ($v8931014201loop->length - 1)); ?>
               <?php if ($page->pageActive) { ?>
                    <li class="<?php if ($v8931014201loop->first) { ?> <?php if ($program->programID != 1) { ?>  <?php } ?> <?php } ?>"><a href="#<?php echo $page->pageSlug; ?>" data-toggle="tab"><?php echo $page->pageTitle; ?></a></li>
                <?php } ?>
            <?php $v8931014201incr++; } ?>
  </ul>
  </div>
</div>
<!-- end of scrollable menu -->

<!-- hide menu to and display it for responsive page -->
<div class="hideme">
 <ul class="nav nav-tabs">
            <!-- Use unique name in anchor tag -->
            <?php if ($program->programID == 1) { ?>
            <li class="active"><a href="#news" data-toggle="tab">Latest Breaking News</a></li>
            <?php } ?>

            <?php $v8931014201iterator = $prog_pages; $v8931014201incr = 0; $v8931014201loop = new stdClass(); $v8931014201loop->length = count($v8931014201iterator); $v8931014201loop->index = 1; $v8931014201loop->index0 = 1; $v8931014201loop->revindex = $v8931014201loop->length; $v8931014201loop->revindex0 = $v8931014201loop->length - 1; ?><?php foreach ($v8931014201iterator as $page) { ?><?php $v8931014201loop->first = ($v8931014201incr == 0); $v8931014201loop->index = $v8931014201incr + 1; $v8931014201loop->index0 = $v8931014201incr; $v8931014201loop->revindex = $v8931014201loop->length - $v8931014201incr; $v8931014201loop->revindex0 = $v8931014201loop->length - ($v8931014201incr + 1); $v8931014201loop->last = ($v8931014201incr == ($v8931014201loop->length - 1)); ?>
                <?php if ($page->pageActive) { ?>
                    <li class="<?php if ($v8931014201loop->first) { ?> <?php if ($program->programID != 1) { ?> active <?php } ?> <?php } ?> "><a href="#<?php echo $page->pageSlug; ?>" data-toggle="tab"><?php echo $page->pageTitle; ?></a></li>
                <?php } ?>
            <?php $v8931014201incr++; } ?>
          </ul> 
</div>
<!-- end of hiding menu -->

          <!-- Tab conten -->
          <div class="tab-content">
                    <?php if ($program->programID == 1) { ?>
                    <div class="tab-pane active inner-news" id="news">
                      <?php if ($post->total_items == 0) { ?>
                        No News Added.
                      <?php } else { ?>
                        <?php $v8931014201iterator = $post->items; $v8931014201incr = 0; $v8931014201loop = new stdClass(); $v8931014201loop->length = count($v8931014201iterator); $v8931014201loop->index = 1; $v8931014201loop->index0 = 1; $v8931014201loop->revindex = $v8931014201loop->length; $v8931014201loop->revindex0 = $v8931014201loop->length - 1; ?><?php foreach ($v8931014201iterator as $p) { ?><?php $v8931014201loop->first = ($v8931014201incr == 0); $v8931014201loop->index = $v8931014201incr + 1; $v8931014201loop->index0 = $v8931014201incr; $v8931014201loop->revindex = $v8931014201loop->length - $v8931014201incr; $v8931014201loop->revindex0 = $v8931014201loop->length - ($v8931014201incr + 1); $v8931014201loop->last = ($v8931014201incr == ($v8931014201loop->length - 1)); ?>
                            <?php if ($p->postStatus == 'publish') { ?>
                            <div class="entry">
                                     <h2><a href="<?php echo $this->url->get('post/news/' . $p->postSlug); ?>"><?php echo $p->postTitle; ?></a></h2>
                                     <!-- Meta details -->

                                     <div class="meta">
                                        <div><i class="icon-calendar"></i> <?php echo date('d-m-Y', $p->postPublishDate); ?></div>
                                        <div  class="share_buttons">
                                        <div class="fb-like" data-href="http://abk.gotitgenius.com/<?php echo $this->url->get('post/news/' . $p->postSlug); ?>" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div> 
                                        <div class="fb-share-button" data-href="<?php echo $this->url->get('post/news/' . $p->postSlug); ?>" data-type="button"></div> 
                                        &nbsp;
                                        <a href="https://twitter.com/share" class="twitter-share-button" data-count="none"></a>
                                        <!-- <span class="pull-right"><i class="icon-comment"></i> <a href="#">2 Comments</a></span> -->
                                     </div>
                                   </div>
                                     
                                     <!-- Thumbnail -->
                                     <?php if ($p->postFeatureImage) { ?>
                                     <div class="bthumb3">
                                        <a href="#"><img src="<?php echo $p->postFeatureImage; ?>" alt="" class="img-responsive"></a>
                                     </div>
                                     <?php } ?>
                                     <!-- Para -->
                                     <p class="post-content"><?php echo $out = strlen(strip_tags($p->postContent)) > 550 ? substr(strip_tags($p->postContent),0,550)."..." : $p->postContent; ?></p>

                                     <!-- Read more -->
                                     <div class="button readmore"><a href="<?php echo $this->url->get('post/news/' . $p->postSlug); ?>">Read More...</a></div>
                                     <div class="clearfix"></div>
                            </div>    
                            <?php } ?>
                            
                        <?php $v8931014201incr++; } ?>
                        <?php } ?>

                      <?php if ($post->total_items > 1) { ?>
                            <div class="paging">
                              <?php if ($post->current != 1) { ?>
                                <?php echo $this->tag->linkTo(array('admin/users?page=' . $post->before, 'Prev')); ?>
                              <?php } ?>

                              <?php $v8931014201iterator = range(1, $post->total_pages); $v8931014201incr = 0; $v8931014201loop = new stdClass(); $v8931014201loop->length = count($v8931014201iterator); $v8931014201loop->index = 1; $v8931014201loop->index0 = 1; $v8931014201loop->revindex = $v8931014201loop->length; $v8931014201loop->revindex0 = $v8931014201loop->length - 1; ?><?php foreach ($v8931014201iterator as $index) { ?><?php $v8931014201loop->first = ($v8931014201incr == 0); $v8931014201loop->index = $v8931014201incr + 1; $v8931014201loop->index0 = $v8931014201incr; $v8931014201loop->revindex = $v8931014201loop->length - $v8931014201incr; $v8931014201loop->revindex0 = $v8931014201loop->length - ($v8931014201incr + 1); $v8931014201loop->last = ($v8931014201incr == ($v8931014201loop->length - 1)); ?>
                              <?php if ($post->current == $index) { ?>
                                <?php echo $this->tag->linkTo(array('programs/page/' . $program->programPage . '?page=' . $index, $index, 'class' => 'current')); ?>
                              <?php } else { ?>
                                <?php echo $this->tag->linkTo(array('programs/page/' . $program->programPage . '?page=' . $index, $index)); ?>
                              <?php } ?>
                              <?php $v8931014201incr++; } ?>         

                              <?php if ($post->current != $post->total_pages) { ?>                 
                                <?php echo $this->tag->linkTo(array('programs/page/' . $program->programPage . '?page=' . $post->next, 'Next')); ?>
                              <?php } ?>
                            </div>
                      <?php } ?>
                    </div>

                    <?php } ?>
              <?php $v8931014201iterator = $prog_pages; $v8931014201incr = 0; $v8931014201loop = new stdClass(); $v8931014201loop->length = count($v8931014201iterator); $v8931014201loop->index = 1; $v8931014201loop->index0 = 1; $v8931014201loop->revindex = $v8931014201loop->length; $v8931014201loop->revindex0 = $v8931014201loop->length - 1; ?><?php foreach ($v8931014201iterator as $page) { ?><?php $v8931014201loop->first = ($v8931014201incr == 0); $v8931014201loop->index = $v8931014201incr + 1; $v8931014201loop->index0 = $v8931014201incr; $v8931014201loop->revindex = $v8931014201loop->length - $v8931014201incr; $v8931014201loop->revindex0 = $v8931014201loop->length - ($v8931014201incr + 1); $v8931014201loop->last = ($v8931014201incr == ($v8931014201loop->length - 1)); ?>
                <?php if ($page->pageActive) { ?>
                    <div class="tab-pane inside-contents <?php if ($v8931014201loop->first) { ?> <?php if ($program->programID != 1) { ?> active <?php } ?> <?php } ?>" id="<?php echo $page->pageSlug; ?>">
                        <?php echo $page->pageContent; ?>
   
                    </div>
                <?php } ?>
            <?php $v8931014201incr++; } ?>
          </div>
        </div>        
      </div> 