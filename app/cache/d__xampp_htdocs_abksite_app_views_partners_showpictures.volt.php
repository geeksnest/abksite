

        


          <h3><?php echo $albumName ?></h3>

            <?php if(empty($page->items)){ ?>
              <span>No albums found</span>
            <?php } ?>
            



            <?php foreach ($page->items as $pic) { ?>
              <a class="prettyPhoto[pp_gal]" href="<?php echo $pic['picturePath'] ?>" title="<?php echo $pic['pictureCaption'] ?>">
                <div title="<?php echo $pic['pictureCaption'] ?>" class="square bg" style="background-image:url('<?php echo $pic['picturePath'] ?>')">
              </div>
              </a>
            <?php } ?>
            
            
            
            

            <div class="">
              <!-- Footer goes here -->
              <?php if ($page->total_pages > 1) { ?>   
              <ul class="pagination pull-right">
                <?php if ($page->current != 1) { ?>
                <li><?php echo $this->tag->linkTo(array('partners/showpictures/' . $albumID . '?page=' . $page->before, 'Prev')); ?></li>
                <?php } ?>

                <?php foreach (range(1, $page->total_pages) as $index) { ?>
                <?php if ($page->current == $index) { ?>
                <li><?php echo $this->tag->linkTo(array('partners/showpictures/' . $albumID . '?page=' . $index, $index, 'style' => 'background-color:#eee')); ?></li>
                <?php } else { ?>
                <li><?php echo $this->tag->linkTo(array('partners/showpictures/' . $albumID . '?page=' . $index, $index)); ?></li>
                <?php } ?>
                <?php } ?>         

                <?php if ($page->current != $page->total_pages) { ?>                 
                <li><?php echo $this->tag->linkTo(array('partners/showpictures/' . $albumID . '?page=' . $page->next, 'Next')); ?></li>
                <?php } ?>
              </ul>
              <div class="clearfix"></div>
              <?php } ?>

        </div>
      