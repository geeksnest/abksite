 <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-question-sign"></i> Help</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="<?php echo $this->url->get('admin'); ?>"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <span>Help</span>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

          <div class="col-md-12">
                    <div class="widget">

                      <div class="widget-head">
                        <div class="pull-left">ABK users manual</div>
                        <div class="clearfix"></div>
                        <form>
                          <input type="submit" name="insertthis">
                        </form>
                          <object data="/manual/ABKusermanual.pdf" type="application/pdf" width="100%" height="900">					  
							</object>
                      </div>


                      <div class="widget-content">

                            <div class="widget-foot">
                              
                              <div class="clearfix"></div> 
                              <a href="/manual/ABKusermanual.pdf" download="ABK users manual" class="btn btn-default"><i class="icon icon-download"></i> Download Manual</a>

                            </div>

                          </div>

                        </div>


                      </div>              