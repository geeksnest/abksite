          <?php $token = $this->security->getToken() ?>

          <!-- Page heading -->
          <div class="page-head">
            <h2 class="pull-left"><i class="icon-table"></i> Towns</h2>

            <!-- Breadcrumb -->
            <div class="bread-crumb pull-right">
              <a href="index.html"><i class="icon-home"></i> Home</a> 
              <!-- Divider -->
              <span class="divider">/</span> 
              <?php echo $this->tag->linkTo(array('admin/towns', 'Towns')); ?>
              <span class="divider">/</span> 
              <span>Add town</span>
            </div>

            <div class="clearfix"></div>

          </div>
          <!-- Page heading ends -->

          <!-- Matter -->

          <div class="matter">
            <?php echo $this->tag->form(array('class' => 'form-horizontal', 'id' => 'main-table-form')); ?>
            <div class="container">

              <!-- Table -->

              <div class="row">

                <div class="col-md-12">
                  <?php echo $this->getContent(); ?>


                  <div class="widget">

                    <div class="widget-head">
                      <div class="pull-left">Add town</div>
                      <div class="widget-icons pull-right">
                        <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                        <a href="#" class="wclose"><i class="icon-remove"></i></a>
                      </div>  
                      <div class="clearfix"></div>
                    </div>


                    <div class="widget-content">

                      <!-- <div id="us2" style="width: 100%; height: 500px;"></div>
                      <input type="hidden" id="us2-address" class="form-control" /> -->

                        <div id="mapPanel">
                          <button onclick="addDefaultMarker();" type="button" class="btn btn-primary"><i class="icon-map-marker"></i> Add Marker</button>
                          <span> &nbsp; or click on the map to add marker.</span>
                          <input class="pull-right" onclick="deleteMarkers();" type=button value="Delete Marker" class="btn btn-default">
                        </div>
                        <div id="map-canvas" style="height:500px; width 100%"></div>

                      <div class="padd">

                          <div class="form-group">
                              <label class="col-lg-4 control-label">Latitude</label>
                              <div class="col-lg-8">
                                  <?php if ($editMode) { ?>
                                    <?php echo $form->render('townLat', array( 'value' => $town->townLat)); ?>
                                  <?php } else { ?>
                                  <?php echo $form->render('townLat'); ?>
                                  <?php } ?>
                                  
                                  <?php echo $form->messages('townLat'); ?>
                              </div>

                          </div>
                          <div class="form-group">
                              <label class="col-lg-4 control-label">Longtitude</label>
                              <div class="col-lg-8">
                                  <?php if ($editMode) { ?>
                                    <?php echo $form->render('townLong', array( 'value' => $town->townLong)); ?>
                                  <?php } else { ?>
                                  <?php echo $form->render('townLong'); ?>
                                  <?php } ?>
                                  
                                  <?php echo $form->messages('townLong'); ?>
                              </div>
                          </div>

                          <hr />
                          
                          <div class="form-group">
                              <label class="col-lg-4 control-label">Town Name</label>
                              <div class="col-lg-8">
                                  <?php if ($editMode) { ?>
                                    <input type="hidden" name="origTownName" value="<?php echo $town->townName; ?>">
                                    <?php echo $form->render('townName', array( 'value' => $town->townName)); ?>
                                  <?php } else { ?>
                                  <?php echo $form->render('townName'); ?>
                                  <?php } ?>
                                  <?php echo $form->messages('townName'); ?>
                              </div>
                          </div>

                          <!-- <div class="form-group">
                              <label class="col-lg-4 control-label"></label>
                              <div class="col-lg-8">
                                  <a href="#partnerGallery" data-toggle="modal" class="btn btn-default pull-right post-add-media"><i class="icon-paper-clip"></i> Add Media</a>
                              </div>
                          </div> -->


                          <div class="form-group">
                              <label class="col-lg-4 control-label">Town Information</label>
                              <div class="col-lg-8">
                              <?php echo $form->messages('townInfo'); ?>
                              <?php if ($editMode) { ?>
                                <?php echo $form->render('townInfo', array( 'value' => $town->townInfo)); ?>
                              <?php } else { ?>
                              <?php echo $form->render('townInfo'); ?>
                              <?php } ?>
                              </div>
                          </div>

                      </div>
                      <div class="widget-foot">
                          <?php echo $this->tag->linkTo(array('admin/towns', 'Back to Town List', 'class' => 'btn btn-default')); ?>
                          <?php echo $this->tag->submitButton(array('Save Town', 'name' => 'saveTown', 'class' => 'btn btn-primary')); ?>
                          <input type="hidden" name="csrf" value="<?php echo $this->security->getToken() ?>"/>
                          <div class="clearfix"></div>
                      </div>
                    </div>

                  </div>


                </div>

              </div>


            </div>
          </form>
        </div>

<!-- Matter ends -->