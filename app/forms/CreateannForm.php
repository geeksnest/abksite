<?php

use Phalcon\Forms\Form,
Phalcon\Forms\Element\TextArea,
Phalcon\Forms\Element\Text,
Phalcon\Forms\Element\Hidden,
Phalcon\Forms\Element\Password,
Phalcon\Forms\Element\Submit,
Phalcon\Forms\Element\Check,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\Email,
Phalcon\Validation\Validator\Identical,
Phalcon\Validation\Validator\StringLength,
Phalcon\Validation\Validator\Confirmation;

class CreateannForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        // In edition the id is hidden
        if (isset($options['edit']) && $options['edit']) {
            $hposttitle = new Hidden('hposttitle');
            $this->add($husername);
            $hemail = new Hidden('hpostid');
            $this->add($hpostid);            
        } 

        //Title
        $title = new Text('ann_title', array('class' => 'form-control col-lg-8', 'placeholder' => 'Enter title'));
        $title->setLabel('title');
        $title->addFilter('trim');
        $title->addValidators(array(
            new PresenceOf(array(
                'message' => 'The title is required'
                ))
            ));
        $this->add($title);

        //Content
        $content = new TextArea('ann_content', array('class' => 'form-control newsPostContent','id'=>'annContent'));
        $content->setLabel('Content');
        $content->addFilter('trim');
        $content->addValidators(array(
            new PresenceOf(array(
                'message' => 'The content is required'
                ))
            ));
        $this->add($content);

        //annStart
        $annStart = new Text('ann_start', array('readonly'=>'readonly', 'class' => 'form-control form-control dtpicker', 'data-format'=>'yyyy-MM-dd'));
        $annStart->setLabel('Start date');
        $annStart->addValidators(array(
            new PresenceOf(array(
                'message' => 'The start date is required'
            ))
        ));
        $this->add($annStart);

        //annStart
        $annEnd = new Text('ann_end', array('readonly'=>'readonly', 'class' => 'form-control form-control dtpicker', 'data-format'=>'yyyy-MM-dd'));
        $annEnd->setLabel('End date');
        $annEnd->addValidators(array(
            new PresenceOf(array(
                'message' => 'The end date is required'
            ))
        ));
        $this->add($annEnd);

        //CSRF
        $csrf = new Hidden('csrf');

       /* $csrf->addValidator(new Identical(array(
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
            )));
*/
        $csrf->addValidator(new Identical(array(
            $this->security->checkToken() => 1,
            'message' => 'CSRF-token validation failed'
        )));
        $this->add($csrf);        

    }
    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }    
}