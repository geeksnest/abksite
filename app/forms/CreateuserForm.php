<?php

use Phalcon\Forms\Form,
Phalcon\Forms\Element\Text,
Phalcon\Forms\Element\Hidden,
Phalcon\Forms\Element\Password,
Phalcon\Forms\Element\Submit,
Phalcon\Forms\Element\Check,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\Email,
Phalcon\Validation\Validator\Identical,
Phalcon\Validation\Validator\StringLength,
Phalcon\Validation\Validator\Regex,
Phalcon\Validation\Validator\Confirmation;

class CreateuserForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        // In edition the id is hidden
        if (isset($options['edit']) && $options['edit']) {
            $husername = new Hidden('huserName');
            $this->add($husername);
            $hemail = new Hidden('huserEmail');
            $this->add($hemail);            
        } 

        //User Account
        $name = new Text('username', array('class' => 'form-control', 'placeholder' => 'Username'));
        $name->setLabel('Username');
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'The username is required'
                )),
            new StringLength(array(
                'min' => 6,
                'messageMinimum' => 'Username should have at least 6 minimum characters'            
                 )),
            new Regex(array(
                'message'    => 'Username is invalid. Avoid spaces and symbols.',
                'pattern'    => '/^[a-zA-Z0-9._]+/',
                'allowEmpty' => false
                ))

            ));
        $this->add($name);

        //Email
        $email = new Text('email', array('class' => 'form-control', 'placeholder' => 'Email Address'));
        $email->setLabel('Email Address');
        $email->addFilter('trim');
        $email->addValidators(array(
           new PresenceOf(array(
                    'message' => 'The Email is required'
                    )),/*
                new Email(array(
                    'message' => 'The Email is required'
                    )),*/
             new Regex(array(
                    'message'    => 'Not a valid email format',
                    'pattern'    => '/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i',
                    'allowEmpty' => false
                    ))
            ));
        $this->add($email);

        if (!isset($options['edit']) && !$options['edit']/* || !isset($options['editown']) && !$options['editown']*/) {
            //Password
            $password = new Password('password', array('class' => 'form-control' , 'placeholder' => 'Password'));
            $password->setLabel('Password');
            $password->addFilter('trim');
            $password->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The password is required'
                    )), 
                new StringLength(array(
                  'max' => 20,
                  'min' => 8,
                  'messageMaximum' => 'Thats an exagerated password.',
                  'messageMinimum' => 'Password should be Minimum of 8 characters.'            
                  )),
                new Regex(array(
                    'message'    => 'Avoid using space.',
                    'pattern'    => '/^\S+$/',
                    'allowEmpty' => false
                    ))

                ));
            $this->add($password);        

            //Re Password
            $repassword = new Password('repassword', array('class' => 'form-control' , 'placeholder' => 'Re-Type Password'));
            $repassword->setLabel('Re-Type Password');
            $repassword->addFilter('trim');
            $repassword->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Retyping your password is required'
                    ))
                ));
            $this->add($repassword);
        }else{
            //Password
            /*$oldpassword = new Password('oldpassword', array('class' => 'form-control' , 'placeholder' => 'Old Password'));
            $oldpassword->setLabel('Old Password');            
            $this->add($oldpassword); */

            //Password
            $password = new Password('password', array('class' => 'form-control' , 'placeholder' => 'New Password'));
            $password->setLabel('New Password');            
            $this->add($password);   

            //Re Password
            $repassword = new Password('repassword', array('class' => 'form-control' , 'placeholder' => 'Re-Type Password'));
            $repassword->setLabel('Re-Type Password'); 
            $this->add($repassword);           
        }

        // if(!empty($options['editprofile']) && $options['editprofile']){
        //     //Password
        //     $oldpassword = new Password('oldpassword', array('class' => 'form-control' , 'placeholder' => 'Old Password'));
        //     $oldpassword->setLabel('Old Password');
        //     $oldpassword->addValidators(array(
        //         new PresenceOf(array(
        //             'message' => 'The old password is required'
        //             ))
        //         ));
        //     $this->add($oldpassword);  
        // }

        //User Profile
        //firstname
        $firstname = new Text('firstname', array('class' => 'form-control' , 'placeholder' => 'Firstname'));
        $firstname->setLabel('Firstname');
        $firstname->addFilter('trim');
        $firstname->addValidators(array(
            new PresenceOf(array(
                'message' => 'First Name is required'
                )),
            new StringLength(array(
              'min' => 2,
              'messageMinimum' => 'Firstname should have at least 2 minimum characters'            
              )),
            
            ));
        $this->add($firstname);
        //lastname
        $lastname = new Text('lastname', array('class' => 'form-control' , 'placeholder' => 'Lastname'));
        $lastname->setLabel('Lastname');
        $lastname->addFilter('trim');
        $lastname->addValidators(array(
            new PresenceOf(array(
                'message' => 'Lastname is required'
                )),
            new StringLength(array(
              'min' => 2,
              'messageMinimum' => 'Lastname should have at least 2 minimum characters'            
              ))            
            ));
        $this->add($lastname);

        //middlename
        $middlename = new Text('middlename', array('class' => 'form-control' , 'placeholder' => 'Middlename'));
        $middlename->setLabel('Middlename');
        $this->add($middlename);        

        //Contact
        $contact = new Text('contact', array('class' => 'form-control' , 'placeholder' => 'Contacts'));
        $contact->setLabel('Contact Number');
        $contact->addValidators(array(
            new PresenceOf(array(
                'message' => 'Contact number is required'
                ))
            ));        
        $this->add($contact);        

        //Address
        $address = new Text('address', array('class' => 'form-control' , 'placeholder' => 'Address'));
        $address->setLabel('Address');
        $this->add($address); 

        //Company
        $company = new Text('company', array('class' => 'form-control' , 'placeholder' => 'Company'));
        $company->setLabel('Company');
        $this->add($company); 

        //position
        $position = new Text('position', array('class' => 'form-control' , 'placeholder' => 'Position'));
        $position->setLabel('Position');
        $this->add($position);         

        //CSRF
        $csrf = new Hidden('csrf');

        // $csrf->addValidator(new Identical(array(
        //     'value' => $this->security->getSessionToken(),
        //     'message' => 'CSRF validation failed'
        //     )));
        $csrf->addValidator(new Identical(array(
            $this->security->checkToken() => 1,
            'message' => 'CSRF-token validation failed'
        )));

        $this->add($csrf);        

    }
    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }    
}