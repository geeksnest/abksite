<?php

use Phalcon\Forms\Form,
Phalcon\Forms\Element\TextArea,
Phalcon\Forms\Element\Text,
Phalcon\Forms\Element\Hidden,
Phalcon\Forms\Element\Password,
Phalcon\Forms\Element\Submit,
Phalcon\Forms\Element\Check,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\Email,
Phalcon\Validation\Validator\Identical,
Phalcon\Validation\Validator\StringLength,
Phalcon\Validation\Validator\Confirmation;

class SliderForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        // In edition the id is hidden
        if (isset($options['edit']) && $options['edit']) {
            $hemail = new Hidden('hsliderid');
            $this->add($hpostid);            
        } 

        $imageUrl = new Text('imageUrl', array('class' => 'form-control col-lg-8', 'placeholder' => 'Enter Image URL'));
        $imageUrl->setLabel('Banner URL');
        $imageUrl->addFilter('trim');
        $imageUrl->addValidators(array(
            new PresenceOf(array(
                'message' => 'The banner\'s URL is required'
                ))
            ));
        $this->add($imageUrl);

        $videoUrl = new Text('videoUrl', array('class' => 'form-control col-lg-8', 'placeholder' => '\'https://www.youtube.com/embed/xxxxxx\''));
        $videoUrl->setLabel('Video URL');
        $this->add($videoUrl);

        //CSRF
        $csrf = new Hidden('csrf');

       /* $csrf->addValidator(new Identical(array(
            $this->security->checkToken() => 1,
            'message' => 'CSRF-token validation failed'
        )));*/
        $csrf->addValidator(new Identical(array(
            $this->security->checkToken() => 1,
            'message' => 'CSRF-token validation failed'
        )));

        $this->add($csrf);        

    }
    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }    
}