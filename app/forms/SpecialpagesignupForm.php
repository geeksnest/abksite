<?php

use Phalcon\Forms\Form,
Phalcon\Forms\Element\Text,
Phalcon\Forms\Element\Hidden,
Phalcon\Forms\Element\Password,
Phalcon\Forms\Element\Submit,
Phalcon\Forms\Element\Check,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\Email,
Phalcon\Validation\Validator\Identical,
Phalcon\Validation\Validator\StringLength,
Phalcon\Validation\Validator\Confirmation;

class SpecialpagesignupForm extends Form
{
    public function initialize($entity = null, $options = null)
    {

        //User Account
        $name = new Text('username', array('class' => 'form-control border-flat', 'placeholder' => 'Enter your username'));
        $name->setLabel('Username');
        $name->addFilter('trim');
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Your username is a required field.'
                ))
            ));
        $this->add($name);

        //Email
        $email = new Text('email', array('class' => 'form-control border-flat', 'placeholder' => 'Enter your email'));
        $email->setLabel('Email Address');
        $email->addFilter('trim');
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'Your email is a required field.',
                'cancelOnFail' => true
                )),
            new Email(array(
                'message' => 'Your email is invalid.'
                ))
            ));
        $this->add($email);

        //Password
        $password = new Password('password', array('class' => 'form-control border-flat' , 'placeholder' => 'Enter your password'));
        $password->setLabel('Password');
        $password->addFilter('trim');
        $password->addValidators(array(
            new PresenceOf(array(
                'message' => 'The password is a required field.',
                'cancelOnFail' => true
                )), 
            new StringLength(array(
              'max' => 20,
              'min' => 8,
              'messageMaximum' => 'Thats an exagerated password.',
              'messageMinimum' => 'Password should be Minimum of 8 characters.'            
              )),

            ));
        $this->add($password);        

        //Re Password
        $repassword = new Password('repassword', array('class' => 'form-control border-flat' , 'placeholder' => 'Re-Type Password'));
        $repassword->setLabel('Re-Type Password');
        $repassword->addValidators(array(
            new PresenceOf(array(
                'message' => 'Retyping your password is required.',
                'cancelOnFail' => true
                )),
            new Confirmation(array(
             'message' => 'Password doesn\'t match confirmation.',
             'with' => 'password'
             )) 
            ));
        $this->add($repassword);

        //User Profile
        //firstname
        $firstname = new Text('name', array('class' => 'form-control border-flat' , 'placeholder' => 'Enter your full name'));
        $firstname->setLabel('Name');
        $firstname->addFilter('trim');
        $firstname->addValidators(array(
            new PresenceOf(array(
                'message' => 'Your name is a required field.',
                'cancelOnFail' => true
                )),
            new StringLength(array(
              'min' => 2,
              'messageMinimum' => 'Your name should have at least 2 minimum characters.'            
              ))
            ));
        $this->add($firstname);
        

       //CSRF
        $csrf = new Hidden('csrf');

       /* $csrf->addValidator(new Identical(array(
            $this->security->checkToken() => 1,
            'message' => 'CSRF-token validation failed'
        )));*/
        $csrf->addValidator(new Identical(array(
            $this->security->checkToken() => 1,
            'message' => 'CSRF-token validation failed'
        )));

        $this->add($csrf);        

    }
    /**
     * Prints messages for a specific element
     */
    public function messages($name, $return = false)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                if($return)
                    return $message;
                else
                    $this->flash->error($message);
            }
        }
    }
}